@ShipCookiesLogin

Feature: Ship Cookies(Logged in User)- This is used to place shipping order

  Background: user access to website
    Given User opens the Website without clearing cache

	Scenario: Verify recaptcha
		And verify captcha is disabled

  Scenario: Precondition - user logs in as a current customer
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra@gmail.com |
      | password | Judge@123			          |
    And Click on the Log In button


  Scenario: TC001 -Logged In-  Verify whether user is able to access Ship cookies page
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    Then ship cookies page should be displayed successfully


  Scenario: TC026 - Verify Logged In user is able to place order through Saved Card option on Checkout Page  #BUG ID: 6020
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
     And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects saved credit card
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    

  Scenario: TC002 - Verify Logged In User is able to place order with Gift card
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC003/TC005 - Verify User is able to place order with Credit card
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue bryn mawr |
    And user selects Payment Method as Credit Card
    #And user clicks New Card
    And user enters credit card details for logged in user to ship order
      | cc no     			| 4111111111111111 |
      | cc expiry month | 12 	             |
      | cc expiry year	|	2019						 |
    And user clicks on Place Order for Shipping order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC004/TC008 - Verify user is able to add TRIPLE LAYERED COOKIE CAKE in Cart through + option and Order Successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user adds cookies by clicking +
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue bryn mawr |
    And user selects Payment Method as Credit Card
    #And user clicks New Card
    And user enters credit card details for logged in user to ship order
      | cc no     			| 4111111111111111 |
      | cc expiry month | 12 	             |
      | cc expiry year	|	2019						 |
    And user clicks on Place Order for Shipping order
    Then Order confirmation page should appear displaying Order summary



  Scenario: TC006 - Verify user is able to add 18 Cookie Gift Box in Cart through Pick For Me option and Order Successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

 
  Scenario: TC007 - Verify user is able to add 24 Cookie Gift Box in Cart through Pick For Me option and Order Successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

   
  Scenario: TC009 - Verify user is able to add 12 Cookie Gift Box in Cart by selecting manually and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue bryn mawr |
    And user selects Payment Method as Credit Card
    #And user clicks New Card
    And user enters credit card details for logged in user to ship order
      | cc no     			| 4111111111111111 |
      | cc expiry month | 12 	             |
      | cc expiry year	|	2019						 |
    And user clicks on Place Order for Shipping order
    Then Order confirmation page should appear displaying Order summary



  Scenario: TC010 - Verify user is able to add 18 Cookie Gift Box in Cart by selecting manually and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC011 - Verify user is able to add 24 Cookie Gift Box in Cart by selecting manually and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC012 - verify user is able to deselect Triple Layer Cookie Cake manually
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user selects cookies manually
    And user deselects cookies manually
    Then Make required choices button should appear indicating zero selections
@ShipCookiesLogin11
  Scenario: TC013 - verify user is able to deselect 12 Cookies Gift box  cookies manually
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user selects cookies manually
    And user deselects cookies manually
    Then Make required choices button should appear indicating zero selections

  Scenario: TC014 - verify user is able to deselect 18 Cookies Gift box manually
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user deselects cookies manually
    Then Make required choices button should appear indicating zero selections


  Scenario: TC015 - verify user is able to deselect 24 Cookies Gift box manually
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user selects cookies manually
    And user deselects cookies manually
    Then Make required choices button should appear indicating zero selections


  Scenario: TC016 - verify user is able to close Triple Layer Cookie Cake  menu
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user clicks on  close button
    Then product menu should close

  Scenario: TC017 - verify user is able to close 12 Cookie Gift Box menu
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on  close button
    Then product menu should close

  Scenario: TC018 - verify user is able to close 18 Cookie Gift Box menu
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user clicks on  close button
    Then product menu should close


    Scenario: TC019 - verify user is able to close 24  Cookie Gift Box Menu
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user clicks on  close button
    Then product menu should close

    Scenario: TC020 - Verify menu is displayed for Triple Layer Cookie Cake
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    Then menu options should be displayed
      | Chocolate Chunk        		|
      | Classic with M&M's 		 		|
      | Double Chocolate Chunk 		|
      | Double Chocolate Mint  		|
      | Oatmeal Raisin         		|
      | Peanut Butter Chip     		|
#      | Red Velvet								|
      | Snickerdoodle          		|
      | Sugar                  		|
      | White Chocolate Macadamia |


    Scenario: TC021 - Verify menu is displayed for 12 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    Then menu options should be displayed
      | Chocolate Chunk        		|
      | Classic with M&M's 		 		|
      | Double Chocolate Chunk 		|
      | Double Chocolate Mint  		|
      | Oatmeal Raisin         		|
      | Peanut Butter Chip     		|
#      | Red Velvet								|
      | Snickerdoodle          		|
      | Sugar                  		|
      | White Chocolate Macadamia |


    Scenario: TC022 - Verify menu is displayed for 18 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    Then menu options should be displayed
      | Chocolate Chunk        		|
      | Classic with M&M's 		 		|
      | Double Chocolate Chunk 		|
      | Double Chocolate Mint  		|
      | Oatmeal Raisin         		|
      | Peanut Butter Chip     		|
#      | Red Velvet								|
      | Snickerdoodle          		|
      | Sugar                  		|
      | White Chocolate Macadamia |

     
    Scenario: TC023 - Verify menu is displayed for 24 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    Then menu options should be displayed
      | Chocolate Chunk        		|
      | Classic with M&M's 		 		|
      | Double Chocolate Chunk 		|
      | Double Chocolate Mint  		|
      | Oatmeal Raisin         		|
      | Peanut Butter Chip     		|
#      | Red Velvet								|
      | Snickerdoodle          		|
      | Sugar                  		|
      | White Chocolate Macadamia |


    Scenario: TC024 - Verify user is able to add VEGAN/GLUTEN FREE 12 COOKIE GIFT BOX in cart by clicking on '+' and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user clicks + of Vegan/ Gluten Free Twelve Cookie Gift Box
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


    Scenario: TC025 - Verify user is able to add VEGAN/GLUTEN FREE 24 COOKIE GIFT BOX in cart by clicking on '+' and order successfully
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user clicks + of Vegan/ Gluten Free Twenty Four Cookie Gift Box
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

@ShipCookiesLogin11
    Scenario: TC027 - Verify user is able to select Quantity for every product from their respective menu page
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user clicks quantity
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    Then updated quantity of the product should reflect in the cart
    And user selects Twelve cookies Gift box
    And user clicks quantity
    And user selects cookies manually
    And user clicks Add Product button
    Then updated quantity of the product should reflect in the cart
    And user selects Eighteen cookies Gift box
    And user clicks quantity
    And user selects cookies manually
    And user clicks Add Product button
    Then updated quantity of the product should reflect in the cart
    And user selects Twenty Four cookies Gift box
    And user clicks quantity
    And user selects cookies manually
    And user clicks Add Product button
    Then updated quantity of the product should reflect in the cart
    And user selects VEGAN/GLUTEN FREE TWELVE COOKIE GIFT BOX
     And user clicks quantity
     And user clicks Add Product button
   Then updated quantity of the product should reflect in the cart
    And user selects VEGAN/GLUTEN FREE TWENTY FOUR COOKIE GIFT BOX
     And user clicks quantity
     And user selects cookies manually
    And user clicks Add Product button
    Then updated quantity of the product should reflect in the cart
  

    Scenario: TC029 - Verify user is able to apply Amount coupon
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then popup should appear stating - Coupon successfully applied
    And Amount coupon should be added successfully to the cart
    

    Scenario: TC030 - Verify user is able to apply Percentage coupon as Logged In user
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Percentage code | CYBER |
    And user clicks Apply button
    Then popup should appear stating - Coupon successfully applied
    And Percentage coupon should be added successfully to the cart

   
    Scenario: TC031 - Verify user is able to apply Free Delivery coupon as Logged In user
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
       | Free Delivery code | DELIVERY10 |
    And user clicks Apply button
    Then popup should appear stating - Coupon successfully applied
    And FreeDelivery coupon should be added successfully to the cart
  
    Scenario: TC032 - Verify user is not able to apply Product coupon as Logged In user for Ship cookies order
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
       | Product coupon | 2Free |
    And user clicks Apply button
    Then popup should appear stating
    |There are no available products at this store for this coupon.|
   
    Scenario: TC067 - Verify Logged In user gets the suggestion to place Delivery order if address entered on checkout page falls in the delivery range
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue Bryn Mawr |
    Then user should get the suggestion to place Delivery order


    Scenario: TC069 - Verify link stating - Hey, it looks like you are in delivery range! Click here to get your order delivered, directs to Orders page and cart should get cleared
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue Bryn Mawr |
    And user clicks on here link displayed in suggession under address box
    Then popup should appear
    And on clicking Okay button
    Then cart should get cleared

  
    Scenario: TC077 - Verify user is able to update product of Triple layer Cookie Cake
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects triple layer cookie cake
    And user adds cookies by clicking +
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
    
   
    Scenario: TC078 - Verify user is able to update product of 12 Cookie Gift Box 
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
    
    Scenario: TC083 - Verify user is able to update product of 18 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
    
    Scenario: TC084 - Verify user is able to update product of 24 Cookie Gift Box
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twenty Four cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
    
    
    Scenario: TC085 - Verify user is able to update product of VEGAN/GLUTEN FREE 12 COOKIE GIFT BOX 
     When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user clicks + of Vegan/ Gluten Free Twelve Cookie Gift Box
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart


    Scenario: TC086 - Verify user is able to update product of VEGAN/GLUTEN FREE 24 COOKIE GIFT BOX 
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user clicks + of Vegan/ Gluten Free Twenty Four Cookie Gift Box
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity of the product should reflect in the cart
    
    Scenario: TC028 - Verify User is able to see 3 delivery options on Checkout Page
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Eighteen cookies Gift box
    And user selects cookies manually
    And user clicks Add Product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue Bryn Mawr |
    Then shipping methods should be displayed
    