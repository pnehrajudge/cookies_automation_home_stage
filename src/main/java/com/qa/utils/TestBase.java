package com.qa.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.aventstack.extentreports.gherkin.model.Scenario;
import com.qa.helper.ConfigFileReader;
import com.qa.pages.AdminStores;

	public class TestBase {
		
		public static WebDriver driver;
		public static String url;
		public static String admin_url;
		public static Properties prop;
//		ConfigFileReader configFileReader= new ConfigFileReader();
		protected static AdminStores adminStores;
		public TestBase () {			
			try {
				prop = new Properties();
				FileInputStream fis = new FileInputStream("src//main//java//com//qa//config//Config.properties");  //C:\\Users\\Adminindia\\workspace\\Cookies_Automation_Home\\src\\main\\java\\com\\qa\\config\\Config.properties
				prop.load(fis);
			}catch (IOException e) {
				e.getMessage();
			}					
			
		}
		
		
		public static WebDriver getDriver() {
			
			if(driver==null){
			String browserName = prop.getProperty("browser");
			
			if(browserName.equals("firefox")) {
				
				// To fetch the severe JS errors
//				DesiredCapabilities capibilities = DesiredCapabilities.chrome();
//				LoggingPreferences loggingPreferences = new LoggingPreferences();
//				loggingPreferences.enable(LogType.BROWSER, Level.SEVERE);
//				capibilities.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);
				
			// Code for Selenium Grid to execute cross browser test using hub and nodes
//				String hubUrl = "http://10.1.30.33:4444/wd/hub";     //Hub URL
//				driver = new RemoteWebDriver(new URL(hubUrl),capibilities);
				
				System.setProperty("webdriver.gecko.driver","src//main//java//com//qa//resources//geckodriver.exe");
				// .....//home//rof//bin//geckodriver   .....src//main//java//com//qa//resources//geckodriver.exe
			driver = new FirefoxDriver();	
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
			}
			else if(browserName.equals("chrome")) {
				System.setProperty("webdriver.chrome.driver","src//main//java//com//qa//resources//chromedriver.exe");  
				// src//main//java//com//qa//resources//chromedriver.exe---//home//rof//bin//chromedriver  -----//usr//bin//chromedriver
				driver = new ChromeDriver();	
				driver.manage().window().maximize();
				driver.manage().deleteAllCookies();
				driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
				driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
			}
			

			
//			driver.get("https://15.stage.insomniacookies.com/admin");
//			adminStores = new AdminStores(driver);
			}
			return driver;
				
		}
	
		public String getUrl(){
			if(url==null){
				
				url=prop.getProperty("url");
			}
			
			return url;
		}
		
		public String getAdminUrl(){
	if(admin_url==null){
				
		admin_url=prop.getProperty("url_admin");
			}
			
			return admin_url;
		}
		
		public void clear_Chrome_History_and_Cookies(WebDriver driver) throws Throwable{
			driver.manage().deleteAllCookies();
//			driver.get("chrome://settings/clearBrowserData");
//			   Thread.sleep(1000);
//			   Robot r= new Robot();			  
//		       r.keyPress(KeyEvent.VK_ENTER);
//		       Thread.sleep(1000);
//		       r.keyPress(KeyEvent.VK_ENTER);
//		       Thread.sleep(30000);
		}
		
		public void clear_Firefox_History_and_Cookies(WebDriver driver) throws Throwable{
			driver.manage().deleteAllCookies();
			driver.get("about:preferences#privacy");
			   Thread.sleep(1000);
			   driver.manage().deleteAllCookies();
		       Thread.sleep(30000);
		}
		
	public String getReportConfigPath(){
	String reportConfigPath = prop.getProperty("reportConfigPath");
	if(reportConfigPath!= null) return reportConfigPath;
	else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath"); 
	}

	@Before
	public void setUp(Scenario scenario)
	{
	}
	
	

	}
	

