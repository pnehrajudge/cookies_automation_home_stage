package com.qa.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

public class ShipCookies extends TestBase{
	
	@FindBy(xpath = "/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/img[1]")
	public WebElement alt_twelve_cookies;

	@FindBy(xpath = "//h5[contains(text(),'12 Cookie Gift Box')]")      //*[@id='ship-cookies']/div/div/div/div[2]/div/div[1]/img
	public WebElement twelve_cookies;

	@FindBy(xpath = "//*[@id='cc-stored-card']")   //*[@id='chase-radio-btn-saved']
	public WebElement saved_credit_card;

	@FindBy(xpath = "//a[contains(text(),'Ship Cookies')]")
	public WebElement ship_cookies_tab;
	
	@FindBy(xpath = "//h3[@class='catalog-title active']")
	public WebElement ship_cookies_page;
	
	@FindBy(xpath = "//input[@id='recipient-address-text']")
	public WebElement recipient_address_box;
	
	@FindBy(xpath = "//div[@class='tt-dataset tt-dataset-1']/div")       //*[@id='recipient-address-text']
	public WebElement address_suggestion;
	
	@FindBy(xpath = "//h5[contains(text(),'Triple Layer Cookie Cake')]")  //div[contains(@class,'row mt-1 mt-lg-5')]//div[contains(@class,'row')]//div[1]//div[1]//div[1]//img[1]
	public WebElement triple_layer_cake;
	
	@FindBy(xpath = "//div[contains(@class,'textbox-container')]//div[1]//div[1]//div[2]//div[1]//div[3]//button[1]")  
	public WebElement triple_layer_choco_chunk;
	
	@FindBy(xpath = "//div[contains(@class,'col-12 col-md-6')]//div[2]//div[1]//div[2]//div[1]//div[3]//button[1]")
	public WebElement triple_layer_classic;
	
	@FindBy(xpath = "//div[3]//div[1]//div[2]//div[1]//div[3]//button[1]")
	public WebElement triple_layer_double_choco_chunk;
	
	@FindBy(xpath = "//div[contains(@class,'mt-4 d-none d-lg-block')]//button[contains(@type,'button')][contains(text(),'Add Product')]")
	public WebElement add_product_button;
	
	@FindBy(xpath = "//h5[contains(text(),'18 Cookie Gift Box')]")
	public WebElement eighteen_cookies_gift_box;
	
	@FindBy(xpath = "//h5[text()='24 Cookie Gift Box']")
	public WebElement twenty_four_cookies_gift_box;
	
	@FindBys(@FindBy(xpath="//*[contains(@class,'btn-rounded-lg slider-btn-add')]"))
	public List<WebElement> manual_cookies_selection;
	
	@FindBys(@FindBy(xpath="//*[contains(@class,'btn-rounded-lg slider-btn-minus')]"))  ////div[contains(@class,'textbox-container')]//div[1]//div[1]//div[2]//div[1]//div[1]//button[1]
	public List<WebElement> manual_cookies_deselection;
	
	@FindBy(xpath = "/html[1]/body[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/button[1]")
	public WebElement make_choice_button;
	
	@FindBys(@FindBy(xpath="//h4[contains(@class,'option-title')]"))
	public List<WebElement> menu_options;
	
	@FindBy(xpath = "//h5[contains(text(),'Vegan/ Gluten Free 12 Cookie Gift Box')]/ancestor::div[@class='col  card-label ']/following-sibling::div/button")   //*[@id=\"ship-cookies\"]/div/div/div/div[5]/div
	public WebElement add_vegan_12_cookies;
	
	
	@FindBy(xpath = "//h5[contains(text(),'Vegan/ Gluten Free 12 Cookie Gift Box')]")
	public WebElement vegan_12_cookies;	
	
	@FindBy(xpath = "//h5[contains(text(),'Vegan/ Gluten Free 24 Cookie Gift Box')]/ancestor::div[@class='col  card-label ']/following-sibling::div/button")
	public WebElement add_vegan_24_cookies;
	
	@FindBy(xpath = "//h5[contains(text(),'Vegan/ Gluten Free 24 Cookie Gift Box')]")     //div[7]/div[1]/div[1]/img[1]
	public WebElement vegan_24_cookies;
	
	
	@FindBy(xpath = "//div[contains(@class,'mt-4 d-none d-lg-block')]//button[contains(@type,'button')][contains(text(),'1')]")
	public WebElement quantity_button;
	
	@FindBy(xpath = "//ul[contains(@class,'dropdown-menu show')]//li[contains(@class,'dropdown-item')][contains(text(),'2')]")
	public WebElement quantity_index;
	
	@FindBy(xpath = "//p[contains(text(),'Hey, it looks like you are in delivery range! Clic')]")
	public WebElement suggestion;
	
	
	@FindBy(xpath = "//a[@id='shipping-delivery-change']")
	public WebElement here;
	
	@FindBy(xpath = "//div[@class='form-group radios-group active']/div[1]")
	public WebElement shipping_method_1;
	
	@FindBy(xpath = "//div[@class='form-group radios-group active']//div[2]")
	public WebElement shipping_method_2;
	
	@FindBy(xpath = "//div[@id='checkout-choose-shipping-method']//div[3]")
	public WebElement shipping_method_3;
	

	WebDriver driver = getDriver();
	WebDriverWait wait = new WebDriverWait(driver, 30);

	public ShipCookies(WebDriver driver) {
		// super(driver);
		PageFactory.initElements(driver, this);

	}
	
	

}
