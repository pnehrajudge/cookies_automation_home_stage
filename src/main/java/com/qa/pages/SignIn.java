package com.qa.pages;

import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.utils.TestBase;

public class SignIn extends TestBase{
	
	private int invalidImageCount;
	
WebDriver driver = getDriver();
	
	public SignIn(WebDriver driver) {
		//super(driver);
		PageFactory.initElements(driver, this);

	}
	
	
	@FindBy(xpath="//a[contains(text(),'log In')]")
	public WebElement login_button;
	
	@FindBy(xpath="//input[@id='email']")
    public WebElement username;
	
	@FindBy(xpath="//form[@id='login-form']//input[@placeholder='Password']")
    public WebElement password;
	
	@FindBy(xpath = "//div[contains(text(),'Please enter an email address.')]")
	public WebElement alert_email;
	
	@FindBy(xpath="//div[contains(text(),'The password field is required.')]")
	public WebElement alert_password;
	
	@FindBy(xpath="//button[@id='login-btn']")
    public WebElement submit_login;
	
	@FindBy(xpath="//a[contains(text(),'Hi')]")
	public WebElement login_text;
	
	@FindBy(xpath="//*[@id='sign-in-email']")
	public WebElement username_hover;
	
	@FindBy(xpath="//*[@id='sign-in-password']")
	public WebElement password_hover;
	
	@FindBy(xpath="//button[@id='sign-in-form-submit']")
	public WebElement submit_hover;
	
	@FindBy(xpath="//a[contains(text(),'Log Out')]")
	public WebElement logout;
	
	@FindBy(xpath="//a[contains(text(),'log In')]")
	public WebElement loginText_ToLogin;
	
	@FindBy(xpath="//button[contains(text(),'Okay')]")
	public WebElement confirmChangingStore;
	
	@FindBy(xpath="//h2[contains(text(),'Order History')]")
	public WebElement orderHistory;
	
	@FindBy(xpath="//input[@id='email-register']")
	public WebElement newCustomerEmail;
	
	@FindBy(xpath="//div[contains(text(),'icprateeknehra1@gmail.com')]")   //*[@id='account-container']/section/div/div[1]/div/div[2]/h4
	public WebElement myAccountDetails;
	
	@FindBy(xpath="//div[@id='card-address']")
	public WebElement addressDetails;
	
	@FindBy(xpath="//*[@id='account-container']/section/div/div[1]/div/ul[1]/li")
	public List<WebElement> loyaltyDetails;
	
	@FindBy(xpath="//div[contains(text(),'Loyalty')]")
	public WebElement loyalty;
	
	@FindBy(xpath="//div[@id='heading1']")
	public WebElement orderHistory1;
	
	@FindBy(xpath="//div[@id='heading2']")
	public WebElement orderHistory2;
	
	@FindBy(xpath="//a[contains(text(),'load more')]")
	public WebElement orderLoadMore;
	
	@FindBy(xpath="//*[contains(@id,\"heading\")]/a")
	public List<WebElement> numberofOrders;
	
	@FindBy(xpath="//a[contains(text(),'edit')]")
	public WebElement editProfile;
	
	@FindBy(xpath="//button[contains(@class,'btn btn-primary btn-block update-profile-btn')]")
	public WebElement updateProfile;	
	
	@FindBy(xpath="//input[@placeholder='First name']")
	public WebElement firstName;	
	
	@FindBy(xpath="//input[@placeholder='Last name']")
	public WebElement lastName;
	
	@FindBy(xpath="//input[@id='telephone']")
	public WebElement phoneNumber;
	
	@FindBy(xpath="//*[@id=\"user-profile-container\"]/section/div/div[1]/div/div[2]")
	public WebElement profileInfo;
	
	@FindBy(xpath="//*[@id='account-container']/section/div/div[1]/div/div[5]/a")
	public WebElement viewAllAddress; 
	
	@FindBy(xpath="//a[contains(text(),'New Address')]")
	public WebElement newAddress; 
		
	@FindBy(xpath="//input[@id='add-new-first']")
	public WebElement firstName_forNewAddress; 
	
	@FindBy(xpath="//input[@id='add-new-last']")
	public WebElement lastName_forNewAddress; 
		
	@FindBy(xpath="//input[@id='add-new-nickname']")
	public WebElement nickName_forNewAddress;
	
	@FindBy(xpath="//input[@id='add-new-address']")
	public WebElement addNewAddressInputField;
	
	@FindBy(xpath="//button[@id='save-new-address']")
	public WebElement saveNewAddressBtn;
	
	@FindBy(xpath="//*[@id='app']/div/div[3]/div[2]/div/div/a[2]")
	public WebElement deleteAddress;
	
	
	@FindBy(xpath="//a[@class='edit-address']")
	public WebElement editAddress;
	
	@FindBy(xpath="//*[@id='edit-address-modal']/div/div/div[3]/button[2]")
	public WebElement updateAddress;
	
	
	@FindBy(xpath="//*[@id=\"edit-address-modal\"]/div/div/div[2]/div/div/div/form/div[2]/div/div[1]/span/input[2]")
	public WebElement editAddressInput;
	
	@FindBy(xpath="//div[@id='collapse0']//button[contains(@class,'btn-primary btn-block')][contains(text(),'Checkout')]")  //*[@id=collapse0]/div[2]/div[2]/div[6]/div[1]/button
	public WebElement myAccountCheckoutBtn;
	
	@FindBy(xpath="//*[@id='collapse0']/div[2]/div[2]/div[5]/div/div/div/following-sibling::select")  //*[@id='inputGroupSelect01']
	public WebElement myAccountTip;
	
	
	@FindBy(xpath="//div[@class='card-body defaut-card']")
	public WebElement cardInfo;
	
	@FindBy(xpath="//div[contains(text(),'Credit Cards')]/a")
	public WebElement viewAllCC;
	
	@FindBy(xpath="//div[@class='col-lg-8']//div[1]//div[1]//div[1]//a[1]")
	public WebElement deleteCC;
	
	@FindBy(xpath="//*[@id='user-profile-container']/section/div/div[2]/form[1]/div[2]/div/div[1]/input")
	public WebElement signedInEmail;
	
	@FindBy(xpath="//*[@id='account-container']/section/div/div[1]/div/div[8]")
	public WebElement noCCText;
	
	@FindBy(xpath="/html/body/nav[1]/div/div[4]/ul/li/a")
	public WebElement topProfileName;
	
	
	@FindBy(xpath="//input[@placeholder='New Password']")
	public WebElement newPassword;
	
	@FindBy(xpath="//input[@placeholder='Current Password']")
	public WebElement currentPassword;
	
	@FindBy(xpath="//input[@placeholder='Confirm Password']")
	public WebElement confirmPassword;
	
	@FindBy(xpath="//button[contains(@class,'btn btn-primary btn-block update-password-btn')]")
	public WebElement updatePasswordBtn;
	
	@FindBy(xpath="//*[@id='user-profile-container']/section/div/div[2]/form[2]/div[2]/div/div[1]/input")
	public WebElement newPasswordType;
	
	@FindBy(xpath="//input[contains(@name,'allow_push_notifications')]")
	public WebElement allowPushNotificationCheckBox;
	
	@FindBy(xpath="//input[@name='storecc']")
	public WebElement remeberCC_ChkBox;
	
	@FindBy(xpath="//input[@name='large_groups']")
	public WebElement largeGrpChkBox;
	
	@FindBy(xpath="//input[@name='event_offers']")
	public WebElement eventOfferingsChkBox;
	
	@FindBy(xpath="//input[@name='newsletter']")
	public WebElement newsLetterChkBox;
	
	@FindBy(xpath="//form[contains(@class,'edit-address-form')]//input[@placeholder='First name']")
	public WebElement firstName_forEditAddress;
	
	@FindBy(xpath="//form[contains(@class,'edit-address-form')]//input[@placeholder='Last name']")
	public WebElement lastName_forEditAddress;
		
	@FindBy(xpath="//input[@name='nickname']")
	public WebElement nickName_forEditAddress;
	
	@FindBy(xpath="//*[@id='edit-address-modal']/div/div/div[2]/div/div/div/form/div[2]/div/div[1]/span/input[2]")
	public WebElement address_forEditAddress;
		
	@FindBy(xpath="//*[@id='edit-address-modal']/div/div/div[2]/div/div/div/form/div[2]/div/div[2]/input")
	public WebElement aptSuite_forEditAddress;
	
	@FindBy(xpath="//form[contains(@class,'edit-address-form')]//select[@class='custom-select mt-2 mt-md-0']")
	public WebElement dormName_forEditAddress;
	
	@FindBy(xpath="//*[contains(@class,'fa favorite-location')]")
	public List<WebElement> favoriteList;
	
	@FindBy(xpath="//*[contains(@class,'fa fa-star')]")
	public List<WebElement> ccFavList;
	
	@FindBy(xpath="//*[@id='app']/div/div[3]/div[1]/div/h5")
	public WebElement favAddressName;
	
	
	@FindBy(xpath="//*[@id='app']/div/div[3]/div[1]/p")
	public WebElement favAddressOnTop;
	
	
	@FindBy(xpath="//div[@class='alert alert-danger']")
	public WebElement emailPasswordMismatch_messsage;
	
	@FindBy(xpath="//*[@id='login-form']/div[1]/div")
	public WebElement emailIDValidationMsg;
	
	@FindBy(xpath="//*[@id='login-form']/div[2]/div")
	public WebElement passwordValidationMsg;
	
	@FindBy(xpath="//*[@id=\"collapse0\"]/div[2]/div[2]/div[6]/div[2]/button")
	public WebElement addToCart_and_Continue;
	
	@FindBy(xpath="//*[@id='heading0']/a/button")
	public WebElement trackOrder;
	
	@FindBy(xpath="//*[@id='addresstext']")
	public WebElement homePageAddressInput;
	
	@FindBy(xpath="//*[@class='btn btn-primary btn-block'][contains(text(),'Sign Up')]")
	public WebElement homePageSignUp;
	
	@FindBy(xpath="//*[@class='btn btn-primary btn-block'][contains(text(),'Learn More')]")
	public WebElement homePageLearnMore;
	
	@FindBy(xpath="//*[@class='btn btn-primary btn-block'][contains(text(),'Order Now')]")
	public WebElement homePageOrderNow;
	
	@FindBy(xpath="//*[@id='order-now-btn']")
	public WebElement homePageContinueBtn;
	
	@FindBy(xpath="//h3[text()='New Customer']")
	public WebElement signUpNewCustomer;
	
	@FindBy(xpath="//strong[text()='Get started by signing up now!']")
	public WebElement getStartedPageText;
	
	@FindBy(xpath="//*[@class='catalog-title active'][text()='Ship Cookies']")
	public WebElement shipCookiesTextOnShipCookiesPage;
	
	public void validateInvalidImages(WebDriver driver) throws InterruptedException {
		try {
			invalidImageCount = 0;
			List<WebElement> imagesList = driver.findElements(By.tagName("img"));
			System.out.println("Total no. of images are " + imagesList.size());
			for (WebElement imgElement : imagesList) {
				if (imgElement != null) {
					verifyimageActive(imgElement);
				}
			}
			System.out.println("Total no. of invalid images are "	+ invalidImageCount);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public void verifyimageActive(WebElement imgElement) {
		try {
			org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(imgElement.getAttribute("src"));
			HttpResponse response = client.execute(request);
			// verifying response code he HttpStatus should be 200 if not,
			// increment as invalid images count
			if (response.getStatusLine().getStatusCode() != 200)
				invalidImageCount++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String randomNumber(String str1) {
		Random rand = new Random();
		int randomNumber = rand.nextInt(10000);
		randomNumber += 1;
		String randomStr = Integer.toString(randomNumber);
		String str = str1;
		String randomString = str+randomStr;
		return randomString;
	}
	
	public boolean isreadOnly(WebElement element) {        
	    Boolean readOnly = false;       
	    readOnly = ((element.getAttribute("disabled") != null) || (element.getAttribute("readonly") != null));  
	    return readOnly;        
	}

}
