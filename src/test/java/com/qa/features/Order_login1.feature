@OrderLogin1
Feature: Order_Login_Part2

  Background: user access to website
    Given User opens the Website without clearing cache

  # | url | https://5.stage.insomniacookies.com/ |
 
  Scenario: Precondition - user logs in as a current customer
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra@gmail.com |
      | password | Judge@123         |
    And Click on the Log In button
 
Scenario: TC099 - Logged in user - Verify user is able to click on Address book icon
    Given user clicks on Order button
    And user clicks on Address icon
    Then Address book  page should open


  Scenario: TC101 - Logged in user - Verify user is able to access left arrow present in address
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on left arrow to see address
    Then User should able to move left arrow to see near by stores

  #Scenario: TC112 - Logged in user - Verify others stores should disable when selecting a store for orders
    #Given user clicks on Order button
    #When user enters the address
      #| address | 1084 East Lancaster Avenue, Bryn Mawr |
    #Then Other stores should be disabled

  Scenario: TC103 - Logged in user - Verify user should able to see catering availability with green tick in stores
    Given user clicks on Order button
    When user enters the address
      | address | 135 South 13th Street, Philadelphia, PA |
    Then catering availability with green tick in stores should be visible


  Scenario: TC104 - Logged in user - Verify user is able to access right arrow present in address
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on right arrow to see address
    Then User should able to move right arrow to see near by stores

#Catering is now available for all the stores so commenting this test case as invalid
  #Scenario: TC105 - Logged in user - Verify user should able to see catering unavailability without any mark in stores
    #Given user clicks on Order button
    #When user enters the address
      #| address | 1084 East Lancaster Avenue, Bryn Mawr |
    #Then catering unavailability is shown with green tick not visible

  Scenario: TC106 - Logged in user - Verify in case user does not find the store ,he /she should be able to search store with different address
    Given user clicks on Order button
    When user enters the address
      | address | Jubilee St, Soldotna, Alaska |
    And user is not able to locate store displaying message displayed
    And user removes entered address
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    Then store should be displayed

  Scenario: TC107 - Logged in user - Verify pop-up appears and cart containing Delivery/Pickup order-gets cleared in case user clicks on Gift card tab
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave S, Birmingham, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on a Six Pack
    And user clicks on add product
    And user clicks on Gifts tab
    And user clicks Gift Card option
    Then popup should appear
    And on clicking Okay button
    Then cart should get cleared

  Scenario: TC108 - Verify user gets the message for Pick up or ship cookies order , if user is within 50 miles of a store while placing Delivery/Pickup /Catering Order
    Given user clicks on Order button
    When user enters the address
      | address | 218 S Dupont Rd, Wilmington, DE |
    Then only pickup button should be displayed

  Scenario: TC109 - Verify user gets the message to place Ship cookies order ,in case user does not finds store while placing Delivery/Pickup /Catering Order
    Given user clicks on Order button
    When user enters the address
      | address | 2345 U.S. 1, Sullivan, ME |
    Then user is not able to locate store displaying message displayed

  Scenario: TC117_1 Logged in user - Verify that tracking ID present in confirmation mail should navigate to tracking page for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    And user clicks on Tracking ID link on Order confirmation page
    Then Tracking ID should navigate to tracking page

  Scenario: TC117_2 Logged in user - Verify that tracking ID present in confirmation mail should navigate to tracking page for pickup order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    And user clicks on Tracking ID link on Order confirmation page
    Then Tracking ID should navigate to tracking page

  #################################Add more Items button- Delivery Order############################################
  Scenario: TC118 - Logged in user - Verify user is able to see  Add more items button on checkout page for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then user should be able to see Add more items button on Checkout page

  Scenario: TC119 - Logged in user - Verify user redirects to Menu page on clicking  Add more items button on checkout page for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user clicks on Add more items button on Checkout page
    Then user should redirect to Menu page


  Scenario: TC120 - Logged in user - Verify user is able to add items from menu after clicking on Add more items and place order successfully for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user clicks on Add more items button on Checkout page
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart again
    Then product added after clicking add more items button should be present in the cart

  ################################Add more Items button- Pickup Order###################################################
  Scenario: TC121 - Logged in user - Verify user is able to see  Add more items button on checkout page for pickup order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then user should be able to see Add more items button on Checkout page

  Scenario: TC122 - Logged in user - Verify user redirects to Menu page on clicking  Add more items button on checkout page for pickup order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user clicks on Add more items button on Checkout page
    Then user should redirect to Menu page


  Scenario: TC123 - Logged in user - Verify user is able to add items from menu after clicking on Add more items and place order successfully for pickup order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user clicks on Add more items button on Checkout page
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart again
    Then product added after clicking add more items button should be present in the cart


  Scenario: TC130 Verify radio options - Continue As Guest,Register After Checkout,Already Registered does not appear if user checkouts as Logged in user

    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then radio options - Continue As Guest,Register After Checkout,Already Registered should disappear


  Scenario: TC132 Verify radio options - Continue As Guest,Register After Checkout,Already Registered does not appear if user checkouts as Logged in user

    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then radio options - Continue As Guest,Register After Checkout,Already Registered should disappear
    

  ############### Pickup - checkout #########################################
  Scenario: TC139 Logged in user - Verify navagating away from cart and then coming back to the cart the button should say Checkout
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user navigates back
    And user goes to the cart
    Then The button in the cart says Checkout


  Scenario: TC140 Logged in user - Verify cart does not get cleared when user Navigates back from Location tab provided user has added item to the cart

    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on Locations tab
    And user goes to the cart again
    Then product should be present
 
  #Scenario: TC142 Logged in user - Verify stores not yet launched shows message -Coming Soon!
  #Given user clicks on Order button
  #When user enters the address
  # | address | 545 South Hoghland Avenue, Pittsburgh, PA |
  #Then Stores not yet launched should displayed message "Coming Soon!"

  Scenario: TC144 Logged in user - Verify user is able to request notification on Email for store not yet launched
    Given user clicks on Order button
    When user enters the address
      | address | 2116 Hawkins Street, Charlotte, NC |
    And user clicks on Request Notification
    And user enters emailID to receive notification
      | emailID | icprateeknehra@gmail.com |
    And user clicks submit
    Then popup should appear saying "You will be sent a notification when this store opens back up!"

  Scenario: TC145 Logged in user - Verify user is able to request notification on Phone for stores not yet launched
    Given user clicks on Order button
    When user enters the address
      | address | 2116 Hawkins Street, Charlotte, NC |
    And user clicks on Request Notification
    And user selects radio button : phone to request notification
    And user enters phone number
      | phone | 9876543210 |
    And user clicks submit
    Then popup should appear saying "You will be sent a notification when this store opens back up!"

 
  Scenario: TC148 Logged in user - Verify user is not able to submit request notification with wrong emailID format if he enters invalid email format then pop up should appear to enter correct email

    Given user clicks on Order button
    When user enters the address
      | address | 2116 Hawkins Street, Charlotte, NC |
    And user clicks on Request Notification
    And user enters emailID to receive notification
      | emailID | icprateeknehra@gmail |
    And user clicks submit
    Then popup should appear saying Please enter a valid email in the text box

  Scenario: TC149 Logged in user - Verify request notification phone textbox should not accept less than 10 digits
    Given user clicks on Order button
    When user enters the address
      | address | 2116 Hawkins Street, Charlotte, NC |
    And user clicks on Request Notification
    And user selects radio button : phone to request notification
    And user enters phone number
      | phone | 98765432 |
    And user clicks submit
    Then popup should appear saying Please enter a phone number in a standard format with the 3-digit area code such as xxx-xxx-xxxx
 
  Scenario: TC162 -  Logged in user - Verify when product is added ot the Cart, the text currently displayed in the existing �Order� button will be changed to display �Cart�

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    Then order button should change to Cart button


  Scenario: TC163 -  Logged in user - Verify when user selects delivery option on order page and move to cart then delivery option should be selected in cart also

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    Then Delivery button should be highlighed in purple color
    

  Scenario: TC164 -  Logged in user - Verify when user selects pickup option on order page and move to cart then pickup option should be selected in cart also

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Pickup time & date displayed on the cart top
    Then Pickup button should be highlighed in purple color


  Scenario: TC165 -  Logged in user - Verify when user selects pickup option on order page and changes to delivery option in cart by giving different address then a pop should appear stating -This address isn't in this store's delivery zone.  Please enter a new address if the entered address in not in delivery zone

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Pickup time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    And user goes to address search box in the cart
    And user enters the address in the cart
      | new delivery address | 1084 Lancaster Avenue, Bryn Mawr, PA |
    Then popup appears saying - This address isnt in this store's delivery zone. Please enter a new address
    

  Scenario: TC166 -  Logged in user - Verify when user selects delivery option on order page and changes store name in cart for delivery then a pop up should appear stating -This address isnt in this store's delivery zone if the entered address in not in delivery zone

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user goes to address search box in the cart
    And user enters the address in the cart
      | new delivery address | 1084 Lancaster Avenue, Bryn Mawr, PA |
    Then popup appears saying - This address isnt in this store's delivery zone. Please enter a new address

  Scenario: TC167 -  Logged in user - Verify  when user changes option from delivery to pickup the address box for delivery should be removed

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Pickup button in the cart
    Then Address box for delivery should be removed

  Scenario: TC168 -  Logged in user - Verify when user deletes all the items from the cart then the Cart button should change to Order button to allow users to order again.

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user deletes the items from the cart
    Then Cart button should change to Order button


   Scenario: TC172 - Logged in user - Verify that when user is selecting delivery button, user should see Address search box for delivery address in cart
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    Then user should able to see search box option in cart for delivery

  Scenario: TC173 -  Logged in user - Verify that user is able to search for delivery address in cart
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user goes to address search box in the cart
    And user enters the address in the cart
      | new delivery address | 4268 Manayunk Ave, Philadelphia, PA 19128 |
    Then popup should appear saying - Address updated

  Scenario: TC174 -  Logged in user - Verify that user should get a pop up - Order is scheduled for pickup, when selecting pickup button in cart

    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on cart Pickup button
    And user clicks Update button
    Then popup should appear stating - Order is scheduled for pickup

 

  Scenario: TC175 -  Logged in user - Verify that when user changes from delivery order to pickup in cart then pickup page details like Full Name, Email and Phone textbox should appear on checkout page.

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Pickup button in the cart
    And user clicks Update button
    And user clicks on the checkout button
    Then checkout page should display details as per pickup order


Scenario: TC191 -  Logged in user - Verify that when user changes from pickup to delivery order in cart, then delivery page details should appear on checkout page.

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    And user clicks Update button
    And user clicks on the checkout button
    Then checkout page should display details as per delivery order
 

   Scenario: TC192 -  Logged in user - Verify that user should get a pop up - Order is scheduled for delivery, when selecting delivery button in cart

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    And user clicks Update button
    Then popup should appear stating - Order is scheduled for delivery
 
   Scenario: TC194 -  Logged in user - Verify that the shopping cart image display inside the Cart button when product added to cart
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    Then cart image should be displayed inside Cart button

  Scenario: TC195- Logged in user - Verify when user selects the Cart button ,it allows user to edit the cart
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the added product in the cart
    And user selects a quantity from the dropdown
    And user clicks update product button
    Then updated quantity should reflect in the cart


  Scenario: TC196-  Logged in user - Verify the address remains same after changing order from pickup to delivery in cart and clicking on checkout button

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    And user clicks Update button
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Prateek Nehra     |
      | customer phone   |        3457689024 |
      | customer emailID | icprateeknehra@gmail.com  |
    And user selects Payment Method as cash
    And user clicks on Place Order
    Then Delivery address should be displayed as mentioned below:
      | Delivery address | Tuscaloosa, AL |

 
  Scenario: TC197-  Logged in user - Verify the address remains same after changing order from delivery to pickup in cart and clicking on checkout button
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Pickup button in the cart
    And user clicks Update button
    And user clicks on the checkout button
    #And user enters into your Info
    #| customer name    | Prateek Nehra    |
    #| customer phone   |       3457689024 |
    #| customer emailID | pnehra@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Pickup address should be displayed as mentioned below:
      | Delivery address | Tuscaloosa, AL |

  Scenario: TC199-  Logged in user - Verify user is able to see the delivery and pickup option in Cart after clicking on edit option present in cart to edit the pickup/delivery date and time.

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    Then Pickup and Delivery buttons should be present in the cart

  Scenario: TC200-  Logged in user - Verify when user Clicks on cart button then, Pickup/Delivery option should not be present
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    Then Pickup and Delivery buttons should not be displayed in the cart


  Scenario: TC201-  Logged in user - Verify when user selects store which has only pickup option on order page then in cart only pickup option should be present

    Given user clicks on Order button
    When user enters the address
      | address | 1319 Anderson Ave |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click The Sugar Rush
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    Then Only Pickup buttons should be present in the cart


  Scenario: TC204-  Logged in user - Verify if user changes order from delivery to pickup in cart then after placing order when user tracks the order it should show pickup tracking information

    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Pickup button in the cart
    And user clicks Update button
    And user clicks on the checkout button
    #And user enters into your Info
    #  | customer name    | Shaurya Nigam    |
    #  | customer phone   |       3457689024 |
    #  | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    And user clicks on Track Order button
    Then Delivery address should be displayed on tracking page as mentioned below:
      | Delivery address | Tuscaloosa, AL |


  Scenario: TC205_1-  Logged in user - Verify when user changes order from pickup to delivery and vice versa should get updated on clicking the updated button in cart

    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user clicks on Delivery button in the cart
    And user clicks Update button
    Then Delivery should be displayed on the cart top


  Scenario: TC205_2-  Logged in user - Verify when user changes order from delivery to pickup and vice versa should get updated on clicking the updated button in cart

    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user changes date
    And user changes time
    And user clicks on Pickup button in the cart
    And user clicks on Pickup button in the cart
    Then Pickup should be displayed on the cart top
    
    
    Scenario: TC209 - Logged in user - Map button should be clickable and map should open
  	Given user clicks on Order button
    When user enters the address
      | address | 1084 lancaster avenue bryn mawr, PA |
    And verify Map button is displayed
    When Click on Map button
    Then Verify Map gets open
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
     And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary 

    
    Scenario: TC210 - Logged in user - Map should get closed by clicking on Hide Map button
    Given user clicks on Order button
    When user enters the address
      | address | 1084 lancaster avenue bryn mawr, PA |
    And verify Map button is displayed
    When Click on Map button
    Then Verify Map gets open
    And click on Hide Map button
    Then verify Map is closed



#@DevOrderLogin
  #Scenario: TC187-  Logged in user - Verify guest user is able to update pickup time from the cart for Pickup Order
    #Given user clicks on Order button
    #When user enters the address
      #| address | 4319 Main St Philadelphia, PA |
    #And user clicks on Pickup button
    #And user select Date and Time from Calendar
    #And user clicks on Continue
    #And On Menu Page click on The Major Rager
    #And user clicks on add product
    #And user goes to the cart
    #And user clicks on Pickup time & date displayed on the cart top
    #And user changes date
    #And user changes time
    #And user clicks Update button
    #Then updated pickup date & time should be displayed on the cart top


  #Scenario: Verify cart items are cleared if user log out

    Scenario: TC050 - Logged in user - Verify user is able to add Cookie Dough to a Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address |  1525 West Tennessee Street Tallahassee, FL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on cookie dough button
    Then Cookie Dough should be added in the cart