@OrderGuest
Feature: OrderGuestPart1

  Background: user access to website
    Given User opens the Website
  
  #| url | https://5.stage.insomniacookies.com/ |

  Scenario: TC001 - Guest User - Verify User is able to place an Delivery order with Credit card
  And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Prateek Nehra     |
      | customer phone   |        3457689024 |
      | customer emailID | icprateeknehra@gmail.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
  
  #Scenario: TC002 - Guest User - Verify User is able to place a Delivery order with Cash
    #Given user clicks on Order button
    #When user enters the address
      #| address | 1130 Universiy Blvd, Tuscaloosa, AL |
    #And user clicks on Delivery button
    #And user select Date and Time from Calendar
    #And user clicks on Continue
    #And On Menu Page click on The Insomniac
    #And user clicks on add product
    #And user goes to the cart
    #And user clicks on the checkout button
    #And user enters Valid Details under Delivery Info & your Info
      #| recepient name   | Theresa Ainsworth |
      #| recepient phone  |        2345678981 |
      #| customer name    | Prateek Nehra     |
      #| customer phone   |        3457689024 |
      #| customer emailID | icprateeknehra@gmail.com  |
    #And user enter delivery message
      #| instructions | deliver fresh warm cookies |
    #And user selects Payment Method as cash
    #And user clicks on Place Order
    #Then Order confirmation page should appear displaying Order summary
    

  Scenario: TC003 - Guest User - Verify User is able to place an Delivery order with School Cash
    Given user clicks on Order button
    When user enters the address
      | address | 421 E Beaver Ave, State College  pa |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Prateek Nehra    |
      | recepient phone  |       2345678981 |
      | customer name    | Test Nehra		    |
      | customer phone   |       3457689024 |
      | customer emailID | icprateeknehra@gmail.com |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as School cash
    And user enters school cash number
      | School cash | 6020000000000000 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

  Scenario: TC004 - Guest User - Verify User is able to place an Delivery order with Gift Card
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Prateek Nehra    |
      | recepient phone  |       2345678981 |
      | customer name    | Prateek Nehra    |
      | customer phone   |       3457689024 |
      | customer emailID | icprateeknehra@gmail.com |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
 

  Scenario: TC005 - Guest User - Verify User is able to place a Pickup order with Credit card
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Prateek Nehra    |
      | customer phone   |       3457689024 |
      | customer emailID | icprateeknehra@gmail.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  #Scenario: TC006 - Guest User - Verify User is able to place a Pickup order with Cash
    #Given user clicks on Order button
    #When user enters the address
      #| address | 1130 Universiy Blvd, Tuscaloosa, AL |
    #And user clicks on Pickup button
    #And user select Date and Time from Calendar
    #And user clicks on Continue
    #And On Menu Page click on The Insomniac
    #And user clicks on add product
    #And user goes to the cart
    #And user clicks on the checkout button
    #And user enters into your Info
      #| customer name    | Prateek Nehra   |
      #| customer phone   |       3457689024 |
      #| customer emailID | icprateeknehra@gmail.com |
    #And user selects Payment Method as cash
    #And user clicks on Place Order
    #Then Order confirmation page should appear displaying Order summary


  Scenario: TC007 - Guest User - Verify User is able to place a Pickup order with School Cash
    Given user clicks on Order button
    When user enters the address
      | address | 421 E Beaver Ave, State College  pa |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Prateek Nehra    |
      | customer phone   |       3457689024 |
      | customer emailID | icprateeknehra@gmail.com |
    And user selects Payment Method as School cash
    And user enters school cash number
      | School cash | 6020000000000000 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC008 - Guest User - Verify User is able to place a Pickup order with Gift Card
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Prateek Nehra    |
      | customer phone   |       3457689024 |
      | customer emailID | icprateeknehra@gmail.com |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
    Scenario: TC010 - Guest User - Verify User is able to track a Delivery Order just after Ordering
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Prateek Nehra     |
      | customer phone   |        3457689024 |
      | customer emailID | icprateeknehra@gmail.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
     And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    And user clicks on Track Order button
    Then Tracking ID should be displayed along with status
    
 
  Scenario: TC011 - Guest User - Verify User is able to track a Pickup Order just after Ordering
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Prateek Nehra    |
      | customer phone   |       3457689024 |
      | customer emailID | icprateeknehra@gmail.com |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    And user clicks on Track Order button
    Then Tracking ID should be displayed along with status

 
	  Scenario: TC015 - Guest User - Verify guest user is not able to add Cookie Dough to a Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then Cookie Dough button should not appear to guest user in the cart


  Scenario: TC016 - Guest User - Verify loyalty point earned for Delivery total under 20$ calculates to 1$ = 1point
    Given user clicks on Order button
    When user enters the address
      | address |  65 Mt. Auburn St, Cambridge MA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Choco Chunk
    And user goes to the cart
    Then Cart should state, You are not logged in! You could be earning One points for this order
    

  Scenario: TC017 - Guest User - Verify loyalty point earned for Delivery total above 20$ calculates to 1$ = 1.25 point
    Given user clicks on Order button
    When user enters the address
    | address | 217 Welch Avenue, Ames, IA |  
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then Cart should state, You are not logged in! You could be earning Thirty Two points for this order
	

  Scenario: TC018 - Guest User - Verify loyalty point earned for Pickup total under 20$ calculates to 1$ = 1point
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave South |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Choco Chunk
    And user goes to the cart
    Then Cart should state, You are not logged in! You could be earning One points for this order

  

  Scenario: TC019 - Guest User - Verify loyalty point earned for Pickup total above 20$ calculates to 1$ = 1.25 point
    Given user clicks on Order button
    When user enters the address
      | address | 217 Welch Ave |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then Cart should state, You are not logged in! You could be earning Thirty Two points for this order
    

  Scenario: TC020 - Guest User - Verify guest user is not able to apply Product coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave South |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Product code | 2Free |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear


  Scenario: TC021 - Guest User - Verify guest user is not able to apply Amount coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave South |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

  Scenario: TC022 - Guest User - Verify guest user is not able to apply Percentage coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Percentage code | CYBER |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear
 
  Scenario: TC023 - Guest User - Verify guest user is not able to apply Free Delivery coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Free Delivery code | DELIVERY10 |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear
    

  Scenario: TC024 - Guest User - Verify guest user is not able to apply Product coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Product code | 2Free |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

  Scenario: TC025 - Guest User - Verify guest user is not able to apply Amount coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

  Scenario: TC026 - Guest User - Verify guest user is not able to apply Percentage coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Percentage code | CYBER |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear


  Scenario: TC027 - Guest User - Verify guest user is not able to place an Delivery order beyond 6 months
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Delivery button
    And user selects date from calender beyond six months but is unable to do so

    
  Scenario: Verify guest user is not able to apply Free Delivery coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Free Delivery code | DELIVERY10 |
    And user clicks Apply button
    Then Error popup stating You must be logged in to use this coupon should appear

  Scenario: TC028 - Guest User - Verify guest user is not able to place an Delivery order for past date
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Delivery button
    And user selects date from calender
    Then user should not be able to select back date

  Scenario: TC029 - Guest User - Verify guest user is not able to place an Pickup order beyond 6 months
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Pickup button
    And user selects date from calender beyond six months but is unable to do so

  Scenario: TC030 - Guest User - Verify guest user is not able to place an Pickup order for past date
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Pickup button
    And user selects date from calender
    Then user should not be able to select back date


  Scenario: TC031 - Guest User - Verify user is able to delete items from cart and add them again into it for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user deleted the product
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then product should be present


  Scenario: TC032 - Guest User - Verify user is able to delete items from cart and add them again into it for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 304 West 14th Street New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user deleted the product
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then product should be present


  Scenario: TC033 - Guest User - Verify Upsell is displayed for the Delivery orders
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then upsell should be displayed


  Scenario: TC034 - Guest User - Verify Upsell is displayed for the Pickup orders
    Given user clicks on Order button
    When user enters the address
      | address | 482 Third Avenue New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then upsell should be displayed


  Scenario: TC035 - Guest User - verify user is able to close menu page for every product
    Given user clicks on Order button
    When user enters the address
      | address | 482 Third Avenue New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on  close button
    Then product menu should close


  Scenario: TC049 - Guest User - Verify guest user is able to set tip for an Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 228 E Clayton St, Athens GA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Prateek Nehra     |
      | customer phone   |        3457689024 |
      | customer emailID | icprateeknehra@gmail.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user selects tip
      | tip selected | 1.00 |
    And validation popup appears saying tip updated
    And user clicks on Place Order
    Then Order confirmation page should appear displaying tip in Order summary
      | tip displayed | $1.00 |

  Scenario: TC081 - Guest User - Verify that guest user is getting a pop up stating that Delivery instructions saved when entering data in delivery instruction textbox
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    Then User should get a pop up stating that Delivery instructions saved


  Scenario: TC082 - Guest User - Verify that guest user is getting a pop up stating that Message successfully added when entering data in Message for recipient textbox
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters message for recepient
      | message | This is for you |
    Then User should get a pop up stating that Message successfully added


  Scenario: TC083 - Guest User - Verify that user is getting pop up after adding product in cart
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    Then User should get a pop up stating Product Name added to cart

  Scenario: TC084 - Guest User - Verify guest user is able to update delivery time from the cart for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user changes date
    And user changes time
    And user clicks Update button
    Then updated date & time should be displayed on the cart top

  Scenario: TC085 - Guest User - Verify guest user is able to login from the cart
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on login button present in the cart
    When User enters valid Email and Password
      | username | icprateeknehra@gmail.com |
      | password | Judge@123         |
    And Click on the Log In button
    Then User must get logged in and UserName should be displayed in the header


  Scenario: TC086 - Guest User - Verify guest user is able to update pickup time from the cart for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on Pickup time & date displayed on the cart top
    And user changes date
    And user changes time
    And user clicks Update button
    Then updated pickup date & time should be displayed on the cart top


  Scenario: TC115 - Guest User - Verify that tracking ID present in confirmation mail should navigate to tracking page for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Prateek Nehra     |
      | customer phone   |        3457689024 |
      | customer emailID | icprateeknehra@gmail.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    And user clicks on Tracking ID link on Order confirmation page
    Then Tracking ID should navigate to tracking page

  Scenario: TC117 - Guest User - Verify that tracking ID present in confirmation mail should navigate to tracking page for pickup order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Prateek Nehra    |
      | customer phone   |       3457689024 |
      | customer emailID | icprateeknehra@gmail.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    And user clicks on Tracking ID link on Order confirmation page
    Then Tracking ID should navigate to tracking page

  #################################Add more Items button- Delivery Order############################################

  Scenario: TC118 - Guest User - Verify user is able to see  Add more items button on checkout page for delivery order
    Given user clicks on Order button
    When user enters the address
      | address | 1130 Universiy Blvd, Tuscaloosa, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then user should be able to see Add more items button on Checkout page

  
