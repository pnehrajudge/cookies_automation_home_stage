package com.qa.hooks;

//import java.io.File;
import java.io.IOException;

//import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;
import com.qa.helper.LoggerHelper;
import com.qa.pages.AdminStores;
import com.qa.utils.TestBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class ServiceHooks {
	
	TestBase testBase = new TestBase();
	WebDriver driver = TestBase.getDriver();
	AdminStores adminStores = new AdminStores(driver);

	Logger log = LoggerHelper.getLogger(ServiceHooks.class);

	 @Before
	 public void beforeScenario(Scenario scenario) throws InterruptedException {
//		 adminStores.disableReCaptcha();
	 }

	@After
	public void endTest(Scenario scenario) {
		if (scenario.isFailed()) {
			String screenshotName = scenario.getName().replaceAll(" ", "_");
			try {
				String srcPath = CaptureScreen(TestBase.driver);
				Reporter.addScreenCaptureFromPath(srcPath);
				scenario.embed(((TakesScreenshot) TestBase.driver).getScreenshotAs(OutputType.BYTES), "image/png" );
				
//				log.info(scenario.getName() + " is Failed");
//				//This takes a screenshot from the driver and save it to the specified location
//				File sourcePath = ((TakesScreenshot) TestBase.driver).getScreenshotAs(OutputType.FILE);
//				
//				//Building up the destination path for the screenshot to save
//				//Also make sure to create a folder 'screenshots' with in the cucumber-report folder
//				//File destinationPath = new File("/target/cucumber-reports/screenshots/" + screenshotName + ".png");
//				File destinationPath = new File(System.getProperty("user.dir") + "/target/cucumber-reports/screenshots/" + screenshotName + ".png");
//				//File destinationPath = new File("/Cookies_5fAutoMation_5fReport/screenshots/" + screenshotName + ".png");
//				//Copy taken screenshot from source location to destination location
//				//Files.copy(sourcePath, destinationPath);
//				FileUtils.copyFile(sourcePath, destinationPath.getAbsoluteFile());
//				// ########FileUtils.copyFile(sourcePath, destinationPath);
//              //This attach the specified screenshot to the test
//			    Reporter.addScreenCaptureFromPath(destinationPath.toString());			    
			    
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
//			String screenshotName = scenario.getName().replaceAll(" ", "_");
//			try {
//				log.info(scenario.getName() + " is pass");
//				File sourcePath = ((TakesScreenshot) TestBase.driver).getScreenshotAs(OutputType.FILE);
//
//				File destinationPath = new File(System.getProperty("user.dir") + "/target/cucumber-reports/screenshots/" + screenshotName + ".png");
//				FileUtils.copyFile(sourcePath, destinationPath);
//			    Reporter.addScreenCaptureFromPath(destinationPath.toString());
//				
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
		}


//		TestBase.driver.quit();
	}
	
	
	public static String CaptureScreen(WebDriver driver){
	     TakesScreenshot newScreen = (TakesScreenshot) driver;
	     String scnShot = newScreen.getScreenshotAs(OutputType.BASE64);
	     return "data:image/png;base64, " + scnShot ;
	}
	
	

}
