package com.qa.stepDefinition;

import static org.junit.Assert.assertThat;

import java.util.List;

import org.apache.log4j.Logger;
import org.hamcrest.CoreMatchers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.Locations;
import com.qa.pages.Order;
import com.qa.pages.SignIn;
import com.qa.pages.SignUp;
import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
//import junit.framework.Assert;
import org.junit.Assert;


public class Step_Location extends TestBase{
	
	
	WebDriver driver = getDriver();

	Order order = new Order(driver);
	SignUp signup = new SignUp(driver);
	SignIn s = new SignIn(driver);
	Locations loc =new Locations(driver);
	
	// Logger log= Logger.getLogger(step_Order.class);
	
	Logger log= Logger.getLogger("shauryaLogger");

	WebDriverWait wait = new WebDriverWait(driver, 30);
	
	@Then("^map and search field should be displayed on the page$")
	public void map_and_search_field_should_be_displayed_on_the_page() throws Throwable{
		
		wait.until(ExpectedConditions.elementToBeClickable(loc.search_box));
		Assert.assertTrue(loc.search_box.isDisplayed());
		log.debug("search button is displayed");
		wait.until(ExpectedConditions.elementToBeClickable(loc.map));
		Assert.assertTrue(loc.map.isDisplayed());
		log.debug("map is displayed");
	}

	@And("^user clicks search button$")
	public void user_clicks_search_button() throws Throwable{
			Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(loc.search_button));
		loc.search_button.click();
		log.debug("search button is displayed");
		
	}
	
	@When("^user enters the address in location search box$")
	public void address_location(DataTable table) throws Throwable{
		
		List<List<String>> data= table.raw();
		wait.until(ExpectedConditions.elementToBeClickable(loc.search_box));
		loc.search_box.sendKeys(data.get(0).get(0));
		log.debug("Address is entered in location search box");
		wait.until(ExpectedConditions.elementToBeClickable(loc.suggestion));
		loc.suggestion.click();
		log.debug("suggestion is clicked");
		
		
		
	}
	
	@Then("^Nearest store locations should be displayed$")
	public void nearest_stores(DataTable table) throws Throwable{
		List<List<String>> data= table.raw();
		wait.until(ExpectedConditions.visibilityOfAllElements(loc.nearest_stores));
	    int i=0;
		for(WebElement e : loc.nearest_stores)
		{

			Assert.assertEquals(data.get(i).get(0), e.getText());
			System.out.println(data.get(i).get(0) +":"+ e.getText() );
			log.info(data.get(i).get(0) +":"+ e.getText());
			i=i+1;

		}
			
			
	}
	
	@Then("^distance of nearest stores should be displayed$")
	public void nearest_stores_distance() throws Throwable{
		
		wait.until(ExpectedConditions.visibilityOfAllElements(loc.distance));
	    
		for(WebElement e : loc.distance)
		{

			Assert.assertTrue(e.isDisplayed());
			System.out.println("Distances of nearest store are :"+ e.getText());
			log.info("Distances of nearest store are :"+ e.getText());
			
		}
		}
		
		@Then("^info links should be displayed$")
		public void nearest_stores_info_links() throws Throwable{
			
			wait.until(ExpectedConditions.visibilityOfAllElements(loc.info_links));
		    
			for(WebElement e : loc.info_links)
			{

				Assert.assertTrue(e.isDisplayed());
				System.out.println("Store info links of nearest store are :"+ e.getText());
				log.info("Store info links of nearest store are :"+ e.getText());
				

			}
		
		 
		
			
			
	}
	
		@Then("^Addresses of the nearest store should be displayed$")
		public void Addresses_of_the_nearest_store_should_be_displayed() throws Throwable{
			
			wait.until(ExpectedConditions.visibilityOfAllElements(loc.addresses));
		    
			for(WebElement e : loc.addresses)
			{

				Assert.assertTrue(e.isDisplayed());
				System.out.println("Addresses of nearest store are :"+ e.getText());
				log.info("Addresses of nearest store are :"+ e.getText());
				

			}
		
		 
		
			
			
	}
		@Then("^info link of each store displayed is clickable$")
		public void click_info_links() throws Throwable{
wait.until(ExpectedConditions.visibilityOfAllElements(loc.info_links));
		    
			for(WebElement e : loc.info_links)
			{

				e.click();
				log.debug("info link for store is clicked");
				wait.until(ExpectedConditions.visibilityOf(loc.store_name_info));
				log.debug("waiting for store name to appear in store info");
				Assert.assertTrue(loc.store_name_info.isDisplayed());
				wait.until(ExpectedConditions.elementToBeClickable(loc.close));
				Thread.sleep(1000);
				loc.close.click();
				System.out.println("Store info links of nearest store are :"+ e.getText());
				log.info("Store info links of nearest store are :"+ e.getText());
				Thread.sleep(1000);
				

			}
		
			
		}
		
		@Then("^store info page should display  Delivery Location, Address, Contact No, Delivery hours, Retailing Hours, Order Now button, Store Image$")
		public void store_info_page() throws Throwable{
			
			wait.until(ExpectedConditions.visibilityOf(loc.store_name_info));
			Assert.assertTrue(loc.store_name_info.isDisplayed());
			log.info("Store name is displayed");
			
			wait.until(ExpectedConditions.visibilityOf(loc.delivery_location));
			Assert.assertTrue(loc.delivery_location.isDisplayed());
			log.info("Delivery Location is displayed");
			
			wait.until(ExpectedConditions.visibilityOf(loc.store_address));
			Assert.assertTrue(loc.store_address.isDisplayed());
			log.info("Store Address is displayed");
			
			wait.until(ExpectedConditions.visibilityOf(loc.contact_no));
			Assert.assertTrue(loc.contact_no.isDisplayed());
			log.info("Contact No. displayed is :" +loc.contact_no.getText());
			
			wait.until(ExpectedConditions.visibilityOf(loc.retail_hours));
			assertThat(loc.retail_hours.getText(), CoreMatchers.containsString("Retail Hours"));
			System.out.println(loc.retail_hours.getText());
			log.info("Retail hours are displayed :"+loc.retail_hours.getText());
			
			wait.until(ExpectedConditions.visibilityOf(loc.delivery_hours));
			assertThat(loc.delivery_hours.getText(), CoreMatchers.containsString("Delivery Hours"));
			System.out.println(loc.delivery_hours.getText());
			log.info("Delivery hours are displayed :"+loc.delivery_hours.getText());
		}
	
	@And("^user clicks info link of store$")
	public void click_info_link() throws Throwable{
		
		wait.until(ExpectedConditions.elementToBeClickable(loc.info_link));
		loc.info_link.click();
		log.info("info link is clicked");
	}
	
	@And("^user clicks Order now button$") 
	public void click_order_now_button() throws Throwable{
	
		wait.until(ExpectedConditions.elementToBeClickable(loc.order_now));
		loc.order_now.click();
		log.info("order now button is clicked");
	}

}
