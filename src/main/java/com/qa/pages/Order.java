package com.qa.pages;

import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

public class Order extends TestBase{
	
	
	@FindBy(xpath = "//a[@id='order-button']")
	public WebElement order_button;
	
	@FindBy(xpath = "//button[contains(text(),'Okay')]")
	public WebElement change_store_alert_button;
	
	@FindBy(xpath = "//input[@id='addresstext']")
	public WebElement address_box;
	
	@FindBy(xpath = "//*[contains(@id,'location')]/div/div[4]/div[2]/div")
	public WebElement addressVerificationDeliveryBtn;
	
	@FindBy(xpath = "//*[@id=\"stores-carousel\"]/div/div/div[1]/div/div/h5")
	public WebElement storeName;
	
	//  //button[@class='btn btn-primary btn-block btn-store btn-store-delivery']
	//  //div[@class='btn-store-override btn-store-delivery'] 
	@FindBy(xpath = "//button[@class='btn btn-primary btn-block btn-store btn-store-delivery ae-button']")
	public WebElement delivery_button;
	
	
	
	@FindBy(xpath = "//*[@class='btn-store-override btn-store-delivery']") //button[@class='btn btn-primary btn-block btn-store btn-store-delivery']
	public WebElement alt_delivery_button;
	
	@FindBy(xpath = "/html[1]/body[1]/div[2]/section[1]/section[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/div[1]/select[1]")
	public WebElement time_dropdown;
	
	@FindBy(xpath = "//select[@class='delivery-timepicker-dropdown custom-select']")
	public WebElement alt_time_dropdown;
	
	
	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	public WebElement Continue;
	
	@FindBy(xpath = "//button[@class='btn btn-primary btn-block btn-lg mt-0 mt-sm-4 btn-datetime-submit']")
	public WebElement alt_Continue;
	
	//   //div[@class='modal fade modal--address-confirmation show']//button[contains(text(),'Continue')]
	
	//*[@id="deals"]/div/div/div/div[3]/div/div[1]/img
	@FindBy(xpath = "//*[@id='deals']/div/div/div/div[3]/div/div[1]/img")
	public WebElement sugar_rush_pic;
	
	@FindBy(xpath = "//h4[@class='modal-title']")
	public WebElement confirm_changing_store;
	
	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	public WebElement continue_alert;
	
	@FindBy(xpath = "/html[1]/body[1]/div[2]/div[2]/div[1]/div[1]/div[3]/button[2]")
	public WebElement alt_continue_alert;
	
	@FindBy(xpath = "/html/body/div[9]/div/div/div[2]/div/div[2]/button")
	public WebElement okay_alert;
	
	
	//  /html/body/div[2]/div[2]/div/div/div[3]/button[2]
	
	@FindBy(xpath = "//h5[contains(text(),'Sixpack')]")
	public WebElement sixpack;
	
	@FindBy(xpath = "//div[@id='deals']//div[2]//div[1]//div[1]//img[1]")
	public WebElement sixpack_pic;
	
	@FindBy(xpath = "//h5[(text()='Chocolate Chunk')]//ancestor::div[contains(@class,'col-sm col-md')]/following-sibling::div")    //h5[text()='Chocolate Chunk']
	public WebElement choco_chunk;
	
	@FindBy(xpath = "//div[@class='container']//div[1]//div[1]//div[1]//div[1]//div[1]//div[1]//div[1]//img[1]")
	public WebElement delicious_duo;
	
	@FindBy(xpath = "//*[@id=\"traditional-cookies\"]/div/div[1]/div/div[2]/div/div[2]/button")
	public WebElement chocolate_Chunk;
	
	@FindBy(xpath = "/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/button[1]/*")
	public WebElement alt_choco_chunk;
	
	
	@FindBy(xpath = "//button[contains(text(),'Pick For Me')]")
	public WebElement pick_for_me;
	
	@FindBy(xpath = "//button[contains(@class,'cart-button btn btn-primary')]")    //button[@class='cart-button btn btn-primary ae-button']
	public WebElement cart_button;
	
	@FindBy(xpath="//button[@class='cart-button btn btn-primary']")
	public WebElement new2_cart_button;
	//   //button[@class='cart-button btn btn-primary ae-button ae-form-field']
	
	@FindBy(xpath = "/html[1]/body[1]/nav[1]/div[1]/div[2]/ul[1]/li[7]/button[1]")
	public WebElement alt_cart_button;
	
	@FindBy(xpath = "//*[@id='navbar-component']/li[7]/button")
	public WebElement alt_cart_button_Stage18;
	
	@FindBy(xpath = "/html[1]/body[1]/nav[1]/div[1]/div[3]/ul[1]/li[7]/button[1]")
	public WebElement new_cart_button;
	
	
	
	
	
	@FindBy(xpath = "//i[@class='mr-2 fa fa-shopping-cart']")
	public WebElement cart_image;
	
	//  //button[@class='cart-button btn btn-primary']
	
	@FindBy(xpath = "//button[contains(text(),'Checkout')]")
	public WebElement checkout_button;
	
	@FindBy(xpath = "//input[@id='recipient-name']")
	public WebElement recipient_name;
	
	@FindBy(xpath = "//input[@id='recipient-phone']")
	public WebElement recipient_phone;
	
	@FindBy(xpath = "//textarea[@id='instructions']")
	public WebElement instructions;
	
	@FindBy(xpath = "//input[@id='customer-name']")
	public WebElement customer_name;
	
	@FindBy(xpath = "//input[@id='customer-phone']")
	public WebElement customer_phone;
	
	@FindBy(xpath = "//input[@id='customer-email']")
	public WebElement customer_email;
	
	@FindBy(xpath = "//a[@id='pills-credit-tab']")
	public WebElement credit_card;
	
	@FindBy(xpath = "//button[@id='chase-logged-out-add-new-card-btn']")
	public WebElement add_new_credit_card_button;
	
	@FindBy(xpath = "//*[@id='hpf-iframe']")
	public WebElement frame;
	
	@FindBy(xpath = "//*[@id='cc-number']")      //*[@id='ccNumber']
	public WebElement cc_num_new;
	
	@FindBy(xpath = "//input[contains(@class,'creCVV2Field')]")
	public WebElement cvv_num;
	
	@FindBy(xpath = "//input[@id='cc-number']")
	public WebElement cc_num;
	
	@FindBy(xpath = "//input[@id='cc-exp']") 
	public WebElement cc_exp;
	
	@FindBy(xpath = "//*[@id='cc-exp']")    //*[@id='expMonth']
	public WebElement cc_exp_DDmnth;
	
	@FindBy(xpath = "//*[@id='cc-exp']")  //*[@id='expYear']
	public WebElement cc_exp_DDyr;
	
	@FindBy(xpath = "//select[@id='payment-tip']")
	public WebElement tip;
	
	@FindBy(xpath = "//div[contains(text(),'Tip')]/following-sibling::div/span")  //  /html[1]/body[1]/div[2]/section[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[3]/div[4]/div[2]/span[1]
	public WebElement tip_selected;
	
	@FindBy(xpath = "//button[@id='submit-order-btn']")      //*[@id='chase-stored-card-place-order-btn']
	public WebElement place_order;
	
	
	@FindBy(xpath = "//*[@id='completeButton']")  //*[@id='completeButton']
	public WebElement iframe_place_order_button;
	
	@FindBy(xpath = "//select[@id='expYear']")
	public WebElement cc_year;
	
	@FindBy(xpath = "//h1[contains(text(),'THANKS!')]")
	public WebElement thanks;
	
	@FindBy(xpath = "//a[contains(text(),'#')]")
	public WebElement tracking_no;

	@FindBy(xpath = "//h4[contains(text(),'Insomnia Cookies - ')]")
	public WebElement store_name;
	
	@FindBy(xpath = "//b[@class='text-primary']")
	public WebElement product_ordered;
	
	@FindBy(xpath = "//b[contains(text(),'Major Rager (18 Traditional)')]")    //div[@class='scroll-wrap']//div[1]//div[2]//b---------//div[@class='scroll-wrap']//div[2]//div[1]//div[1]//div[2]   
	public WebElement product_added_through_add_more_items_button;
	
	@FindBy(xpath = "//a[@class='btn btn-primary btn-block my-2']")
	public WebElement track_order;
	
	@FindBy(xpath = "//h4[contains(text(),'Baking...')]")
	public WebElement tracker_delivery;
	
	@FindBy(xpath = "/html/body/div[2]/nav/form/div/span/div/div/div[1]")
	public WebElement address_suggestion;
	
	//Comment
	@FindBy(xpath = "//h5[contains(text(),'Insomniac (24 Traditional)')]")     
	//@CacheLookup
	public WebElement the_insomniac;
		
	
	@FindBy(xpath = "//div[@id='deals']//div[5]//div[1]//div[1]//img[1]" )   //div[@id='deals']//div[2]//div[1]//div[1]//img[1]
	public WebElement insomniac_pic;
	
	
	@FindBy(xpath = "//div[@id='deals']//div[3]//div[1]//div[1]//img[1]")
	public WebElement major_rager_pic;
	
	@FindBy(xpath = "//div[contains(@class,'container')]//div[1]//div[1]//div[1]//div[1]//div[3]//div[1]//div[1]//img[1]")
	public WebElement alt_major_rager_pic;
	
	@FindBy(xpath = "//a[@id='pills-cash-tab']")
	public WebElement cash;
	
	@FindBy(xpath = "//a[@id='pills-schoolcash-tab']")
	public WebElement school_cash;
	
	@FindBy(xpath = "//input[@id='school-cash']")
	public WebElement school_cash_details;
	
	@FindBy(xpath = "//h5[contains(text(),'Major Rager')]")   //*[@id=\"deals\"]/div/div/div[1]/div[4]/div/div[2]/div/div[1]/h5  
	public WebElement major_rager;
	 
	@FindBy(xpath = "//a[@id='pills-giftcard-tab']")
	public WebElement gift_card;
	
	@FindBy(xpath = "//input[@id='giftcard-number']")
	public WebElement gift_card_no;

    @FindBy(xpath = "//div[@class='col-6 mt-0 mt-sm-1']//button[@class='btn btn-primary btn-block btn-store btn-store-pickup ae-button'][contains(text(),'Pickup')]")
    public WebElement pickup;
    
    @FindBy(xpath = "//*[contains(@id,'location-1')]/div/div[4]/div/button") //*[@id='location-1071']/div/div[4]/div/button]  //button[@class='btn btn-primary btn-block btn-store btn-store-pickup ae-button']
    public WebElement alt_pickup;
    
    @FindBy(xpath = "//div[@class='col-md-6 col-lg-4 mt-xs-2 mt-md-0 location-div active']//div[@class='btn-store-override btn-store-pickup']")
    public WebElement only_pickupBtn;
    
    
    @FindBy(xpath = "//span[contains(text(),'Ready for pickup')]")
    @CacheLookup
    public WebElement pickup_verify;
    
    @FindBy(xpath = "//*[@id='order-id']")
    public WebElement tracker_no_verify;
    
    @FindBy(xpath = "//button[contains(text(),'Apply Cookie Dough $')]")
    public WebElement cookie_dough_button;
    
    @FindBy(xpath = "//button[contains(text(),'Cookie Dough')]")
    public WebElement cookie_dough;
    
    @FindBy(xpath = "//*[contains(text(),'-$0.00')]")
    public WebElement cookie_doughApplied;
    
    @FindBy(xpath = "//button[contains(text(),'Remove Cookie Dough')]")
    public WebElement remove_cookie_dough;
    
    @FindBy(xpath="//div[@class='loyalty-text mb-1']")
    public WebElement loyalty_points;
    
    @FindBy(xpath = "//i[@class='fa fa-lg fa-trash']")
    public  WebElement delete_product_button;
    
    @FindBy(xpath = "//div[@class='mt-1']//button[@type='button']")
    public  WebElement alt_delete_product_button;
    
    //   //input[@placeholder='Enter coupon code']
    //   
	
    @FindBy(xpath = "//*[@id=\'cart-side-panel-component\']/div/div[1]/div/div/div[2]/div[3]/div/div/input")   //  /html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/input[1]
    public WebElement coupon_box;
    
    
    
    @FindBy(xpath = "//div[@class='input-group-append']//button[@type='button']")
    public WebElement apply;
    
    @FindBy(xpath = "//*[@id=\"free-items-modal\"]/div/div/div[2]/div/div/div/div[2]/div/div[2]/div/div[3]/button")
    public WebElement free_cookies;
    
    @FindBy(css = "body.cart-open.modal-open:nth-child(2) div.modal.product-options-modal.show:nth-child(8) div.modal-dialog div.modal-content div.modal-body div.container div.row div.col-12.col-md-8.offset-md-2 div.form-group.text-opt:nth-child(2) div.row div.col-6:nth-child(2) div.custom-options-counter.d-flex.flex-row div.plus-btn button.btn-rounded-lg svg:nth-child(1) > path:nth-child(1)")
    public WebElement alt_free_cookie;
    
    @FindBy(xpath = "//span[contains(text(),'Remove')]")
    public WebElement remove_coupon_button;
    
    @FindBy(xpath = "//*[@id=\"free-items-modal\"]/div/div/div[3]/div/div/div/button")
    public WebElement add_free_items;
    
    @FindBy(xpath = "/html/body/div[3]/div/div/div[1]/div/div/div[2]/div[5]/div[1]/div/div/div[2]/b")
    public WebElement added_product_coupon;
    
    
    @FindBy(xpath = "//input[@id='delivery-datepicker']")
    public WebElement datepicker;
    
    @FindBy(xpath = "//td[contains(@class,'ui-datepicker-days-cell-over undefined ui-datepicker-current-day ui-datepicker-today')]")
    public WebElement current_date;
    
    @FindBy(xpath = "//span[@class='ui-icon ui-icon-circle-triangle-w']")
    public WebElement back_arrow;
    
    @FindBy(xpath = "//span[@class='ui-icon ui-icon-circle-triangle-e']")
    public WebElement forward_arrow;
    
    @FindBy(xpath = "//a[contains(text(),'12')]")
    public WebElement current_day;
    
    @FindBy(xpath = "//b[@class='text-primary']")
    public WebElement product_present;
    
    @FindBy(xpath = "//*[contains(@class,'img-fluid')]")
    public WebElement upsell;
    
    @FindBy(xpath = "//div[@class='col-12 col-sm-12']//span[@aria-hidden='true'][contains(text(),'close')]")
    public WebElement product_close;
    
    //  //*[@id='comments'] 
    
    @FindBy(xpath = "//textarea[@id='comments']")
    public WebElement message;
    
    @FindBy(xpath= "//div[@class='card clickable']")
    public WebElement edit_delivery;
    
    @FindBy(xpath = "//button[@id='order-type-delivery']")
    public WebElement cart_delivery_button;
    
    @FindBy(xpath = "//input[@id='cart-delivery-datepicker']")
    public WebElement cart_delivery_datepicker;
    
    @FindBy(xpath = "//select[@class='form-control cart-delivery-timepicker-dropdown']")
    public WebElement cart_delivery_timepicker;
    
    @FindBy(xpath = "//*[@id=\"cart-edit-time\"]/form/div[3]/button")  //div[@class='input-group mt-2 mb-3']//button[@class='btn btn-primary btn-block'][contains(text(),'Update')]
    public WebElement cart_update_button;
    
    @FindBy(xpath = "//span[@class='cart-delivery-text']")
    public WebElement edit_delivery_text;
    
    @FindBy(xpath = "//a[@class='badge badge-primary']")
    public WebElement cart_login;
    
    @FindBy(xpath = "//button[@id='add-more-items']")
    public WebElement add_more_button;
    
    @FindBy(xpath = "//*[@id='add-more-items']")
    public WebElement alt_add_more_button;
    
    @FindBy(xpath = "//h3[contains(text(),'Deals')]")
    public WebElement menu_page;
    
    @FindBy(xpath =  "//input[@value='Register']")
    public WebElement register;
    
    @FindBy(xpath =  "//div[contains(@class,'col-lg-8')]//div[contains(@class,'col-lg-4')]//div[3]")  //  /html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/div[1]/div[1]/div[3]/input[1]
    public WebElement already_registered;
    
    @FindBy(xpath = "//label[contains(text(),'Already Registered')]")
    public WebElement alt_already_registered;
    
    @FindBy(xpath =  "//input[@id='checkout-login-email']")
    public WebElement checkout_login_email;
    
    @FindBy(xpath =  "//input[@id='checkout-login-password']")
    public WebElement checkout_login_password;
    
    
    @FindBy(xpath =  "//*[@id='checkout-login-btn']")
    public WebElement checkout_login_btn;
  
    
    @FindBy(xpath = "//input[@id='register-new-password']")
    public WebElement register_new_password;
    
    @FindBy(xpath = "//input[@id='register-new-password-confirm']")
    public WebElement register_new_password_confirm;
    
    @FindBy(xpath = "//input[@value='Nope']")
    public WebElement continue_as_guest;
    
    @FindBy(xpath = "//a[contains(text(),'Locations')]")
    public WebElement location_tab;
    
    @FindBy(xpath = "//*[contains(text(),'Coming Soon!')]")
    public WebElement coming_soon;
    
    @FindBy(xpath = "//button[contains(text(),'Request Notification')]")
    public WebElement request_notification;
    
    @FindBy(xpath = "//*[@id=\"location-1195\"]/div/div[4]/div/button")    //  /html[1]/body[1]/div[2]/section[1]/section[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/button[1]
    public WebElement alt_request_notification;
    
    @FindBy(xpath = "//input[@id='store-notify-info']")
    public WebElement emailID_for_notification;
    
    @FindBy(xpath = "//input[@type='submit']")
    public WebElement submit_notification;
    
    @FindBy(xpath = "//label[contains(text(),'Phone')]")
    public WebElement phone_notification;
    
    @FindBy(xpath= "//*[@id='cart-side-panel-component']/div/div[1]/div/div/div[2]/div[4]/div[2]/div/div/span/input[2]") //*[@id='cart-side-panel-component']/div/div[1]/div/div/div[2]/div[4]/div[2]/div/div/span ----- ///html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[4]/div[2]/div[1]/div[1]/span[1]/input[2]
    public WebElement address_cart; 
    
    @FindBy(xpath = "//*[@id='cart-side-panel-component']/div/div[1]/div/div/div[2]/div[4]/div[2]/div/div/span/input[2]")
    public WebElement address_cart1;
    
    
    @FindBy(xpath= "//*[@id='cart-side-panel-component']/div/div[1]/div/div/div[2]/div[4]/div[2]/div/div/span/div/div/div")
    public WebElement address_suggestion_cart;
    
    @FindBy(xpath = "//button[@id='order-type-pickup']")
    public WebElement cart_pickup_button;
    
    @FindBy(xpath = "//*[@id='cart-side-panel-component']/div/div[1]/div/div/div[2]/div[4]/div[1]/div/div/button[2]")
    public WebElement only_cart_pickup_button;
    
    @FindBy(xpath = "//div[contains(@class,'col-6 mt-0 mt-sm-1')]//button[contains(@class,'btn btn-primary btn-block btn-store btn-store-pickup')][contains(text(),'Pickup')]")
    public WebElement only_pickup_button;
    
    
    @FindBy(xpath = "//h5[contains(text(),'Recipient Delivery Information')]")
    public WebElement Recipient_Delivery_Information;
    
    @FindBy(xpath = "//select[@class='custom-select']")
    public WebElement quantity_update_dropdown;
    
    @FindBy(xpath="//div[contains(@class,'mt-4 d-none d-lg-block')]//button[@type='button'][contains(text(),'Update Product')]")
    public WebElement quantity_update_button;
    
    @FindBy(xpath = "//h5[contains(text(),'Sugar Rush (12 Traditional)')]")
    public WebElement sugar_rush;
    
    @FindBy(xpath = "//div[@class='col-sm-12 text-center ng-binding']")
    public WebElement address_on_tracking_page;
    
    @FindBy(xpath="//span[contains(text(),'-$5.00')]")
    public WebElement amount_coupon;
    
    @FindBy(xpath="//span[contains(text(),'-$')]")  //  --/html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[5]/div[5]/div[2]/span[1]
    public WebElement percentage_coupon;
    
    @FindBy(xpath="//span[contains(text(),'-$2')]")
    public WebElement delivery_coupon;
    
    @FindBy(xpath = "//div[@class='col-sm-6']//i[@class='icon fa fa-check']")
    public WebElement same_As_above;
    
    @FindBy(xpath = "//i[@data-count='1']")
    public WebElement cart_button_item_count;
    
    @FindBy(xpath = "//span[@class='ui-icon ui-icon-circle-triangle-w']")
    public WebElement left_date_arrow;
   
    @FindBy(xpath = "//span[@class='ui-icon ui-icon-circle-triangle-e']")
    public WebElement right_date_arrow;
    
    @FindBy(xpath = "//h5[contains(text(),'100 Cookies')]")
    public WebElement hundred_cookies;
    
    @FindBy(xpath = "//div[@class='input-group-append address-book-icon']//button[@type='button']")
    public WebElement address_book_icon;
    
    @FindBy(xpath = "//h5[@id='addressBookModalTitle']")
    public WebElement address_book_header;
    
    @FindBy(xpath = "/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h5[1]")
    public WebElement alt_address_book_header;
    
    @FindBy(xpath = "//div[@id='residence-hall-specials']//div[@class='row']//div[2]//div[1]//div[1]//img[1]")
    public WebElement hundred_cookies_pic;
    
    @FindBy(xpath = "//span[@class='oi oi-chevron-left']")
    public WebElement left_arrow;
    
    @FindBy(xpath = "//span[@class='oi oi-chevron-right']")
    public WebElement right_arrow;
    
    @FindBy(xpath = "//div[contains(text(),'3400 Lancaster Avenue')]")     
    public WebElement new_store_address_right;
    
    @FindBy(xpath = "//div[contains(text(),'108 South 16th Street')]")
    public WebElement new_store_address_left;
    
    @FindBy(xpath = "//*[@id='location-33']/div/div[4]/div/div")    //div[@id='location-1087']//button[@class='btn btn-primary btn-block btn-store btn-store-pickup ae-button'][contains(text(),'Pickup')]
    public WebElement disabled_pickup_one;
    
    @FindBy(xpath = "//*[@id='location-20']/div/div[4]/div/div")   //div[@id='location-1166']//button[@class='btn btn-primary btn-block btn-store btn-store-pickup ae-button'][contains(text(),'Pickup')]
    public WebElement disabled_pickup_two;
    
    @FindBy(xpath = "//div[@class='location card border active']//i[@class='fa fa-check']")
    public WebElement catering_check;
    
    @FindBy(xpath = "//div[@id='location-1071']//i[@class='fa fa-times']")
    public WebElement catering_cross;
    
    @FindBy(xpath = "//h3[contains(text(),'Unfortunately, there are no nearby stores to the e')]")
    public WebElement no_store_present;
    
    @FindBy(xpath = "//div[contains(text(),'1084 E Lancaster Ave')]")
    public WebElement store_displayed;
    
    @FindBy(xpath = "//a[contains(text(),'Gifts')]")
    public WebElement gifts_tab;
    
    @FindBy(xpath = "//div[@class='location card border active']//button[@class='btn btn-primary btn-block btn-store btn-store-pickup'][contains(text(),'Pickup')]")
    public WebElement pickup_only;
  
    @FindBy(xpath = "//div[@class='pull-right']")
    public WebElement product_cost;
  
    
    @FindBy(xpath = "//a[contains(text(),'Gift Cards')]")
    public WebElement gift_card_tab;
    
    @FindBy(xpath = "//label[@id='chase-radio-btn-new']")
    public WebElement newCard;
    
    @FindBy(xpath = "//button[text()='Okay']")
    public WebElement okayToClearCart;
    
    @FindBy(xpath = "//*[@class='growl-message']")
    public WebElement growlMessage;
    
    @FindBy(xpath = "//*[@class='btn-toggle-map btn btn-primary'][text()='Map']")
    public WebElement mapButton;
    
    @FindBy(xpath = "//*[@class='btn-toggle-map btn btn-primary'][text()='Hide Map']")
    public WebElement hideMapButton;
    
  /*  Success!
    Order ready at 11:30 am on 11/19/18*/
    
	/*@FindAllBy(className="form-control search-input tt-input")
	public List<WebElement> autocomplete_Address;*/
	
	
WebDriver driver = getDriver();
WebDriverWait wait = new WebDriverWait(driver, 30);
	
	public Order(WebDriver driver) {
		//super(driver);
		PageFactory.initElements(driver, this);

	}
	
	public  void delete_product(WebDriver driver) throws InterruptedException{
		try{
		wait.until(ExpectedConditions.elementToBeClickable(delete_product_button));
		Actions action = new Actions(driver);
		 
        action.moveToElement(delete_product_button).build().perform();
        Thread.sleep(2000);
		
		JavascriptExecutor jse= (JavascriptExecutor) driver;
		jse.executeScript("return arguments[0].click()", delete_product_button);
		
		//delete_product_button.click();
		Thread.sleep(3000);
		System.out.println("Removing product from the cart");
		}catch(Exception e)
		{
			e.getMessage();
			Actions action = new Actions(driver);
		 
        action.moveToElement(alt_delete_product_button).build().perform();
        Thread.sleep(2000);
		
			
			JavascriptExecutor jse= (JavascriptExecutor) driver;
			jse.executeScript("return arguments[0].click()", alt_delete_product_button);
			//delete_product_button.click();
			System.out.println("Removing product from the cart");
		}
	}
	public boolean retryingFindClick(By by) {
	    boolean result = false;
	    int attempts = 0;
	    while(attempts < 2) {
	        try {
	            driver.findElement(by).click();
	            result = true;
	            break;
	        } catch(StaleElementReferenceException e) {
	        }
	        attempts++;
	    }
	    return result;
	}
	
	
	
public void select_current_date(WebDriver driver) throws InterruptedException{
	
	try{
		wait.until(ExpectedConditions.elementToBeClickable(current_date));
		String str=current_date.getText();
		
	}catch(Exception e)
	{
		e.getMessage();
	}
}
public static String randomEmail() {
    return "random-" + UUID.randomUUID().toString() + "@example.com";
}

}
