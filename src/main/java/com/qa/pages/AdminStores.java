package com.qa.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

public class AdminStores extends TestBase {

	protected WebDriver driver;

	// CREATED PARAMETERIZED CONSTRUCTOR AND PASSING DRIVER INTO IT
	public AdminStores(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@id='username']")
	public WebElement userName;

	@FindBy(xpath = "//input[@id='password']")
	public WebElement password;

	@FindBy(xpath = "//button[contains(text(),'Login')]")
	public WebElement login;

	@FindBy(xpath = "//span[contains(text(),'Stores')]")
	public WebElement stores;

	@FindBy(xpath = "//input[@class='form-control']")     //label[contains(text(),'Search')]/input
	public WebElement storeSearchTextBox;
	
	@FindBy(xpath = "//*[@id='dt_stores_filter']/label/input")
	public WebElement altStoreSearchTextBox;

	@FindBy(xpath = "//tr[1]//td[9]//a[1]")
	public WebElement paymentsLink;

	@FindBy(xpath = "//select[@id='selected-payment-processor']")
	public WebElement ccPaymentProcessor;

	@FindBy(xpath = "//a[@class='dropdown-toggle no-margin userdropdown']//img[@class='online']")
	public WebElement profileIcon;

	@FindBy(xpath = "//strong[contains(text(),'ogout')]")
	public WebElement logout;

	@FindBy(xpath = "//button[@id='bot2-Msg1']")
	public WebElement confirmLogout;

	@FindBy(xpath = "//button[@id='update-payment-processor-btn']")
	public WebElement updateBtn;

	@FindBy(xpath = "//p[contains(text(),'Payment processor updated')]")
	public WebElement popupUpdateSuccessMsg;

	@FindBy(xpath = "//tr[1]//td[2]")
	public WebElement cardStatus;

	@FindBy(xpath = "//th[contains(text(),'Status')]")
	public WebElement headerStatus;

	@FindBy(xpath = "//tr[@class='odd']//a[@class='btn btn-success btn-xs'][contains(text(),'edit')]")
	public WebElement storeEditOddRow;

	@FindBy(xpath = "//tr[@class='even']//a[@class='btn btn-success btn-xs'][contains(text(),'edit')]")
	public WebElement storeEditEvenRow;

	@FindBy(xpath = "//*[@id='store_form']/fieldset[6]/div[1]/section/span/input")
	public WebElement enableDisableCaptcha;

	@FindBy(xpath = "//*[@id='store_form']/fieldset[6]/div[1]/section/span/label/span[2]") // *[@id='store_form']/fieldset[6]/div[1]/section/span/label/span[2]
	public WebElement enableDisableCaptcha1;

	@FindBy(xpath = "//*[@id='store_form']/fieldset[6]/div[1]/section/span/label/span[1]")
	public WebElement captchaSpan;
	
	@FindBy(xpath ="//h2[contains(text(),'Edit Store')]")
	public WebElement inSideEditStore;

	@FindBy(xpath = "//*[@id='store_form']/footer/button")
	public WebElement editStore;

//	public void login (String userName1, String password1){
//		userName.sendKeys(userName1);
//		password.sendKeys(password1);	
//		login.click();
//	}
	public void Admin_login() {
		try {

			if (userName.isDisplayed()) {
				userName.sendKeys(prop.getProperty("adminSiteUserName"));
				password.sendKeys(prop.getProperty("adminSitePassword"));
				login.click();
			}
		} catch (Exception e) {
			if (profileIcon.isDisplayed()) {
				System.out.println("User already logged in");
			}
		}
	}

	public void disableReCaptcha() throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		String webSiteUrl = getUrl();
		String adminSiteUrl = webSiteUrl + "/admin";
		driver.get(adminSiteUrl);
		int counter = 1;

		if (userName.isDisplayed()) {
			userName.sendKeys(prop.getProperty("adminSiteUserName"));
			password.sendKeys(prop.getProperty("adminSitePassword"));
			login.click();
		}
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(stores));
		stores.click();
		for (int i = 0; i < 2; i++) {
			if (counter == 1) {
				try {
					wait.until(ExpectedConditions.visibilityOf(storeSearchTextBox));
					Thread.sleep(3000);
					storeSearchTextBox.sendKeys("Gift Cards");
					Thread.sleep(1000);
				}catch (Exception e) {
					wait.until(ExpectedConditions.visibilityOf(altStoreSearchTextBox));
					Thread.sleep(3000);
					altStoreSearchTextBox.sendKeys("Gift Cards");
					Thread.sleep(1000);
				}
				
				storeEditOddRow.click();
				wait.until(ExpectedConditions.visibilityOf(inSideEditStore));
				js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				counter++;
				if (enableDisableCaptcha.isSelected()) {
					System.out.println("Recaptcha is enabled so disabling it");
					enableDisableCaptcha1.click();
					Thread.sleep(5000);
					editStore.click();
				} else {
					System.out.println("Recaptcha already disabled");
				}
			} else {
				driver.get(adminSiteUrl);
				wait.until(ExpectedConditions.visibilityOf(stores));
				stores.click();
				try {
					wait.until(ExpectedConditions.visibilityOf(storeSearchTextBox));
					Thread.sleep(3000);
					storeSearchTextBox.sendKeys("Gift");
					Thread.sleep(1000);
				}catch (Exception e) {
					wait.until(ExpectedConditions.visibilityOf(altStoreSearchTextBox));
					Thread.sleep(3000);
					altStoreSearchTextBox.sendKeys("Gift");
					Thread.sleep(1000);
				}
				
				storeEditEvenRow.click();
				wait.until(ExpectedConditions.visibilityOf(inSideEditStore));
				js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				if (enableDisableCaptcha.isSelected()) {
					System.out.println("Recaptcha is enabled so disabling it");
					enableDisableCaptcha1.click();
					Thread.sleep(5000);
					editStore.click();
				} else {
					System.out.println("Recaptcha already disabled");
				}
			}
		}

	}

}
