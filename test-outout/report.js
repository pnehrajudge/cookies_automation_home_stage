$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("MyAccount.feature");
formatter.feature({
  "line": 2,
  "name": "My Account",
  "description": "",
  "id": "my-account",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@MyAccount"
    }
  ]
});
formatter.before({
  "duration": 11151182500,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "user access to website",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "User opens the Website",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "if user is already logged in- just logout",
  "keyword": "And "
});
formatter.match({
  "location": "Step_SignIn.user_opens_the_Website()"
});
formatter.result({
  "duration": 37810149500,
  "status": "passed"
});
formatter.match({
  "location": "Step_SignIn.logout_if_already_logged_in()"
});
formatter.result({
  "duration": 5580934600,
  "status": "passed"
});
formatter.scenario({
  "line": 535,
  "name": "TC053 - User should be able to mark favourite credit card by clicking star image displayed in front of credits.",
  "description": "",
  "id": "my-account;tc053---user-should-be-able-to-mark-favourite-credit-card-by-clicking-star-image-displayed-in-front-of-credits.",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 534,
      "name": "@MyAccountDebug"
    }
  ]
});
formatter.step({
  "line": 536,
  "name": "Click on Log In link present in top right corner of the homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 537,
  "name": "User enters valid Email and Password",
  "rows": [
    {
      "cells": [
        "username",
        "icprateeknehra@gmail.com"
      ],
      "line": 538
    },
    {
      "cells": [
        "password",
        "Judge@123"
      ],
      "line": 539
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 540,
  "name": "Click on the Log In button",
  "keyword": "And "
});
formatter.step({
  "line": 541,
  "name": "Click on \"view all\" in Credit cards section.",
  "keyword": "And "
});
formatter.step({
  "line": 542,
  "name": "verify by clicking star the CC becomes favorite",
  "keyword": "Then "
});
formatter.match({
  "location": "Step_SignIn.click_on_Login_button_in_Header()"
});
formatter.result({
  "duration": 6787110500,
  "status": "passed"
});
formatter.match({
  "location": "Step_SignIn.under_SIGN_IN_section_enter_valid_Email_and_Password(DataTable)"
});
formatter.result({
  "duration": 1203462600,
  "status": "passed"
});
formatter.match({
  "location": "Step_SignIn.click_on_Submit()"
});
formatter.result({
  "duration": 8957742200,
  "status": "passed"
});
formatter.match({
  "location": "Step_MyAccount.click_on_ViewAll_CC_section()"
});
formatter.result({
  "duration": 4164355200,
  "status": "passed"
});
formatter.match({
  "location": "Step_MyAccount.validate_Favorite_CC()"
});
formatter.result({
  "duration": 4278519500,
  "status": "passed"
});
formatter.after({
  "duration": 101800,
  "status": "passed"
});
});