package com.qa.stepDefinition;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.AdminCallCenter;
import com.qa.pages.Catering;
import com.qa.pages.Order;
import com.qa.utils.TestBase;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class Step_Catering extends TestBase{
	
	WebDriver driver = getDriver();
	Order order = new Order(driver);
	AdminCallCenter adminCallCenter = new AdminCallCenter(driver);
	Catering catering = new Catering(driver);
	WebDriverWait wait = new WebDriverWait(driver, 20);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	Actions action =  new Actions(driver);
	
	
	@Given("^User clicks on Cater$")
	public void user_clicks_on_Cater() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(catering.cater));
		catering.cater.click();

	}

	@And("^User clicks on catering$")
	public void user_clicks_on_catering() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(catering.catering));
		catering.catering.click();
	}
	
    @And("^On Menu Page click on Catering$")
    public void click_Catering_on_MenuPage(){
    	wait.until(ExpectedConditions.elementToBeClickable(catering.cateringMenuPageBtn));
    	catering.cateringMenuPageBtn.click();
    	
    }
    @And("^On Menu Page click on The Big Birthday Celebration$")
    public void click_BigBirthday_Celeb() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		// Actions action = new Actions(driver);

		action.moveToElement(catering.bigBirthDayCeleberation).build().perform();
		Thread.sleep(5000);
		// order.the_insomniac.click();
		jse.executeScript("return arguments[0].click()", catering.bigBirthDayCeleberation);
	
//    	
//    	wait.until(ExpectedConditions.elementToBeClickable(catering.bigBirthDayCeleberation));
//    	catering.bigBirthDayCeleberation.click();
    	
    }
    
    @And("^user clicks pick for me for Big Bday Celeberation$")
    public void click_Pick_forMe_for_BigBdayCeleb() throws InterruptedException{
    	wait.until(ExpectedConditions.elementToBeClickable(catering.BigBdayCelebPickForMe1));
//    	jse.executeScript("arguments[0].scrollIntoView(true);", catering.BigBdayCelebPickForMe1);
    	jse.executeScript("window.scrollBy(0,650)");
    	catering.BigBdayCelebPickForMe1.click();
    	Thread.sleep(1000);
    	catering.BigBdayCelebPickForMe2.click();   	
//		jse.executeScript("window.scrollBy(0,650)");
    	
    }
    
    @And("^user clicks on add product for catering order$")
    public void click_catering_AddProduct(){
    	jse.executeScript("arguments[0].scrollIntoView(true);", catering.AddProduct_Catering);
    	jse.executeScript("arguments[0].click()", catering.AddProduct_Catering);
    	//catering.AddProduct_Catering.click();
    }
    
    @And("^On Menu Page click on Birthday Celebration$")
    public void click_Birthday_Celeb() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.birthdayCeleberation).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.birthdayCeleberation);

    	
    }
    
    @And("^user clicks pick for me for Bday Celeberation$")
    public void click_Pick_forMe_for_BdayCeleb() throws InterruptedException{
    	wait.until(ExpectedConditions.elementToBeClickable(catering.BdayCelebPickForme));
    	jse.executeScript("window.scrollBy(0,650)");
    	catering.BdayCelebPickForme.click();
    	Thread.sleep(3000);
    }
	
    @And("^On Menu Page click on Ice Cream Bar$")
    public void click_IceCreamBar_Celeb() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.iceCreamBar).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.iceCreamBar);

    	
    }
    
    @And("^user clicks pick for me for Ice Cream Bar$")
    public void click_Pick_forMe_for_IceCreamBar() throws InterruptedException{
    	wait.until(ExpectedConditions.elementToBeClickable(catering.IceCreamBarPickForMe1));
    	catering.IceCreamBarPickForMe1.click();
    	Thread.sleep(1000);
    	jse.executeScript("window.scrollBy(0,250)");
    	catering.IceCreamBarPickForMe2.click();
    	Thread.sleep(3000);
    }
    
    
	@When("^user select Date and tomorrows Time from Calendar$")
	public void user_select_Date_and_Time_from_Calendar() throws Throwable {


		 wait.until(ExpectedConditions.elementToBeClickable(catering.deliveryDate));
//		 adminCallCenter.asap.click();
		 try {
//			 jse.executeScript("return arguments[0].click()", catering.deliveryDate);
			 Thread.sleep(1000);
    		catering.deliveryDate.click();
	        Thread.sleep(2000);

	        String tomorrow = adminCallCenter.getCurrentDay();
	        System.out.println("Today's number: " + tomorrow + "\n");
	        List<WebElement> columns = catering.deliveryDateCalender.findElements(By.tagName("td"));

	        for (WebElement cell : columns) {
	            if (cell.getText().equals(tomorrow)) {
	                cell.click();
	                break;
	            }
	        }
			wait.until(ExpectedConditions.elementToBeClickable(order.time_dropdown));
			Select time_dropdown = new Select(order.time_dropdown);
			Thread.sleep(3000);
			time_dropdown.selectByValue("12:00 PM");
//			time_dropdown.selectByIndex(4);

		} catch (Exception e) {

			Select time_dropdown = new Select(order.alt_time_dropdown);
			time_dropdown.selectByValue("12:00 PM");

			// System.out.println("running exception block");
		}

	}
	
    
	@And("^On Menu Page click on Office Party$")
    public void click_Office_Party() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.officeParty).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.officeParty);

    	
    }
    
//    @And("^user clicks pick for me for Office Party$")
//    public void click_Pick_forMe_for_OfficeParty() throws InterruptedException{
//    	wait.until(ExpectedConditions.elementToBeClickable(catering.officePartyPickForMe1));
//    	catering.officePartyPickForMe1.click();
//    	Thread.sleep(1000);
//    	jse.executeScript("window.scrollBy(0,250)");
//    	catering.officePartyPickForMe2.click();
//    	Thread.sleep(3000);
//    }
    
    
    @And("^user clicks pick for me$")
    public void click_Pick_forMe() throws InterruptedException{
    	wait.until(ExpectedConditions.elementToBeClickable(catering.pickForMe));
    	List<WebElement> pickForMe =  catering.pickForMeList;
    	int numberOfPickForMe = pickForMe.size();
    	for(int i=0;i<numberOfPickForMe;i++){
    		WebElement element = pickForMe.get(i);
    		Thread.sleep(1000);
    		element.click();
    		Thread.sleep(1000);
    	}
//    	
//    	
//    	catering.officePartyPickForMe1.click();
//    	Thread.sleep(1000);
//    	jse.executeScript("window.scrollBy(0,250)");
//    	catering.officePartyPickForMe2.click();
//    	Thread.sleep(3000);
    }
    
    
	@And("^On Menu Page click on Party Starter$")
    public void click_Party_Starter() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.partyStarter).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.partyStarter);

    	
    }
    
    @And("^user clicks pick for me for Party Starter$")
    public void click_Pick_forMe_for_PartyStarter() throws InterruptedException{
    	wait.until(ExpectedConditions.elementToBeClickable(catering.officePartyPickForMe1));
    	catering.officePartyPickForMe1.click();
    	Thread.sleep(1000);
    	jse.executeScript("window.scrollBy(0,250)");
    	catering.officePartyPickForMe2.click();
    	Thread.sleep(3000);
    }
    
	@And("^On Menu Page click on Sweet-Tooth Sampler$")
    public void click_Sweet_Tooth_Sampler() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.sweetToothSampler).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.sweetToothSampler);

    	
    }
	
	@And("^On Menu Page click on Crowd Pleaser$")
    public void click_Crowd_Pleaser() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.crowdPleaser).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.crowdPleaser);

    	
    }
    
	@And("^On Menu Page click on 50Cookies$")
    public void click_FiftyCookies() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.FiftyCookies).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.FiftyCookies);  	
    }
	
	@And("^On Menu Page click on 100Cookies$")
    public void click_HundredCookies() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.HundredCookies).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.HundredCookies);  	
    }
	
	@And("^On Menu Page click on 200Cookies$")
    public void click_TwoHundredCookies() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.TwoHundredCookies).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.TwoHundredCookies);  	
    }
	
	@And("^On Menu Page click on 300Cookies$")
    public void click_ThreeHundredCookies() throws InterruptedException{
    	
    	new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));

		action.moveToElement(catering.ThreeHundredCookies).build().perform();
		Thread.sleep(5000);

		jse.executeScript("return arguments[0].click()", catering.ThreeHundredCookies);  	
    }
	
	@And("^user select Date and tomorrows Time outside 10AM to 6PM from Calendar$")
	public void user_select_Date_and_Time_Between_10Am_to_6PM_from_Calendar() throws Throwable {


		 wait.until(ExpectedConditions.elementToBeClickable(catering.deliveryDate));
//		 adminCallCenter.asap.click();
		 try {
//			 jse.executeScript("return arguments[0].click()", catering.deliveryDate);
			 Thread.sleep(1000);
    		catering.deliveryDate.click();
	        Thread.sleep(2000);

	        String tomorrow = adminCallCenter.getCurrentDay();
	        System.out.println("Today's number: " + tomorrow + "\n");
	        List<WebElement> columns = catering.deliveryDateCalender.findElements(By.tagName("td"));

	        for (WebElement cell : columns) {
	            if (cell.getText().equals(tomorrow)) {
	                cell.click();
	                break;
	            }
	        }
			wait.until(ExpectedConditions.elementToBeClickable(order.time_dropdown));
			Select time_dropdown = new Select(order.time_dropdown);
			Thread.sleep(3000);
			time_dropdown.selectByValue("10:30 AM");
//			time_dropdown.selectByIndex(4);

		} catch (Exception e) {

			Select time_dropdown = new Select(order.alt_time_dropdown);
			time_dropdown.selectByValue("2:45 AM");

			// System.out.println("running exception block");
		}

	}
	
	@Then("^verify validation message pops up to select time between 10 Am to 6PM$")
	public void verify_validation_message_pops_Up(){
		wait.until(ExpectedConditions.visibilityOf(catering.errorMsgTenToSix));
//		String errorMsg = catering.errorMsgTenToSix.getText();
		Assert.assertTrue("Validation error message to order a catring order between 10am to 6pm is not displayed: #############", catering.errorMsgTenToSix.isDisplayed());
	}
	
	
}
