@HomePage
Feature: HomePage

  Background: user access to website
    Given User opens the Website without clearing cache

  Scenario: TC001_Verify Website is opening or not
    # | url | https://20.stage.insomniacookies.com/ |
    Given user Enters the website url
    Then Verify Insomnia Cookies Homepage is displayed

  Scenario: TC002_Verify IC Logo is present in left side of header
    Given user Enters the website url
    Then Verify the IC logo in the left side of header

  # can't automate this scenario due to rEcaptcha
  #Scenario: TC003_Verify user is able to Sign up successfully with new EmailID & password
  #Given Click on Log In link present in top right corner of the homepage
  #When user enters new emailID
  #And user enters password and confirm password
  #| password         | 12345 |
  #| confirm password | 12345 |
  #And Click on Create Account button
  #Then User must get logged in and UserName should be displayed in the header
  Scenario: TC005_Verify user is not able to signup with existing email
    Given Click on Log In link present in top right corner of the homepage
    When User enters an already registered emaid Id in email text box
      | email           | pnehra@judge.com |
      | newpassword     | password         |
      | confirmpassword |              123 |
    And Click on Create Account button
    Then Error popup should appear stating that The email has already been taken

  Scenario: TC006_Verify user is not able to SignUp if email id format is wrong
    Given Click on Log In link present in top right corner of the homepage
    When User enter valid inputs in all fields and invalid  emailid format
      | email | pnehra@ |
    And Click on Create Account button
    Then Error popup should appear stating that The email must be a valid email address

  #Scenario: Verify validation message comes across all Mandatory fields
  #Given Click on Log In link present in top right corner of the homepage
  #And Click on Create Account button
  #Then Error popup should appears stating Please enter your first name
  #When User enter first Name
  #| firstname | Prateek |
  #And Click on Create Account button
  #Then Error popup should appears stating Please enter your last name
  #When User enter last Name
  # | lastname | Nehra |
  # And Click on Create Account button
  #Then Error popup should appears stating The email field is required
  #When User enters emailid
  #| email | test11@test.com |
  #And Click on Create Account button
  #Then Error popup should appears stating The telephone field is required
  #When User enters The telephone number
  # | phone | 1111234534 |
  #And Click on Create Account button
  #Then Error popup should appear stating The password field is required
  #When User enter the password
  #| newpassword | password |
  #And Click on Create Account button
  #Then Error popup should apppear stating The password confirmation does not match
  Scenario: TC007_Verify user is not able to SignUp if password is not matched with the confirm password fields
    Given Click on Log In link present in top right corner of the homepage
    When User enter valid inputs in all fields and uncommon inputs in Password and confirm password fields
      | email           | test12@test.com |
      | newpassword     | password        |
      | confirmpassword |             123 |
    And Click on Create Account button
    Then Error Validation messages should appear stating that The password confirmation does not match

  Scenario: TC008_Verify user is not able to SignUp if entered password length is less than 5
    Given Click on Log In link present in top right corner of the homepage
    When User enter valid inputs in all fields and 4 digit password in Password fields
      | email           | test12@test.com |
      | newpassword     |             123 |
      | confirmpassword |             123 |
    And Click on Create Account button
    Then Error Validation messages should appear stating that The password must be at least 5 characters

  Scenario: TC009_Validation message is appearing  for all mandantory field
    Given Click on Log In link present in top right corner of the homepage
    And Under 'New customer' section, do not enter Data in valid fields and click create Account button
    Then verify required field validation message pops up for email ID
    And fill in the email ID
      | test32145@tester.com |
    And Click on Create Account button
    Then verify required field validation message pops up for password
    And fill the password
      | 123456 |
    And Click on Create Account button
    Then verify required field validation message pops up for confirm password
    And fill the confirm password
      | 123456 |
    And Click on Create Account button
    Then verify validation message for invalid captcha is displayed

  Scenario: TC011_user is not able to login with invalid credentials
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and incorrect Password
      | username | icprateeknehra1@gmail.com |
      | password |                    123456 |
    And Click on the Log In button
    Then verify user validation message stating Email and password do not match is displayed

  #Scenario: Verify user is not able to SignUp if entered phone no. is greater than 10 digit
  # Given Click on Log In link present in top right corner of the homepage
  #When User enter valid inputs in all fields and phone no greater than 10
  # | firstname | Shaurya         |
  #| lastname  | Nigam           |
  # | email     | test12@test.com |
  # | phone     |    123456789012 |
  # Then Only 10 digits should be displayed in the textbox
  #@tag4
  #Scenario: Verify phone no text box accept only numbers not characters/Alphanumeric
  #Given Click on Login button in Header
  #When User enter valid inputs in all fields and phone no in characters
  #| firstname | Prateek         |
  #| lastname  | Nehra           |
  #| email     | test12@test.com |
  #| phone     | sdggsgdfgdf     |
  #Then Nothing should be displayed in the textbox
  Scenario: TC012_Verify user  redirects to Forgot Password page on clicking Forgot password link.
    Given Click on Log In link present in top right corner of the homepage
    When User Click on Forgot Passoword
    Then user redirects to Forgot password Page

  Scenario: TC016_Alert should appear saying Please enter an email address  & The word field is required
    Given Click on Log In link present in top right corner of the homepage
    And Click on the Log In button
    Then Verify validation message stating email ID and password required should be displayed


  Scenario: TC017_Verify user is able to place order from home page
    Given On home page enter the address
      | 1084 East Lancaster Avenue, Bryn Mawr, PA |
    And click Continue to navigatge to Orders page
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth        |
      | recepient phone  |               2345678981 |
      | customer name    | Prateek Nehra            |
      | customer phone   |               3457689024 |
      | customer emailID | icprateeknehra@gmail.com |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

  Scenario: TC018_Verify clicking "Signup" on home page redirects to the Signup page
  Given Click on Sign Up button on home page
  Then Verify Sign up page gets displayed


  Scenario: TC019_Verify clicking "Learn More" on home page redirects to the Get started by signing up now page
  Given Click on Learn More button on home page
  Then Verify Get started page is displayed


  Scenario: TC020_Verify clicking "Order Now" under "We Ship Nationwide" section redirects to ship coookies menu page and user is able place the order
		Given On home page user clicks Order Now button under We ship NationWide section
		Then Verify User gets redirected to ship cookies menu page
		And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Prateek Nehra     |
      | customer phone     |        3457689024 |
      | customer emailID   | pnehra@judge.com  |
    And user enters shipping address
      | Delivery Address | 2227 Massachusetts Avenue, Lexington, MA |
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
  Scenario: TC010_user is able to signIn successfully as "current customer"
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com |
      | password | Judge@123                 |
    And Click on the Log In button
    Then verify user logs in successfully
