package com.qa.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

public class Locations extends TestBase{
	
	@FindBy(id = "entered_location")
	public WebElement entered_location;
	
	@FindBy(id = "input")
	public WebElement input;
	
	@FindBy(xpath="//input[@id='addresstext']")
	public WebElement search_box;
	
	@FindBy(xpath = "//div[@class='input-group']//button[@type='button']")
	public WebElement search_button;
	
	@FindBy(xpath = "//div[@title='Show street map']")
	public WebElement map;
	
	@FindBys(@FindBy(xpath="//h5[contains(@class,'card-title')]"))
	public List<WebElement> nearest_stores;
	
	@FindBy(xpath = "//div[contains(@class,'tt-suggestion tt-selectable')]")
	public WebElement suggestion;
	
	@FindBys(@FindBy(xpath="//small"))
	public List<WebElement> distance;
	
	@FindBys(@FindBy(xpath="//a[contains(@data-toggle,'modal')]"))
	public List<WebElement> info_links;
	
	@FindBys(@FindBy(xpath="//p[contains(@class,'mb-1')]"))
	public List<WebElement> addresses;
	
	@FindBy(xpath = "//h1[@class='mt-4 h4']")
	public WebElement store_name_info;
	
	@FindBy(xpath = "//div[@id='close-icon']")
	public WebElement close;
	
	@FindBy(xpath = "//p[contains(text(),'Delivering to ')]")
	public WebElement delivery_location;
	
	@FindBy(xpath = "//div[@class='col-lg-6'][contains(text(),'1084 E Lancaster Ave')]")     // /html/body/div[3]/div/div/div[2]/div[2]/div[2]/div[1]
	public WebElement store_address;
	
	@FindBy(xpath = "//*[@id='locations-detail-view']/div[2]/div[2]/a")  // /html/body/div[3]/div/div/div[2]/div[2]/div[2]/div[2]/a
	public WebElement contact_no;
	
	@FindBy(xpath = "//button[@class='order-now-btn btn btn-primary btn-block']")      //button[@class='order-now-btn btn btn-primary btn-block ae-button']
	public WebElement order_now;
	
	@FindBy(xpath = "//*[@id='locations-detail-view']/div[3]/div[1]")    //div[@id='ae-main-content']//div[@class='row mt-2']//div[1]
	public WebElement retail_hours;
	
	@FindBy(xpath = "//*[@id='locations-detail-view']/div[3]/div[2]")   //div[@id='ae-main-content']//div[@class='row mt-2']//div[2]
	public WebElement delivery_hours;
	
	@FindBy(xpath = "//div[@id='store-image-wrap']")
	public WebElement store_image;
	
	@FindBy(xpath = "//div[@id='location-1071']//div[@class='d-flex justify-content-between']//div[@class='text-primary']//a[@href='#'][contains(text(),'info')]")
	public WebElement info_link;
	
	
	WebDriver driver = getDriver();
	WebDriverWait wait = new WebDriverWait(driver, 30);
	
	public Locations(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	

}
