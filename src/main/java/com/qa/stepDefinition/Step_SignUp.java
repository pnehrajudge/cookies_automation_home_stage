package com.qa.stepDefinition;

//import static org.testng.AssertJUnit.assertEquals;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.SignIn;
import com.qa.pages.SignUp;
import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class Step_SignUp extends TestBase {

	/*
	 * public step_Signup(WebDriver driver) { super(driver); // TODO Auto-generated
	 * constructor stub }
	 */

	WebDriver driver = getDriver();
	WebDriverWait wait = new WebDriverWait(driver, 30);
	SignUp signup = new SignUp(driver);
	SignIn signIn = new SignIn(driver);
	// signin s=new signin(driver);

	@And("^Click on Submit button$")
	public void click_submit() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(signup.submit_login));
		signup.submit_login.click();
		Thread.sleep(4000);
	}

	@When("^User enters an already registered emaid Id in email text box$")
	public void user_enters_an_already_registered_emaid_Id_in_email_text_box(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();

		signup.emailid.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(signup.new_password));
		signup.new_password.sendKeys(data.get(1).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(signup.new_user_confirm_password));
		signup.new_user_confirm_password.sendKeys(data.get(2).get(1));

	}

	@Then("^Error popup should appear stating that The email has already been taken$")
	public void error_popup_should_appear_stating_that_The_email_has_already_been_taken() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(signup.popup));
		Assert.assertEquals("The email has already been taken.", signup.popup.getText());

	}

	@When("^Click on Create Account button$")
	public void click_on_new_Submit_button() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(signup.create_account));
		signup.create_account.click();
		Thread.sleep(3000);
	}

	@When("^User enter valid inputs in all fields and invalid  emailid format$")
	public void user_enter_invalid_emailid_format(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();

		signup.emailid.sendKeys(data.get(0).get(1));

		Thread.sleep(3000);

	}

	@Then("^Error popup should appear stating that The email must be a valid email address$")
	public void error_popup_should_appear_stating_that_The_email_must_be_a_valid_email_address() throws Throwable {
		String message = signup.emailid.getAttribute("validationMessage");
		if (message.contains("Please enter a part following '@'.")) {
			System.out.println("Validation message displayed");
		} else {
			Assert.fail("Validation check not getting applied for incorrect email ID");
		}
	}

	@Then("^Error popup should appears stating Please enter your first name$")
	public void error_popup_should_appears_stating_Please_enter_your_first_name() throws Throwable {
		Assert.assertEquals("Please enter your first name", signup.popup.getText());
		System.out.println(signup.popup.getText());
		Thread.sleep(5000);
	}

	@When("^User enter first Name$")
	public void user_enter_first_Name(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();

		signup.first_name.sendKeys(data.get(0).get(1));
		// Thread.sleep(3000);
	}

	@Then("^Error popup should appears stating Please enter your last name$")
	public void error_popup_should_appears_stating_Please_enter_your_last_name() throws Throwable {
		Assert.assertEquals("Please enter your last name", signup.popup.getText());
		System.out.println(signup.popup.getText());
		Thread.sleep(5000);
	}

	@When("^User enter last Name$")
	public void user_enter_last_Name(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		signup.last_name.sendKeys(data.get(0).get(1));
		Thread.sleep(3000);
	}

	@Then("^Error popup should appears stating The email field is required$")
	public void error_popup_should_appears_stating_The_email_field_is_required() throws Throwable {
		Assert.assertEquals("The email field is required.", signup.popup.getText());
		System.out.println(signup.popup.getText());
		Thread.sleep(5000);
	}

	@When("^User enters emailid$")
	public void user_enters_emailid(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		signup.emailid.sendKeys(data.get(0).get(1));
	}

	@Then("^Error popup should appears stating The telephone field is required$")
	public void error_popup_should_appears_stating_The_telephone_field_is_required() throws Throwable {
		Assert.assertEquals("The telephone field is required.", signup.popup.getText());
		System.out.println(signup.popup.getText());
		Thread.sleep(5000);
	}

	@When("^User enters The telephone number$")
	public void user_enters_The_telephone_number(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		// signup.phone.sendKeys(data.get(0).get(1));
	}

	@Then("^Error popup should appear stating The password field is required$")
	public void error_popup_should_appear_stating_The_password_field_is_required() throws Throwable {
		Assert.assertEquals("The password field is required.", signup.popup.getText());
		System.out.println(signup.popup.getText());
		Thread.sleep(5000);
	}

	@When("^User enter the password$")
	public void user_enter_the_password(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		signup.new_password.sendKeys(data.get(0).get(1));
	}

	@Then("^Error popup should apppear stating The password confirmation does not match$")
	public void error_popup_should_apppear_stating_The_password_confirmation_does_not_match() throws Throwable {
		Assert.assertEquals("The password confirmation does not match.", signup.popup.getText());
		System.out.println(signup.popup.getText());
		Thread.sleep(5000);
	}

	@When("^User enter valid inputs in all fields and uncommon inputs in Password and confirm password fields$")
	public void user_enter_valid_inputs_in_all_fields_and_uncommon_inputs_in_Password_and_confirm_password_fields(
			DataTable table) throws Throwable {
		List<List<String>> data = table.raw();

		wait.until(ExpectedConditions.elementToBeClickable(signup.emailid));
		signup.emailid.sendKeys(data.get(0).get(1));
		Thread.sleep(3000);
		// signup.phone.sendKeys(data.get(3).get(1));
		// Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(signup.new_password));
		signup.new_password.sendKeys(data.get(1).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(signup.new_user_confirm_password));
		signup.new_user_confirm_password.sendKeys(data.get(2).get(1));

	}

	@Then("^Error Validation messages should appear stating that The password confirmation does not match$")
	public void error_Validation_messages_should_appear_stating_that_The_password_confirmation_does_not_match()
			throws Throwable {
		Assert.assertEquals("The password confirmation and password must match.", signup.popup.getText());
		System.out.println(signup.popup.getText());
	}

	@When("^User enter valid inputs in all fields and 4 digit password in Password fields$")
	public void User_enter_valid_inputs_in_all_fields_and_4_digit_password_in_Password_fields(DataTable table)
			throws Throwable {
		List<List<String>> data = table.raw();

		signup.emailid.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(signup.new_password));

		signup.new_password.sendKeys(data.get(1).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(signup.new_user_confirm_password));
		signup.new_user_confirm_password.sendKeys(data.get(2).get(1));

	}

	@And("^user enters password and confirm password$")
	public void user_enters_password_and_confirm_password(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		signup.new_password.sendKeys(data.get(0).get(1));
		signup.new_user_confirm_password.sendKeys(data.get(1).get(1));

	}

	@Then("^Error Validation messages should appear stating that The password must be at least 5 characters$")
	public void paaswprd_less_than_5() throws Throwable {
		Assert.assertEquals("The password must be at least 5 characters.", signup.popup.getText());
		System.out.println(signup.popup.getText());

	}

	@When("^User enter valid inputs in all fields and phone no less than (\\d+)$")
	public void user_enter_valid_inputs_in_all_fields_and_phone_no_less_than(int arg1, DataTable table)
			throws Throwable {
		List<List<String>> data = table.raw();

		signup.first_name.sendKeys(data.get(0).get(1));
		// Thread.sleep(3000);
		signup.last_name.sendKeys(data.get(1).get(1));
		// Thread.sleep(3000);
		signup.emailid.sendKeys(data.get(2).get(1));
		// Thread.sleep(3000);
		// signup.phone.sendKeys(data.get(3).get(1));
		// Thread.sleep(3000);
		signup.new_password.sendKeys(data.get(4).get(1));
		// Thread.sleep(3000);
		signup.new_user_confirm_password.sendKeys(data.get(5).get(1));
		// Thread.sleep(3000);

	}

	@Then("^Error Validation messages should appear stating that Please enter a (\\d+) digit phone number$")
	public void error_Validation_messages_should_appear_stating_that_Please_enter_a_digit_phone_number(int arg1)
			throws Throwable {
		Assert.assertEquals("Please enter a 10 digit phone number", signup.popup.getText());
		System.out.println(signup.popup.getText());

	}

	@When("^User enter valid inputs in all fields and phone no greater than (\\d+)$")
	public void user_enter_valid_inputs_in_all_fields_and_phone_no_greater_than(int arg1, DataTable table)
			throws Throwable {
		List<List<String>> data = table.raw();

		signup.first_name.sendKeys(data.get(0).get(1));
		// Thread.sleep(3000);
		signup.last_name.sendKeys(data.get(1).get(1));
		// Thread.sleep(3000);
		signup.emailid.sendKeys(data.get(2).get(1));
//		signup.phone.sendKeys(data.get(3).get(1));
	}

	@Then("^Only (\\d+) digits should be displayed in the textbox$")
	public void only_digits_should_be_displayed_in_the_textbox(int arg1) throws Throwable {

//		String str = signup.phone.getAttribute("value");
//		System.out.println(str);
//		int i = str.length();
//		System.out.println(i);
//		Assert.assertEquals(10, i);

	}

	@When("^User enter valid inputs in all fields and phone no in characters$")
	public void user_enter_valid_inputs_in_all_fields_and_phone_no_in_characters(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		signup.first_name.sendKeys(data.get(0).get(1));
		// Thread.sleep(3000);
		signup.last_name.sendKeys(data.get(1).get(1));
		// Thread.sleep(3000);
		signup.emailid.sendKeys(data.get(2).get(1));
		// signup.phone.sendKeys(data.get(3).get(1));
	}

	@Then("^Nothing should be displayed in the textbox$")
	public void nothing_should_be_displayed_in_the_textbox() throws Throwable {
//		String str = signup.phone.getAttribute("value");
//		System.out.println(str);
//		if (str.isEmpty()) {
//			System.out.println("Test Case Passed");
//		} else {
//			System.out.println("Test Case Passed");
//		}

	}

	@And("^User Click on Forgot Passoword$")
	public void click_forgot_password() throws Throwable {
		signup.forgot_password.click();
	}

	@Then("^user redirects to Forgot password Page$")
	public void forgot_password_page() throws Throwable {

		Assert.assertEquals("Forgot Your Password?", signup.forgot_password_page.getText());
		System.out.println(signup.forgot_password_page.getText());
		Thread.sleep(5000);
	}

	@And("^Under 'New customer' section, do not enter Data in valid fields and click create Account button$")
	public void click_create_Account_button() {
		wait.until(ExpectedConditions.elementToBeClickable(signup.create_account));
		signup.create_account.click();
	}

	@Then("^verify required field validation message pops up for email ID$")
	public void verify_validation_message_for_email_is_displayed() {
		String message = signup.emailid.getAttribute("validationMessage");
		Assert.assertEquals("Email required field validation is missing", "Please fill out this field.", message);

	}

	@And("^fill in the email ID$")
	public void fill_the_email(DataTable table) {
		List<List<String>> data = table.raw();
		signup.emailid.sendKeys(data.get(0).get(0));
	}

	@Then("^verify required field validation message pops up for password$")
	public void verify_validation_message_for_password_is_displayed() {
		String message = signup.new_password.getAttribute("validationMessage");
		Assert.assertEquals("Password required field validation is missing", "Please fill out this field.", message);

	}

	@Then("^verify required field validation message pops up for confirm password$")
	public void verify_validation_message_for_Confirmpassword_is_displayed() {
		String message = signup.new_user_confirm_password.getAttribute("validationMessage");
		Assert.assertEquals("Password required field validation is missing", "Please fill out this field.", message);

	}

	@And("^fill the password$")
	public void fill_the_password(DataTable table) {
		List<List<String>> data = table.raw();
		signup.new_password.sendKeys(data.get(0).get(0));
	}

	@And("^fill the confirm password$")
	public void fill_the_confirmPassword(DataTable table) {
		List<List<String>> data = table.raw();
		signup.new_user_confirm_password.sendKeys(data.get(0).get(0));
	}

	@Then("^verify validation message for invalid captcha is displayed$")
	public void verify_growl_message() {
		wait.until(ExpectedConditions.visibilityOf(signup.popup));
		Assert.assertEquals("Captcha not clicked validation message missing", "Invalid captcha, try again or refresh the page.", signup.popup.getText());

	}
	@Then("^verify user logs in successfully$")
	public void verify_userloggedinSuccess() {
		wait.until(ExpectedConditions.visibilityOf(signIn.logout));
		System.out.println("User logs in success");
	}
	
}
