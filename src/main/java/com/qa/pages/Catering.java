package com.qa.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.utils.TestBase;

public class Catering extends TestBase{
	
	protected WebDriver driver;
	//CREATED PARAMETERIZED CONSTRUCTOR AND PASSING DRIVER INTO IT
	public Catering(WebDriver driver) 
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[@id='cater-navbar-dropdown']")
	public WebElement cater;
	
	@FindBy(xpath="//a[contains(text(),'Catering')]")
	public WebElement catering;
	
	@FindBy(xpath="//a[@class='nav-link py-3 px-2 px-md-auto'][contains(text(),'Catering')]")
	public WebElement cateringMenuPageBtn;
	
	@FindBy(xpath="//div[@id='catering']//div[5]//div[1]//div[1]//img[1]")
	public WebElement bigBirthDayCeleberation;
	
	@FindBy(xpath="//div[@id='catering']//div[6]//div[1]//div[1]//img[1]")
	public WebElement birthdayCeleberation;
	
	@FindBy(xpath="//div[@id='catering']//div[7]//div[1]//div[1]//img[1]")
	public WebElement crowdPleaser;
	
	@FindBy(xpath="//div[@id='catering']//div[8]//div[1]//div[1]//img[1]")
	public WebElement iceCreamBar;
	
	@FindBy(xpath="//div[@id='catering']//div[9]//div[1]//div[1]//img[1]")
	public WebElement officeParty;
	
	@FindBy(xpath="//div[10]//div[1]//div[1]//img[1]")
	public WebElement partyStarter;
	
	@FindBy(xpath="//div[11]//div[1]//div[1]//img[1]")
	public WebElement sweetToothSampler;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[7]/div/div[1]/button")
	public WebElement BigBdayCelebPickForMe1;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[8]/div/div[1]/button")
	public WebElement BigBdayCelebPickForMe2;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[4]/div/div[1]/button")
	public WebElement BdayCelebPickForme;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[10]/div/div/button")
	public WebElement AddProduct_Catering;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[1]/div/div[1]/button")
	public WebElement IceCreamBarPickForMe1;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[2]/div/div[1]/button")
	public WebElement IceCreamBarPickForMe2;
	
	@FindBy(xpath="//input[@id='delivery-datepicker']")
	public WebElement deliveryDate;
	
	@FindBy(xpath="//div[@id='ui-datepicker-div']")
	public WebElement deliveryDateCalender;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[1]/div/div[1]/button")
	public WebElement officePartyPickForMe1;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[2]/div/div[1]/button")
	public WebElement officePartyPickForMe2;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[1]/div/div[1]/button")
	public WebElement PartyStarterPickForMe1;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[2]/div/div[1]/button")
	public WebElement PartyStarterPickForMe2;
	
	@FindBy(xpath="//*[@id='modal-content']/div[2]/div/div/div[2]/div[3]/div/div[1]/button")
	public WebElement PartyStarterPickForMe3;
	
	
	@FindBy(xpath="//*[@class='btn btn-primary btn-block pick-for-me']")
	public WebElement pickForMe;
	
	@FindBy(xpath="//*[@class='btn btn-primary btn-block pick-for-me']")
	public List<WebElement> pickForMeList;
	
	@FindBy(xpath="//*[@id='catering']/div[1]/div/div/div[1]/div/div[1]/img")
	public WebElement FiftyCookies;
	
	@FindBy(xpath="//*[@id='catering']/div[1]/div/div/div[2]/div/div[1]/img")
	public WebElement HundredCookies;
	
	@FindBy(xpath="//*[@id='catering']/div[1]/div/div/div[3]/div/div[1]/img")
	public WebElement TwoHundredCookies;
	
	@FindBy(xpath="//*[@id='catering']/div[1]/div/div/div[4]/div/div[1]/img")
	public WebElement ThreeHundredCookies;
	
	@FindBy(xpath="//div[@class='growl-message']")
	public WebElement errorMsgTenToSix;
	
}
