package com.qa.stepDefinition;

import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.qa.pages.SignUp;
import com.qa.pages.AdminStores;
import com.qa.pages.Order;
import com.qa.pages.SignIn;

public class Step_SignIn extends TestBase {

	// public WebDriver driver;
	String url = getUrl();
	/*
	 * public step_signin() { driver = Hooks.driver; // TODO Auto-generated
	 * constructor stub }
	 */
	// ConfigFileReader configFileReader = new ConfigFileReader();
	WebDriver driver = getDriver();
	WebDriverWait wait = new WebDriverWait(driver, 12);
	// public Map<String, String> url;
	SignIn s = new SignIn(driver);
	Order order = new Order(driver);
	SignUp signup = new SignUp(driver);
	// ConfigFileReader configFileReader= new ConfigFileReader();

	/*
	 * @Given("^User opens the Website$") public void user_opens_the_Website()
	 * throws Throwable { driver.get(configFileReader.getApplicationUrl());
	 * System.out.println(configFileReader.getApplicationUrl()); Thread.sleep(3000);
	 * }
	 */

	@Given("^User opens the Website$")
	public void user_opens_the_Website(/* DataTable table */) throws Throwable {
		/*
		 * List<List<String>> data = table.raw();
		 * System.out.println(data.get(0).get(1)); driver.get(data.get(0).get(1));
		 */

		/*
		 * if(url==null){ url=configFileReader.getApplicationUrl();
		 */

//	    #############   Clear browser History, this is required when running order test cases along with admin site cases in single session   ############
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();
		if (browserName.equalsIgnoreCase("firefox")) {
			clear_Firefox_History_and_Cookies(driver);
			driver.get(url);
		} else if (browserName.equalsIgnoreCase("chrome")) {
			clear_Chrome_History_and_Cookies(driver);
			driver.get(url);
		}

	}

	@Given("^User opens the Website without clearing cache$")
	public void user_opens_the_Website_for_Ship_Orders(/* DataTable table */) throws Throwable {

		driver.get(url);
//		   		Thread.sleep(5000);
//				try {
//					if(order.alt_Continue.isDisplayed()) {
//						order.okay_alert.click();
//					} 
//					} catch(Exception e) {
//						System.out.println("##################### ----Confirm Changing store alert not displayed");
//					}
	}

	@And("^if user is already logged in- just logout$")
	public void logout_if_already_logged_in() throws Throwable {
		WebDriverWait waitlocal = new WebDriverWait(driver, 8);
		try {
			waitlocal.until(ExpectedConditions.elementToBeClickable(s.login_button));
			System.out.println("Logs in to the Website");
			s.login_button.click();
		} catch (Exception e) {
			e.printStackTrace();
			s.login_text.click();
			waitlocal.until(ExpectedConditions.elementToBeClickable(s.logout));
			s.logout.click();

		}

	}

//	@And("^if user is already logged out- just login$")
//	public void login_if_already_logged_out() throws Throwable{
//		WebDriverWait waitlocal = new WebDriverWait(driver, 8);
//		try{			
//			waitlocal.until(ExpectedConditions.elementToBeClickable(s.login_button));
//			System.out.println("Logs in to the Website");
//			s.login_button.click();
//		}catch(Exception e){
//			e.printStackTrace();
//			s.login_text.click();
//			waitlocal.until(ExpectedConditions.elementToBeClickable(s.logout));
//				s.logout.click();
//			
//		}
//		
//	}

	@Given("^Click on Log In link present in top right corner of the homepage$")
	public void click_on_Login_button_in_Header() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(s.login_button));
		s.login_button.click();

	}

	@When("^User enters valid Email and Password$")
	public void under_SIGN_IN_section_enter_valid_Email_and_Password(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.elementToBeClickable(s.username));
		s.username.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(s.password));
		s.password.sendKeys(data.get(1).get(1));

	}

	@When("^User enters valid Email and incorrect Password$")
	public void under_SIGN_IN_section_enter_valid_Email_and_InvalidPassword(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.elementToBeClickable(s.username));
		s.username.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(s.password));
		s.password.sendKeys(data.get(1).get(1));

	}

	@Then("^verify user validation message stating Email and password do not match is displayed$")
	public void verify_email_Password_dontmatch_message() {
		wait.until(ExpectedConditions.visibilityOf(s.emailPasswordMismatch_messsage));
		Assert.assertEquals("Validation check not displayed for email and password mismatch",
				"Email and password do not match.", s.emailPasswordMismatch_messsage.getText());

	}

	@When("^User enters the invalid Email and Password$")
	public void under_SIGN_IN_section_enter_invalid_Email_and_Password(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.elementToBeClickable(s.username));
		s.username.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(s.password));
		s.password.sendKeys(data.get(1).get(1));

	}

	@When("^Click on the Log In button$")
	public void click_on_Submit() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(s.submit_login));
		s.submit_login.click();
		try {
			wait.until(ExpectedConditions.elementToBeClickable(s.login_text));
		} catch (Exception e) {

			e.getMessage();
		}
	}

	@When("^User clicks Logout button$")
	public void click_logout() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(s.logout));
		s.logout.click();

	}

	@Then("^User must get logged in and UserName should be displayed in the header$")
	public void user_must_get_login_in_and_UserName_should_be_displayed_in_the_header() throws Throwable {

		try {

			wait.until(ExpectedConditions.elementToBeClickable(s.login_text));
			String str = new String();
			str = s.login_text.getText();
			System.out.println(str);
			if (str.contains("Hi")) {
				System.out.println("Test Case Passed");
			} else {
				System.out.println("Test Case Failed");
			}
		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(s.login_text));
			s.login_text.click();
			wait.until(ExpectedConditions.elementToBeClickable(s.logout));
			s.logout.click();
		}
	}

	@Then("^Error popup should appear stating Email and password do not match$")
	public void error_popup_should_appear_stating_Email_and_password_do_not_match() throws Throwable {
		wait.until(ExpectedConditions.visibilityOf(signup.alert));
		Assert.assertEquals("Email and password do not match.", signup.alert.getText());

	}

	@Then("^Alert should appear saying Please enter an email address$")
	public void alert_should_appear_saying_Please_enter_an_email_address() throws Throwable {

		// Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(s.alert_email));
		Assert.assertEquals("Please enter an email address.", s.alert_email.getText());
	}

	@Then("^The password field is required$")
	public void the_password_field_is_required() throws Throwable {
		// Thread.sleep(2000);

		wait.until(ExpectedConditions.visibilityOf(s.alert_password));
		Assert.assertEquals("The password field is required.", s.alert_password.getText());
	}

	@And("^User should logout successfully$")
	public void User_should_logout_successfully() throws Throwable {

		try {

			Assert.assertEquals("log In", s.login_button.getText());

			System.out.println("Logged out successfully");
			Thread.sleep(5000);
		} catch (NoSuchElementException e) {
			System.out.println("User is still Logged In");
		}
	}

	@Then("^Verify validation message stating email ID and password required should be displayed$")
	public void validation_message_email_password() {
		wait.until(ExpectedConditions.visibilityOf(s.emailIDValidationMsg));
		Assert.assertEquals("Email ID required validation not applied", "Please enter an email address.",
				s.emailIDValidationMsg.getText());
		Assert.assertEquals("Password required validation not applied", "The password field is required.",
				s.passwordValidationMsg.getText());

	}

	@And("^Under Order time stamp click on Add to cart and continue shopping$")
	public void click_AddtoCart_andContinueBtn() {
		wait.until(ExpectedConditions.visibilityOf(s.addToCart_and_Continue));
		s.addToCart_and_Continue.click();
	}

	@And("^Under Order time stamp click on Track Order button$")
	public void click_TrackOrder() {
		wait.until(ExpectedConditions.visibilityOf(s.trackOrder));
		s.trackOrder.click();
	}

	@When("^On home page enter the address$")
	public void user_enters_the_address_onHome(DataTable table) throws Throwable {
		WebDriverWait waitlocal = new WebDriverWait(driver, 6);
		waitlocal.until(ExpectedConditions.visibilityOf(s.homePageAddressInput));
		// log.info("waiting for address textbox to appear");
		/*
		 * new WebDriverWait(driver, 20).until( webDriver -> ((JavascriptExecutor)
		 * webDriver).executeScript("return document.readyState").equals( "complete"));
		 */
//		String selectAll = Keys.chord(Keys.CONTROL, "a");
//		s.homePageAddressInput.sendKeys(selectAll, Keys.DELETE);
//		boolean verify_text = s.homePageAddressInput.getAttribute("value").isEmpty();
//		System.out.println("Value present in Address testbox :" + verify_text);

		List<List<String>> data = table.raw();
		// Thread.sleep(5000);
		s.homePageAddressInput.sendKeys(data.get(0).get(0));
		System.out.println("Entered address is: " + s.homePageAddressInput.getAttribute("value"));
		Thread.sleep(2000);
		s.homePageAddressInput.sendKeys(Keys.DOWN);
		s.homePageAddressInput.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
//		try {
//			if (order.okayToClearCart.isDisplayed()) {
//				order.okayToClearCart.click();
//			}
//		} catch (Exception e) {
//			System.out.println("cart is already empty");
//
//		}

	}

	@And("^click Continue to navigatge to Orders page$")
	public void click_Continue_HomePage() {
		wait.until(ExpectedConditions.visibilityOf(s.homePageContinueBtn));
		s.homePageContinueBtn.click();
	}

	@Given("Click on Sign Up button on home page")
	public void click_SignUp_Btn() {
		wait.until(ExpectedConditions.visibilityOf(s.homePageSignUp));
		s.homePageSignUp.click();
	}

	@Then("Verify Sign up page gets displayed")
	public void verify_SignUp_Page_isDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(s.signUpNewCustomer));
		System.out.println("User redirected to SignUp page++++++++++ Success");
	}

	@Given("Click on Learn More button on home page")
	public void click_LearnMore_Btn() {
		wait.until(ExpectedConditions.visibilityOf(s.homePageLearnMore));
		s.homePageLearnMore.click();
	}

	@Then("Verify Get started page is displayed")
	public void verify_GetStarted_Page_isDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(s.getStartedPageText));
		System.out.println("User redirected to Get Started page++++++++++ Success");
	}

	@Given("On home page user clicks Order Now button under We ship NationWide section")
	public void click_OrderNow_Btn() {
		wait.until(ExpectedConditions.visibilityOf(s.homePageOrderNow));
		s.homePageOrderNow.click();
	}

	@Then("Verify User gets redirected to ship cookies menu page")
	public void verify_ShipCookies_Page_isDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(s.shipCookiesTextOnShipCookiesPage));
		System.out.println("User redirected to ShipCookies page++++++++++ Success");
	}
}
