@gear_SignUp
Feature: Gear
  
  Background: user access to website
    Given User opens the Website

@DevGear  
  Scenario: Verify user is able to order gears using credit card
    When user clicks on Gifts tab
    And user clicks Gear option
    And user selects moon tee
    And user clicks add product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Shaurya Nigam     |
      | customer phone     |        3457689024 |
      | customer emailID   | snigam@judge.com  |
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue bryn mawr |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
      |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  
   