@MyAccount
Feature: My Account

  Background: user access to website
    Given User opens the Website
    And if user is already logged in- just logout

#@MyAccountDebug1


Scenario: TC001 - Logged In User - Verify user is able to access Order History  Page on logging in with current customer credentials
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com |
      | password | Judge@123			          |
    And Click on the Log In button
    Then User should be redirected to Order History Page
    

Scenario: TC003 - Verify user information is displayed on logging in thorugh Current customer credentials
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com |
      | password | Judge@123			          |
    And Click on the Log In button
    Then First Name,Last Name ,Email & Phone should be displayed under My Account Section.  
    
  
Scenario: TC004 - Verify Address information is displayed on logging in thorugh Current customer credentials
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | pnehra@judge.com				  |
      | password | Judge@123			          |
    And Click on the Log In button
    Then Address should be displayed under Address Book Section.

     
Scenario: TC005 - Verify Loyalty information  is displayed on logging in thorugh Current customer credentials
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com |
      | password | Judge@123			          |
    And Click on the Log In button
    Then Loyalty No., points,Rewards and Next reward should be displayed under Loyalty section. 

 
Scenario: TC006 - Verify Order History is being displayed in descensing order on My Account Page
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com |
      | password | Judge@123			          |
    And Click on the Log In button
    Then Order History should be displayed in descensing order number.

 
Scenario: TC007 - Verify user is able view entire order history from My Account Page by clicking "load more" link.
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com |
      | password | Judge@123			          |
    And Click on the Log In button
    And Click Load More link under the orders
    Then Entire Order History should be displayed


Scenario: TC008 - Verify user is able to edit profile page.
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | pnehra@judge.com |
      | password | Judge@123         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    Then Update FirstName, LastName, Email, Phone and click Update Profile button
    | First Name 	| Prateek123 	|
    | Last Name		| Test 				|
    | Phone Number| 8967589476 	|


Scenario: TC010 - Verify user is able to add an Address using view all link on Address Book section
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | pnehra@judge.com |
      | password | Judge@123         |
    And Click on the Log In button
    And Click on "view all" link in Address Book section
    And Click on "New Address" link
    And In the opened dialog, enter valid inputs in all the fields.
    | First Name 	| Prateek123 										|
    | Last Name		| Nehra 												|
    | Nick Name		| Office				 								|
    | Address			| 8 East Broad Street Greenville South Carolina 29601 	|
    Then Click on Save button and verify the new address got added successfully
 
   
 Scenario: TC011 - Verify user is able to edit the address.
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | pnehra@judge.com |
      | password | Judge@123         |
    And Click on the Log In button
    And Click on "view all" link in Address Book section
    And Click on "Edit Address" link
    Then change the address and click on save button and verify the address is updated successfully
    | Address			| 871 7th Avenue, New York, NY 	|
 

 Scenario: TC012 - Verify user is able to delete the Address Book
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on "view all" link in Address Book section
    And Click on "New Address" link
    And In the opened dialog, enter valid inputs in all the fields.
    | First Name 	| Test 										|
    | Last Name		| Nest 													|
    | Nick Name		| Office				 								|
    | Address			| XYZ Art Gallery, North Main Street, Blacksburg, VA 	|
    Then Click on Save button and after saving delete the address 
     
    Scenario: TC013 - Verify user is able to re-order from My Account Page.
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com 		|
      | password | Judge@123				        	 	|
    And Click on the Log In button
    And Click on Checkout Button
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
  
  	Scenario: TC014 - Verify  credit card information is displayed under Credit Cards section.
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com 		|
      | password | Judge@123				        	 	|
    And Click on the Log In button
    Then verify saved card info is displayed
				
		Scenario: TC015 - Verify  credit card information is displayed under Credit Cards section.
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com 		|
      | password | Judge@123				        	 	|
    And Click on the Log In button
    And Click on "view all" in Credit cards section.
		Then Click "delete" present in front of credit cards displayed and verify CC deleted successfully 		
		
		Scenario: TC016 - Verify in case saved credit cards is removed/no credit card is saved, the message You do not have any saved credit cards. is displayed under Credit Cards section
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test@teser.com 		|
      | password | 123456				   	 	|
    And Click on the Log In button
    Then verify no CC is saved and the message is displayed		
		
		Scenario: TC018 - Verify email textbox on profile page displays signup username on logging in 
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com 		|
      | password | Judge@123				        	 	|
    And Click on the Log In button
    And Click on the edit button to update the profile
		Then verify the email textbox contains the username entered at the time of signup
		
	
		Scenario: TC019 - Verify email displayed on profile page on logging in  is non-editable
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com 		|
      | password | Judge@123				        	 	|
    And Click on the Log In button
    And Click on the edit button to update the profile
		Then verify the email textbox is disabled
		
	
		Scenario: TC020 - Verify First Name updated on profile page is displayed on top right corner of the page
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test@teser.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    Then Update FirstName and click Update Profile button and verify updated firstname is displayed on top right of the profile page
    | First Name 	| Prateek1234 			|
    
  
    Scenario: TC021 - Verify user is able to update password on profile page.
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | password@test.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    And Enter Current Password
    | Current Password 	| 123456 				|
    And Enter New Password
    | New Password 			| 1234567 			|
    And Enter Confirm Password
    | Confirm Password 	| 1234567 			|  
    Then click update password and verify password changed successfully and change the password back to original
    | Current Password 	| 1234567 		|
    | New Password 			| 123456 			|
    | Confirm Password 	| 123456 			|
    
   
    Scenario: TC022 - Verify current password box hides the data entered in it
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | pnehra@judge.com |
      | password | Judge@123         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    And Enter Current Password
    | Current Password 	| 123456 				|
    Then verify password entered should be hidden in current password box 
    
    Scenario: TC023 - Verify New password box hides the data entered in it
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | pnehra@judge.com |
      | password | Judge@123         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    And Enter New Password
    | Current Password 	| 123456 				|
    Then verify password entered should be hidden in New password box     
    
    Scenario: TC024 - Verify Confirm password box hides the data entered in it
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | pnehra@judge.com |
      | password | Judge@123         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    And Enter Confirm Password
    | Current Password 	| 123456 				|
    Then verify password entered should be hidden in Confirm password box
       
    Scenario: TC025 - Verify pop-up is displayed in case User click Update Password w/o entering password in current Password textbox
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    And Enter New Password
    | New Password 			| 1234567 			|
    And Enter Confirm Password
    | Confirm Password 	| 1234567 			|  
    Then Click Update Password and Verify pop-up should be displayed saying The current password field is required
        
    Scenario: TC026 - Verify pop-up is displayed provided user is clicks Update Password w/o entering New Password
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    And Enter Current Password
    | Current Password 	| 123456 				|
    And Enter Confirm Password
    | Confirm Password 	| 1234567 			|  
    Then Click Update Password and Verify pop-up should be displayed saying The New password field is required    
   
    Scenario: TC027 - Verify pop-up is displayed provided user clicks Update Password w/o entering Confirm Password
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    And Enter Current Password
    | Current Password 	| 123456 				|
    And Enter New Password
    | New Password 			| 1234567 			|  
    Then Click Update Password and Verify pop-up should be displayed saying The Confirm password field is required

    Scenario: TC028 - Verify pop-up is displayed in case New & Confirm passwords do not match
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile  
    And Enter Current Password
    | Current Password 	| 123456 				| 
    And Enter New Password
    | New Password 			| 12345678 			|  
    And Enter Confirm Password
    | Current Password 	| 1234567 				|
    Then Click Update Password and Verify pop-up should be displayed stating The new password confirmation and new password must match
    
 
    Scenario: TC029 - Verify pop-up is displayed in case user tries to update with wrong current password
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile  
    And Enter Current Password
    | Current Password 	| 12345678 				| 
    And Enter New Password
    | New Password 			| 1234567 			|  
    And Enter Confirm Password
    | Current Password 	| 1234567 				|
    Then Click Update Password and Verify pop-up should be displayed stating The current password entered does not match the password on your account
    
     
    Scenario: TC030 - Verify pop-up is displayed in case user tries to keep New password same as current password
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile  
    And Enter Current Password
    | Current Password 	| 1234567 			| 
    And Enter New Password
    | New Password 			| 1234567 			|  
    And Enter Confirm Password
    | Current Password 	| 1234567 			|
    Then Click Update Password and Verify pop-up should be displayed stating The new password and current password must be different
    
  
    Scenario: TC031 - Verify user is not able to update with new password length < 5 
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile  
    And Enter Current Password
    | Current Password 	| 1234567 				| 
    And Enter New Password
    | New Password 			| 1234 			|  
    And Enter Confirm Password
    | Current Password 	| 1234				|
    Then Click Update Password and Verify pop-up should be displayed stating The new password must be at least 5 characters.
    
    
    Scenario: TC032 - Verify User is able to check & uncheck "Remember Credit cards" checkbox on Profile page 
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile  
    Then Verify user is able to check/uncheck Remember Credit Cards check box    
    
 
    Scenario: TC033 - Verify User is able to check & uncheck "Allow push notifications. (If disabled, you will still get order tracking notifications.)" checkbox on Profile page 
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile  
    Then Verify by default Allow push notifications checkbox is checked
    Then Uncheck and verify the Allow push notifications checkbox is unchecked
     
 		
 		Scenario: TC034 - Verify User is able to check & uncheck "Are you responsible for ordering for large groups?" checkbox on Profile page 
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile  
    Then Verify user is able to check/uncheck Are you responsible for ordering for large groups check box
    
 
    Scenario: TC035 - Verify User is able to check & uncheck "Are you interested in learning more about Insomnia Cookies Event offerings?" checkbox on Profile page 
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile  
    Then Verify user is able to check/uncheck event offerings check box
    
  
    Scenario: TC036 - Verify User is able to check & uncheck "Are you responsible for ordering for large groups?" checkbox on Profile page 
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile  
    Then Verify user is able to check/uncheck weekly updates check box
 
  		  
    Scenario: TC037 - Verify phone no. length > 10 should not be allowed to enter
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    Then Verify phone number more than 10 digits is not allowed
    | Phone Number|67589789561|
    
     
    Scenario: TC039 - Verify user is not able to submit phone < 10 digit
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on the edit button to update the profile
    Then Verify phone number less than 10 digits is not allowed
    | Phone Number|675897895|
 
        
    Scenario: TC040 - Verify user is able to logout using Logout link under Logout section 
		Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click the logout button
    Then verify user logsout successfully
          
    Scenario: TC041 - Verify First Name, Last Name ,NickName, Address, Apt/Suites, Dorm added previously should be displayed in the address dialogbox.
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test@teser.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on "view all" link in Address Book section
    And Click on "Edit Address" link
    Then In the opened dialog, validate FirstName, LastName, NickName, Address, Apt/Suites, Dorm
    | First Name 	| Prateek		 																	|
    | Last Name		| TestName 																		|
    | Nick Name		| Tester				 															|
    | Address			| 1084 East Lancaster Avenue, Bryn Mawr, PA 	|
    | Apt/Suite		| Test Suites																	|
    | Dorm Name		| Denbigh																			|
    
      
    Scenario: TC042 - Verify user is able to mark favourite address by clicking star image.
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on "view all" link in Address Book section
    Then verify by clicking star the address becomes favorite
       
    Scenario: TC044 - Verify favourite address moves to top of the Address Book
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on "view all" link in Address Book section
    Then verify by clicking star the address moves to top
         
    Scenario: TC045 - Verify favourite address is displayed under Address book section
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on "view all" link in Address Book section
    Then verify favourite address is displayed under Address book section     
     
    Scenario: TC047 - Verify user can change a previously  marked favourite address  
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on "view all" link in Address Book section
    Then verify by clicking star the address becomes favorite
        
    Scenario: TC048 - Verify pop-up appears saying "Your default address has been changed" in case user marks/changes the Favourite Address  
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | test123@tester.com |
      | password | 123456         |
    And Click on the Log In button
    And Click on "view all" link in Address Book section
    Then verify by clicking star pop up appears stating Your default address had been changed
    
    Scenario: TC050 - User is able select tip  from Order History on My Account page.
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com |
      | password | Judge@123				         |
    And Click on the Log In button
    And User selects tip
    And Click on Checkout Button
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
   
    Scenario: TC051 - User should able to place order with "Add to cart & Continue Shopping" button successfully.
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com |
      | password | Judge@123				         |
    And Click on the Log In button
    And Under Order time stamp click on Add to cart and continue shopping
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
      
    Scenario: TC052 - User should redirect to Tracker.
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra1@gmail.com |
      | password | Judge@123				         |
    And Click on the Log In button
    And Under Order time stamp click on Track Order button
    Then Tracking ID should be displayed along with status of order
   
    Scenario: TC053 - User should be able to mark favourite credit card by clicking star image displayed in front of credits.
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra@gmail.com 	|
      | password | Judge@123				         	|
    And Click on the Log In button
    And Click on "view all" in Credit cards section.
    Then verify by clicking star the CC becomes favorite
		
    
    
    