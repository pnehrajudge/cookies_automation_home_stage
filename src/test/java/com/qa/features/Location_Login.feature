@Location
Feature: Title of your feature
  I want to use this template for my feature file

  Background: user access to website
    Given User opens the Website without clearing cache

@DevLocationLogin
  Scenario: Precondition - user logs in as a current customer
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | snigam@judge.com |
      | password | password         |
    And Click on the Log In button

  Scenario: Verify user is able to access Location tab
    And user clicks on Location tab
    Then map and search field should be displayed on the page

@DevLocationLogin
  Scenario: Verify user is not able to search with Address field leaving blank
    And user clicks on Location tab
    And user clicks search button
    Then popup appears stating
      | The address you entered cannot be found |

@DevLocationLogin
  Scenario: Verify user is able to search with valid Address
    And user clicks on Location tab
    When user enters the address in location search box
      | 1084 East Lancaster Avenue, Bryn Mawr, PA |
    And user clicks search button
    Then Nearest store locations should be displayed
      | Bryn Mawr                           |
      | Manayunk                            |
      | Test Store               						|
      | University City North               |
      | University City South               |
      | Templetown                          |
      | 16th St. Philadelphia / Center City |
      | 13th St. Philadelphia / Center City |
      | Old City Philadelphia, PA           |
      | West Chester, PA                    |

  Scenario: Verify nearest stores are displayed below the searched address along with their distance from the searched address
    And user clicks on Location tab
    When user enters the address in location search box
      | 1084 East Lancaster Avenue, Bryn Mawr, PA |
    And user clicks search button
    Then distance of nearest stores should be displayed

  Scenario: Verify suggested store displays the address, miles and info. link
    And user clicks on Location tab
    When user enters the address in location search box
      | 1084 East Lancaster Avenue, Bryn Mawr, PA |
    And user clicks search button
    Then info links should be displayed
    And distance of nearest stores should be displayed
    And Addresses of the nearest store should be displayed

  Scenario: Verify user redirects to store info page on clicking info link
    And user clicks on Location tab
    When user enters the address in location search box
      | 1084 East Lancaster Avenue, Bryn Mawr, PA |
    And user clicks search button
    Then info link of each store displayed is clickable
    

  @debug
  Scenario: Verify store info page displays :
    - Delivery Location
    - Address
    - Contact No.
    - Delivery hours
    - Retailing Hours
    - Order Now button
    - Store Image

    And user clicks on Location tab
    When user enters the address in location search box
      | 1084 E Lancaster Ave, Bryn Mawr, PA 19010 |
    And user clicks search button
    And user clicks info link of store
    Then store info page should display  Delivery Location, Address, Contact No, Delivery hours, Retailing Hours, Order Now button, Store Image

  Scenario: Verify store info page closes by clicking 'X' button at top right corner of the page below search field
    And user clicks on Location tab
    When user enters the address in location search box
      | 1084 East Lancaster Avenue, Bryn Mawr, PA |
    And user clicks search button
    Then info link of each store displayed is clickable
    
@DevLocationLogin
  Scenario: Verify user is able to place Delivery Order using Locations Tab
    And user clicks on Location tab
    When user enters the address in location search box
      | 1084 E Lancaster Ave Bryn Mawr, PA 19010 |
    And user clicks search button
    And user clicks info link of store
    And user clicks Order now button
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    #And user enters Valid Details under Delivery Info & your Info
      #| recepient name   | Theresa Ainsworth |
      #| recepient phone  |        2345678981 |
      #| customer name    | Shaurya Nigam     |
      #| customer phone   |        3457689024 |
      #| customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as cash
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

  Scenario: Verify user is able to place Pickup Order using Locations Tab
    And user clicks on Location tab
    When user enters the address in location search box
      | 1084 E Lancaster Ave Bryn Mawr, PA 19010 |
    And user clicks search button
    And user clicks info link of store
    And user clicks Order now button
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    #And user enters into your Info
      #| customer name    | Shaurya Nigam    |
      #| customer phone   |       3457689024 |
      #| customer emailID | snigam@judge.com |
    And user selects Payment Method as cash
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

@DevLocationLogin
  Scenario: Verify user is able to search stores based on Zip Code and Miles
    And user clicks on Location tab
    When user enters the address in location search box
      | 19010 |
    And user clicks search button
    Then Addresses of the nearest store should be displayed

  Scenario: Verify user is able to search stores based on state
    And user clicks on Location tab
    When user enters the address in location search box
      | Philadelphia, PA |
    And user clicks search button
    Then Addresses of the nearest store should be displayed
