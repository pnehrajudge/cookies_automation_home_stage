package com.qa.stepDefinition;

import static org.junit.Assert.assertThat;
//import static org.testng.AssertJUnit.assertEquals;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import com.qa.pages.AdminCallCenter;
import com.qa.pages.Catering;
import com.qa.pages.Order;
import com.qa.pages.SignUp;
import com.qa.pages.SignIn;

public class Step_Order extends TestBase {

	WebDriver driver = getDriver();
	AdminCallCenter adminCallCenter = new AdminCallCenter(driver);
	Catering catering = new Catering(driver);
	Order order = new Order(driver);
	SignUp signup = new SignUp(driver);
	SignIn s = new SignIn(driver);
	Actions action = new Actions(driver);
	JavascriptExecutor jse = (JavascriptExecutor) driver;

	// Logger log= Logger.getLogger(step_Order.class);

	Logger log = Logger.getLogger("shauryaLogger");

	WebDriverWait wait = new WebDriverWait(driver, 30);

	@Given("^user clicks on Order button$")
	public void user_clicks_on_Order_button() throws Throwable {
		WebDriverWait waitlocal = new WebDriverWait(driver, 6);
		try {

			waitlocal.until(ExpectedConditions.elementToBeClickable(order.order_button));
			order.order_button.click();
			// log.info("order button is clicked");
			// log.debug("order button is clicked");
			// Thread.sleep(3000);
		} catch (Exception e) {

			waitlocal.until(ExpectedConditions.elementToBeClickable(order.new_cart_button));
			// log.info("waiting for cart button to appear");
			// log.debug("waiting for cart button to appear");

			order.new_cart_button.click();
			// log.info("cart button is clicked");
			// log.debug("cart button is clicked");
			order.delete_product(driver);
			// log.info("delete product from the cart");
			// log.debug("delete product from the cart");
			waitlocal.until(ExpectedConditions.elementToBeClickable(order.order_button));
			// log.debug("waiting for cart button to appear");
			order.order_button.click();

		}
		try {
			Thread.sleep(3000);
			if (order.confirm_changing_store.isDisplayed()) {

				order.okay_alert.click();
				System.out.println("########Change store alert button is clicked");
				driver.get(url);
				// log.info("Change store alert button is clicked");
			}
		} catch (Exception j) {
			j.printStackTrace();
			System.out.println("#########Change store alert is not displayed");
			// log.info("Change store alert is not displayed");
		}
	}

	@When("^user enters the address$")
	public void user_enters_the_address(DataTable table) throws Throwable {
		WebDriverWait waitlocal = new WebDriverWait(driver, 6);
		waitlocal.until(ExpectedConditions.visibilityOf(order.address_box));
		// log.info("waiting for address textbox to appear");
		/*
		 * new WebDriverWait(driver, 20).until( webDriver -> ((JavascriptExecutor)
		 * webDriver).executeScript("return document.readyState").equals( "complete"));
		 */
		String selectAll = Keys.chord(Keys.CONTROL, "a");
		order.address_box.sendKeys(selectAll, Keys.DELETE);
		boolean verify_text = order.address_box.getAttribute("value").isEmpty();
		System.out.println("Value present in Address testbox :" + verify_text);
		// log.info("Value present in Address testbox :"+verify_text);

		List<List<String>> data = table.raw();
		// Thread.sleep(5000);
		order.address_box.sendKeys(data.get(0).get(1));
		System.out.println("Entered address is: " + order.address_box.getAttribute("value"));
		Thread.sleep(2000);
		order.address_box.sendKeys(Keys.DOWN);
		order.address_box.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		try {
			if (order.okayToClearCart.isDisplayed()) {
				order.okayToClearCart.click();
			}
		} catch (Exception e) {
			System.out.println("cart is already empty");

		}
//		new WebDriverWait(driver, 30).until(webDriver -> ((JavascriptExecutor) webDriver)
//				.executeScript("return document.readyState").equals("complete"));

	}

	@And("^user clicks on Track Order button$")
	public void track_order() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(order.track_order));
		log.info("waiting for Track Order button to appear");
		order.track_order.click();
		log.info("Track Order button is clicked");
	}

	@When("^user clicks on Delivery button$")
	public void user_clicks_on_Delivery_button() throws Throwable {
		WebDriverWait waitlocal = new WebDriverWait(driver, 15);
		try {
			/*
			 * new WebDriverWait(driver, 20).until( webDriver -> ((JavascriptExecutor)
			 * webDriver).executeScript("return document.readyState").equals( "complete"));
			 */
			Thread.sleep(5000);			
			jse.executeScript("window.scrollBy(0,100)");
//			Thread.sleep(1000);
//			jse.executeScript("arguments[0].scrollIntoView(true);", order.alt_delivery_button);
			waitlocal.until(ExpectedConditions.visibilityOf(order.alt_delivery_button));
//			wait.until(ExpectedConditions.elementToBeClickable(order.delivery_button));
			log.info("waiting for Delivery button to be clickable");
			// order.delivery_button.click();

			action.moveToElement(order.alt_delivery_button).build().perform();
			Thread.sleep(5000);
//			jse.executeScript("arguments[0].click()", order.alt_delivery_button);
			order.alt_delivery_button.click();
			log.info("Delivery button is clicked");

		} catch (Exception e) {

			/*
			 * try{ JavascriptExecutor jse = (JavascriptExecutor)driver;
			 * jse.executeScript("return arguments[0].click()", order.delivery_button);
			 * log.info(" Delivery button is clicked using javascriptExecutor");
			 * }catch(Exception r){
			 */
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			// jse.executeScript("return arguments[0].click()",
			// order.alt_delivery_button);

			waitlocal.until(ExpectedConditions.elementToBeClickable(order.delivery_button)); // delivery_button
			action.moveToElement(order.delivery_button).build().perform();
//			Thread.sleep(5000);
			// order.delivery_button.click();
			jse.executeScript("arguments[0].click()", order.delivery_button);

			// }

		}
		try {
			// Thread.sleep(5000);
			waitlocal.until(ExpectedConditions.elementToBeClickable(order.continue_alert));
			// Actions action = new Actions(driver);

			action.moveToElement(order.continue_alert).build().perform();
//			Thread.sleep(5000);
			// order.continue_alert.click();

			log.info("waiting for Continue button in the alert to be clickable");
			// order.continue_alert.click();
			action.doubleClick(order.continue_alert).perform();
			// order.continue_alert.click();
			log.info("Continue button is clicked");

		} catch (Exception e) {
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			// jse.executeScript("return arguments[0].click()",
			// order.alt_continue_alert);

			waitlocal.until(ExpectedConditions.elementToBeClickable(order.alt_continue_alert));
			// Actions action = new Actions(driver);

			action.moveToElement(order.alt_continue_alert).build().perform();
//			Thread.sleep(5000);
			// order.continue_alert.click();

			log.info("waiting for Continue button in the alert to be clickable");
			// order.alt_continue_alert.click();
			action.doubleClick(order.alt_continue_alert).perform();
			log.info(" Delivery button in the alert is clicked using javascriptExecutor");
		}
	}

	@When("^user select Date and Time from Calendar$")
	public void user_select_Date_and_Time_from_Calendar() throws Throwable {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.time_dropdown));
			log.info("waiting for Time Dropdown to be clickable");
			Select time_dropdown = new Select(order.time_dropdown);
			Thread.sleep(3000);
			time_dropdown.selectByIndex(4);
			log.info("Time Dropdown - 4th option is selected");

		} catch (Exception e) {
			try {
				Select time_dropdown = new Select(order.alt_time_dropdown);
				time_dropdown.selectByIndex(4);
				log.info("Time Dropdown - 4th option is selected");
			} catch (Exception ex) {
				try {
//					 jse.executeScript("return arguments[0].click()", catering.deliveryDate);
					Thread.sleep(1000);
					catering.deliveryDate.click();
					Thread.sleep(2000);

					String tomorrow = adminCallCenter.getCurrentDay();
					System.out.println("Today's number: " + tomorrow + "\n");
					List<WebElement> columns = catering.deliveryDateCalender.findElements(By.tagName("td"));

					for (WebElement cell : columns) {
						if (cell.getText().equals(tomorrow)) {
							cell.click();
							break;
						}
					}
					wait.until(ExpectedConditions.elementToBeClickable(order.time_dropdown));
					Select time_dropdown = new Select(order.time_dropdown);
					Thread.sleep(3000);
					time_dropdown.selectByIndex(4);
//					time_dropdown.selectByIndex(4);

				} catch (Exception exe) {

					Select time_dropdown = new Select(order.alt_time_dropdown);
					time_dropdown.selectByIndex(4);

					// System.out.println("running exception block");
				}
			}
			// System.out.println("running exception block");
		}

	}

	@When("^user clicks on Continue$")
	public void user_clicks_on_Continue() throws Throwable {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.Continue));
			log.info("waiting for Continue button to be clickable");
			// Thread.sleep(5000);
			// Actions action = new Actions(driver);
			action.doubleClick(order.Continue).build().perform();
//			jse.executeScript("arguments[0].click()", order.Continue);
			log.info("Continue button is clicked");
			// Thread.sleep(3000);
			// order.Continue.click();
		} catch (Exception e) {
			order.alt_Continue.click();
		}
		try {
			if (order.alt_Continue.isDisplayed()) {
				order.okay_alert.click();
			}
		} catch (Exception e) {
			System.out.println("##################### ----Continue Alert not displayed");
		}

	}

	@When("^On Menu Page click on a Six Pack$")
	public void on_Menu_Page_click_on_a_Six_Pack() throws Throwable {

		try {
			new WebDriverWait(driver, 30).until(webDriver -> ((JavascriptExecutor) webDriver)
					.executeScript("return document.readyState").equals("complete"));
			wait.until(ExpectedConditions.elementToBeClickable(order.sixpack));
			// Actions action = new Actions(driver);

			action.moveToElement(order.sixpack).build().perform();

			Thread.sleep(2000);
			order.sixpack.click();

		} catch (Exception e) {
			e.getMessage();
			wait.until(ExpectedConditions.elementToBeClickable(order.sixpack_pic));
			// Actions action = new Actions(driver);

			action.moveToElement(order.sixpack_pic).build().perform();
			Thread.sleep(2000);
			order.sixpack_pic.click();
		}
	}

	@When("^On Menu Page click on a Choco_Chunk$")
	public void on_Menu_Page_click_on_a_Delicious_Duo() throws Throwable {

		try {
			new WebDriverWait(driver, 30).until(webDriver -> ((JavascriptExecutor) webDriver)
					.executeScript("return document.readyState").equals("complete"));
			wait.until(ExpectedConditions.elementToBeClickable(order.chocolate_Chunk));
			// Actions action = new Actions(driver);

			action.moveToElement(order.chocolate_Chunk).build().perform();

			Thread.sleep(2000);
			order.chocolate_Chunk.click();

		} catch (Exception e) {
			e.getMessage();
			wait.until(ExpectedConditions.elementToBeClickable(order.chocolate_Chunk));
			// Actions action = new Actions(driver);

			action.moveToElement(order.chocolate_Chunk).build().perform();
			Thread.sleep(2000);
			order.chocolate_Chunk.click();
		}
	}

	@When("^user clicks on add product$")
	public void user_clicks_on_add_product() throws Throwable {

		try {
			/*
			 * new WebDriverWait(driver, 10).until( webDriver -> ((JavascriptExecutor)
			 * webDriver).executeScript("return document.readyState").equals( "complete"));
			 */
			wait.until(ExpectedConditions.elementToBeClickable(order.pick_for_me));
			order.pick_for_me.click();
			Thread.sleep(2000);
			if(order.pick_for_me.isDisplayed()) {
				order.pick_for_me.click();
			}
		} catch (Exception e) {
			e.printStackTrace();
			order.pick_for_me.click();
		}
	}

	@When("^user goes to the cart$")
	public void user_goes_to_the_cart() throws Throwable {

		try {
			/*
			 * new WebDriverWait(driver, 30).until( webDriver -> ((JavascriptExecutor)
			 * webDriver).executeScript("return document.readyState").equals( "complete"));
			 */

			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			order.cart_button.click();
			// Thread.sleep(2000);
		} catch (Exception e) {

			try {
				// JavascriptExecutor jse= (JavascriptExecutor)driver;
				jse.executeScript("return arguments[0].click()", order.new2_cart_button);
			} catch (Exception j) {
				wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
				order.alt_cart_button.click();
			}

		}
	}

	@And("^user clicks on login button present in the cart$")
	public void click_cart_login_button() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_login));
			order.cart_login.click();
		} catch (Exception e) {

			e.printStackTrace();
			order.delete_product(driver);
		}
	}

	@When("^Cookie Dough button should not appear to guest user in the cart$")
	public boolean cookie_dough() throws Throwable {
		try {
			Assert.assertEquals(0, order.cookie_dough_button.getSize());
			System.out.println(order.cookie_dough_button.getSize());

		} catch (Exception e) {
			e.printStackTrace();
			return true;
		} finally {

			order.delete_product(driver);
		}
		return false;
	}

	@And("^user clicks on cookie dough button$")
	public void add_cookie_dough() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.cookie_dough_button));
			order.cookie_dough_button.click();
		} catch (Exception e) {

			e.printStackTrace();
			// order.delete_product(driver);
		}
	}

	@And("^user clicks on remove cookie dough button$")
	public void remove_cookie_dough_button() throws Throwable {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.remove_cookie_dough));
			order.remove_cookie_dough.click();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Then("^Cookie Dough should be added in the cart$")
	public void verify_cookies_dough() throws Throwable {
		try {
			wait.until(ExpectedConditions.visibilityOf(order.cookie_dough));
			Assert.assertTrue(order.cookie_dough.isDisplayed());
			System.out.println("User is able to add Cookie dough");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Cookie dough button not present");
			order.delete_product(driver);

		} finally {

			order.delete_product(driver);

		}

	}

	@Then("^user should be able to remove cookie dough after applying it for Delivery Order$")
	public void remove_cookie_dough_after_applying_it_for_delivery_order() throws Throwable {

		try {
			Thread.sleep(5000);
			wait.until(ExpectedConditions.visibilityOf(order.cookie_doughApplied));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Cookie Dough removed success ########");

		} finally {

			order.delete_product(driver);
		}

	}

	@When("^user clicks on the checkout button$")
	public void user_clicks_on_the_checkout_button() throws Throwable {

		try {

			/*
			 * new WebDriverWait(driver, 30).until( webDriver -> ((JavascriptExecutor)
			 * webDriver).executeScript("return document.readyState").equals( "complete"));
			 */

			// Thread.sleep(10000);
			wait.until(ExpectedConditions.elementToBeClickable(order.checkout_button));
			jse.executeScript("arguments[0].click()", order.checkout_button);
//			order.checkout_button.click();
			new WebDriverWait(driver, 30).until(
					wd -> ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
		} catch (Exception e) {
			e.getMessage();
			order.checkout_button.click();
			new WebDriverWait(driver, 30).until(
					wd -> ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
			// driver.navigate().refresh();
		}

	}

	@When("^user enters Valid Details under Delivery Info & your Info$")
	public void user_enters_Valid_Details_under_Delivery_Info_your_Info(DataTable table) throws Throwable {

		Thread.sleep(10000);

		List<List<String>> data = table.raw();

		System.out.println(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.recipient_name));
		order.recipient_name.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.recipient_phone));
		order.recipient_phone.sendKeys(data.get(1).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_name));
		order.customer_name.sendKeys(data.get(2).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_phone));
		order.customer_phone.sendKeys(data.get(3).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_email));
		order.customer_email.sendKeys(data.get(4).get(1));

	}

	@And("^user enters Valid Details under Delivery Info & your Info along with new emailID$")
	public void add_new_emailID(DataTable table) throws Throwable {

		Thread.sleep(10000);
		List<List<String>> data = table.raw();
		System.out.println(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.recipient_name));
		order.recipient_name.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.recipient_phone));
		order.recipient_phone.sendKeys(data.get(1).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_name));
		order.customer_name.sendKeys(data.get(2).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_phone));
		order.customer_phone.sendKeys(data.get(3).get(1));
		String randomEmail = order.randomEmail();
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_email));
		order.customer_email.sendKeys(randomEmail);

	}

	@And("^user enters into your Info along with new emailID$")
	public void user_enters_into_your_Info_along_with_new_emailID(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_name));
		order.customer_name.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_phone));
		order.customer_phone.sendKeys(data.get(1).get(1));
		String randomEmail = order.randomEmail();
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_email));
		order.customer_email.sendKeys(randomEmail);
	}

	@When("^user enter delivery message$")
	public void user_enter_delivery_message(DataTable table) throws Throwable {

		Thread.sleep(10000);
		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.elementToBeClickable(order.instructions));
		order.instructions.sendKeys(data.get(0).get(1));
		// order.instructions.sendKeys(Keys.ENTER);

	}

	@When("^user selects Payment Method as Credit Card$")
	public void user_selects_Payment_Method_as_Credit_Card() throws Throwable {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.credit_card));
			order.credit_card.click();
		} catch (Exception e) {
			e.printStackTrace();
			order.credit_card.click();
		}
	}

	@And("^user clicks New Card$")
	public void user_Clicks_New_Card() throws InterruptedException {
		order.newCard.click();
		Thread.sleep(5000);
	}

	@When("^user enters credit card details$")
	public void user_enters_credit_card_details(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		jse.executeScript("arguments[0].scrollIntoView(true);", order.place_order);
//		jse.executeScript("window.scrollTo(0, 600)");

		try {

			if (order.add_new_credit_card_button.isDisplayed())

			{
				action.moveToElement(order.add_new_credit_card_button).build().perform();

				action.doubleClick(order.add_new_credit_card_button).perform();
				Thread.sleep(5000);
				driver.switchTo().frame(order.frame);
				wait.until(ExpectedConditions.elementToBeClickable(order.cc_num_new));
				order.cc_num_new.sendKeys(data.get(0).get(1));
//				wait.until(ExpectedConditions.elementToBeClickable(order.cvv_num));
//				order.cvv_num.sendKeys(data.get(2).get(1));
				Select year = new Select(order.cc_year);
				year.selectByIndex(1);

			}
		} catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(order.cc_num));
			order.cc_num.sendKeys(data.get(0).get(1));
			wait.until(ExpectedConditions.elementToBeClickable(order.cc_exp));
			order.cc_exp.sendKeys(data.get(1).get(1));
		}
	}

	@When("^user enters credit card details for logged in user to ship order$")
	public void enter_CC_Details_for_Ship_Order(DataTable table) {

		List<List<String>> data = table.raw();

		try {
			WebDriverWait waitLocal = new WebDriverWait(driver, 5);
			waitLocal.until(ExpectedConditions.visibilityOf(order.place_order));
			jse.executeScript("arguments[0].scrollIntoView(true);", order.place_order);
//		jse.executeScript("window.scrollTo(0, 600)");
		} catch (Exception e) {
			e.printStackTrace();
			driver.switchTo().frame(order.frame);
			jse.executeScript("arguments[0].scrollIntoView(true);", order.iframe_place_order_button);
		}
//		    driver.switchTo().frame(order.frame);
		wait.until(ExpectedConditions.elementToBeClickable(order.cc_num_new));
		order.cc_num_new.sendKeys(data.get(0).get(1));
		order.cc_exp_DDmnth.sendKeys(data.get(1).get(1));
		order.cc_exp_DDyr.sendKeys(data.get(2).get(1));

//			Select select = new Select(order.cc_exp_DDmnth);
//			select.selectByValue(data.get(1).get(1));
//			Select select1 = new Select(order.cc_exp_DDyr);
//			select1.selectByValue(data.get(2).get(1));

	}

	@When("^user clicks on Place Order$")
	public void user_clicks_on_Place_Order() throws Throwable {

		try {
			wait.until(ExpectedConditions.visibilityOf(order.place_order));
			if (order.place_order.isDisplayed()) {
				// wait.until(ExpectedConditions.elementToBeClickable(order.place_order));
				Thread.sleep(5000);
				// jse.executeScript("arguments[0].click()", order.place_order);
				order.place_order.click();
			}
		} catch (Exception e) {
			e.printStackTrace();
			driver.switchTo().defaultContent();

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");

			driver.switchTo().frame(order.frame);
			wait.until(ExpectedConditions.elementToBeClickable(order.iframe_place_order_button));
			order.iframe_place_order_button.click();
			driver.switchTo().defaultContent();

		}

		driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
	}

	@When("^user clicks on Place Order for Shipping order$")
	public void user_clicks_on_Place_Order_forShip_Order() throws Throwable {
		WebDriverWait waitlocal = new WebDriverWait(driver, 5);
		try {
			waitlocal.until(ExpectedConditions.visibilityOf(order.place_order));
			if (order.place_order.isDisplayed()) {
				waitlocal.until(ExpectedConditions.elementToBeClickable(order.place_order));
				Thread.sleep(5000);
				order.place_order.click();
			}
		} catch (Exception e) {

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");

			waitlocal.until(ExpectedConditions.elementToBeClickable(order.iframe_place_order_button));
			order.iframe_place_order_button.click();
			driver.switchTo().defaultContent();

		}

		driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
	}

	@And("^user selects tip$")
	public void user_selects_tip(DataTable table) throws Throwable {

		// try{
		List<List<String>> data = table.raw();
		// wait.until(ExpectedConditions.elementToBeSelected(order.tip));
		// Select tip_dropdown=new Select(order.tip);
		// tip_dropdown.selectByVisibleText(data.get(0).get(1));
		WebElement w = driver.findElement(By.xpath("//select[@id='payment-tip']"));
		// wait.until(ExpectedConditions.elementToBeSelected(w));
		Thread.sleep(5000);

		System.out.println(data.get(0).get(1));

		Select tip_dropdown = new Select(w);
		tip_dropdown.selectByValue(data.get(0).get(1));

		/*
		 * }catch(Exception e) { e.printStackTrace();
		 * wait.until(ExpectedConditions.elementToBeClickable(order.cart_button) );
		 * order.cart_button.click(); order.delete_product(driver); }
		 */
	}

	@And("validation popup appears saying tip updated")
	public void popup_appears_saying_tip_updated() throws Throwable {

		Thread.sleep(2000);
		Assert.assertEquals("Tip updated!", signup.popup.getText());
	}

	@Then("^Order confirmation page should appear displaying Order summary$")
	public void order_confirmation_page_should_appear_displaying_Order_summary() throws Throwable {
		WebDriverWait waitLocal = new WebDriverWait(driver, 50);
		// try{
		waitLocal.until(ExpectedConditions.visibilityOf(order.thanks));
		Assert.assertEquals("THANKS!", order.thanks.getText());
		System.out.println(order.thanks.getText());

		assertThat(order.tracking_no.getText(), CoreMatchers.containsString("#"));
		System.out.println(order.tracking_no.getText());
		assertThat(order.store_name.getText(), CoreMatchers.containsString("Insomnia Cookies -"));
		System.out.println(order.store_name.getText());
		// return true;
		/*
		 * } catch(Exception e) { e.printStackTrace();
		 * wait.until(ExpectedConditions.elementToBeClickable(order.cart_button) );
		 * order.cart_button.click(); order.delete_product(driver); }
		 */

		// return false;
	}

	@Then("^Order confirmation page should appear displaying tip in Order summary$")
	public void verify_tip(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.visibilityOf(order.tip_selected));

		Assert.assertEquals(data.get(0).get(1), order.tip_selected.getText());

	}

	@And("^On Menu Page click on The Insomniac$")
	public void on_Menu_Page_click_on_the_Insomniac() throws Throwable {
		try {
			new WebDriverWait(driver, 20).until(webDriver -> ((JavascriptExecutor) webDriver)
					.executeScript("return document.readyState").equals("complete"));

			// Actions action = new Actions(driver);

			action.moveToElement(order.the_insomniac).build().perform();
			Thread.sleep(5000);
			// order.the_insomniac.click();
			jse.executeScript("return arguments[0].click()", order.the_insomniac);

		} catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(order.insomniac_pic));
			// Actions action = new Actions(driver);

			action.moveToElement(order.insomniac_pic).build().perform();
			Thread.sleep(5000);
			// order.insomniac_pic.click();
			// order.insomniac_pic.click();
			action.doubleClick(order.insomniac_pic).perform();
		}
	}

	@And("^On Menu Page click The Sugar Rush$")
	public void on_Menu_Page_click_on_the_sugar_rush() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.sugar_rush));
			// Actions action = new Actions(driver);

			action.moveToElement(order.sugar_rush).build().perform();
			Thread.sleep(2000);
			order.sugar_rush.click();

		} catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(order.sugar_rush_pic));
			// Actions action = new Actions(driver);
			action.moveToElement(order.sugar_rush_pic).build().perform();
			Thread.sleep(2000);
			order.sugar_rush_pic.click();
		}
	}

	@And("^user selects Payment Method as cash$")
	public void payment_through_cash() throws Throwable {
		try {
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(order.cash));
			order.cash.click();
			// Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@And("^user selects Payment Method as School cash$")
	public void school_cash() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.school_cash));
			order.school_cash.click();
		} catch (Exception e) {
			e.printStackTrace();
			order.school_cash.click();
		}
	}

	@And("^user enters school cash number$")
	public void school_cash_details(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.elementToBeClickable(order.school_cash_details));
		order.school_cash_details.sendKeys(data.get(0).get(1));

	}

	@And("^user clicks on Pickup button$")
	public void pickup() throws Throwable {

		try {

			/*
			 * new WebDriverWait(driver, 30).until( webDriver -> ((JavascriptExecutor)
			 * webDriver).executeScript("return document.readyState").equals( "complete"));
			 */
			// order..click();
			Thread.sleep(2000);
			jse.executeScript("window.scrollBy(0,100)");
			wait.until(ExpectedConditions.elementToBeClickable(order.only_pickup_button));
			// Actions action = new Actions(driver);

			action.moveToElement(order.only_pickup_button).build().perform();
			Thread.sleep(5000);
			jse.executeScript("arguments[0].click()", order.only_pickup_button);

		} catch (Exception e) {

			try {

				action.moveToElement(order.pickup).build().perform();
				Thread.sleep(5000);
				order.pickup.click();
				// order.only_pickup_button.click();

			} catch (Exception r) {
				r.printStackTrace();
				action.moveToElement(order.alt_pickup).build().perform();
				Thread.sleep(5000);
				jse.executeScript("arguments[0].click()", order.alt_pickup);
				// order.alt_pickup.click();

				try {
					action.moveToElement(order.only_pickupBtn).build().perform();
					Thread.sleep(5000);
					jse.executeScript("arguments[0].click()", order.only_pickupBtn);
				} catch (Exception ex) {
					e.printStackTrace();
				}

			}

		}
		try {

			log.info("waiting for Continue button in the alert to be clickable");
			wait.until(ExpectedConditions.elementToBeClickable(order.continue_alert));
			action.moveToElement(order.continue_alert).build().perform();
			action.doubleClick(order.continue_alert).perform();
			log.info("Continue button is clicked");

		} catch (Exception e) {
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			// jse.executeScript("return arguments[0].click()",
			// order.alt_continue_alert);
			// Actions action = new Actions(driver);
			action.moveToElement(order.alt_continue_alert).build().perform();
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_continue_alert));
			// order.alt_continue_alert.click();
			action.doubleClick(order.alt_continue_alert).perform();
			// order.alt_continue_alert.click();
			log.info(" Delivery button in the alert is clicked using javascriptExecutor");
		}
	}

	@And("^user clicks on cart Pickup button$")
	public void click_cart_pickup_button() throws Throwable {

		order.cart_pickup_button.click();
		System.out.println("cart pickup button is clicked");
	}

	@And("^user clicks on Tracking ID link on Order confirmation page$")
	public void click_tracking_id() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.tracking_no));
			order.tracking_no.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@And("^user enters into your Info$")
	public void pickup_button(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_name));
		order.customer_name.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_phone));
		order.customer_phone.sendKeys(data.get(1).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_email));
		order.customer_email.sendKeys(data.get(2).get(1));

	}

	@And("^On Menu Page click on The Major Rager$")
	public void major_rager() throws Throwable {

		try {
			new WebDriverWait(driver, 30).until(webDriver -> ((JavascriptExecutor) webDriver)
					.executeScript("return document.readyState").equals("complete"));
			wait.until(ExpectedConditions.elementToBeClickable(order.major_rager));
			// Actions action = new Actions(driver);

			action.moveToElement(order.major_rager).build().perform();

			Thread.sleep(5000);
			order.major_rager.click();

		} catch (Exception e) {

			try {
				wait.until(ExpectedConditions.elementToBeClickable(order.major_rager_pic));
				// Actions action = new Actions(driver);

				action.moveToElement(order.major_rager_pic).build().perform();
				Thread.sleep(3000);
				order.major_rager_pic.click();
			} catch (Exception j) {

				// Actions action = new Actions(driver);

				action.moveToElement(order.alt_major_rager_pic).build().perform();
				Thread.sleep(3000);
				order.alt_major_rager_pic.click();

			}
		}
	}

	@And("On Menu Page click on The Choco Chunk")
	public void choco_chunk() throws Throwable {
		try {
			Thread.sleep(4000);
			// driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollTo(0, 1200)");
			wait.until(ExpectedConditions.elementToBeClickable(order.choco_chunk));
			// Actions action = new Actions(driver);

			action.moveToElement(order.choco_chunk).build().perform();

			Thread.sleep(3000);
			order.choco_chunk.click();
			System.out.println("Choco-chunk cookie is added to the cart");

		} catch (Exception e) {
			e.getMessage();
			/*
			 * JavascriptExecutor jse = (JavascriptExecutor)driver;
			 * jse.executeScript("window.scrollBy(0,2000)", "");
			 * jse.executeScript("return arguments[0].click();", order.choco_chunk);
			 * System.out.println("Choco-chunk cookie is added to the cart");
			 */ // wait.until(ExpectedConditions.elementToBeClickable(order.choco_chunk));

			// try{
			// Actions action = new Actions(driver);
			action.moveToElement(order.alt_choco_chunk).build().perform();
			Thread.sleep(5000);
			order.alt_choco_chunk.click();
			System.out.println("Choco-chunk cookie is added to the cart");
		} /*
			 * catch(Exception j){
			 * 
			 * e.getMessage(); //JavascriptExecutor jse2 = (JavascriptExecutor)driver;
			 * //jse.executeScript("window.scrollBy(0,2000)", "");
			 * jse.executeScript("return arguments[0].click();", order.alt_choco_chunk);
			 * System.out.println("Choco-chunk cookie is added to the cart"); }
			 */
		// }

	}

	@And("^user selects Payment Method as Gift card$")
	public void gift_card() throws Throwable {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.gift_card));
			order.gift_card.click();
		} catch (Exception e) {
			e.getMessage();
			order.gift_card.click();
		}
	}

	@Then("^Tracking ID should be displayed along with status$")
	public void verify_tracking_ID() throws Throwable {
		// Assert.assertThat(order.tracker_no_verify.getAttribute("value"),
		// isEmptyString());

		wait.until(ExpectedConditions.visibilityOf(order.tracker_no_verify));
		if (!order.tracker_no_verify.getAttribute("value").isEmpty()) {

			System.out.println("Test Case Passed");

		}
	}
	
	@Then("^Tracking ID should be displayed along with status of order$")
	public void verify_tracking_ID_MyAccount() throws Throwable {
		// Assert.assertThat(order.tracker_no_verify.getAttribute("value"),
		// isEmptyString());

		Set<String> st = driver.getWindowHandles();
		Iterator<String> it = st.iterator();
		String parent = it.next();
		String child = it.next();

		// swtich to parent
		driver.switchTo().window(parent);
		System.out.println("Returned to parent");
		Thread.sleep(2000);
		// switch to child
		driver.switchTo().window(child);

		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOf(order.tracker_no_verify));
		if (!order.tracker_no_verify.getAttribute("value").isEmpty()) {

			System.out.println("Test Case Passed");

		}
		// swtich to parent
		Thread.sleep(2000);
		driver.switchTo().window(parent);
		System.out.println("Returned to parent");
	}

	@Then("^Tracking ID should navigate to tracking page$")
	public void verify_tracking_page() throws Throwable {
		// wait.until(ExpectedConditions.visibilityOf(order.tracker_no_verify));
		/*
		 * if () {
		 * 
		 * System.out.println("Test Case Passed");
		 * 
		 * }
		 */

		Thread.sleep(3000);
		Assert.assertTrue(!order.tracker_no_verify.getAttribute("value").isEmpty());
		System.out.println("Tracking ID navigates to tracking page and Tracking ID is dispayed");

		String oldTab = driver.getWindowHandle();
		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		Thread.sleep(5000);
		System.out.println(newTab);
		newTab.remove(newTab);
		driver.switchTo().window(oldTab);

	}

	@And("^user enters Gift card number$")
	public void gift_card_details(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.elementToBeClickable(order.gift_card_no));
		order.gift_card_no.sendKeys(data.get(0).get(1));

	}

	@Then("^Cart should state, You could be earning Five points for this order$")
	public void cart_should_state_You_could_be_earning_points_for_this_order() throws Throwable {
		try {
			assertThat(order.loyalty_points.getText(),
					CoreMatchers.containsString("You could be earning 5 points for this order"));
			System.out.println(order.loyalty_points.getText());

		} catch (Exception e) {
			e.getMessage();

		} finally {

			order.delete_product(driver);
		}

	}

	@Then("^Cart should state, You will earn One points for this order$")
	public void cart_should_state_You_will_earn_points_for_this_order() throws Throwable {
		try {
			assertThat(order.loyalty_points.getText(),
					CoreMatchers.containsString("You will earn 1 points from this order"));
			System.out.println(order.loyalty_points.getText());

		} catch (Exception e) {
			e.getMessage();

		} finally {

			order.delete_product(driver);
		}

	}

	// You're not logged in! You could be earning 4 points for this order.

	@Then("^Cart should state, You are not logged in! You could be earning One points for this order$")
	public void cart_should_state_You_could_be_earning_one_points_for_this_order() throws Throwable {
		try {
			assertThat(order.loyalty_points.getText(),
					CoreMatchers.containsString("You're not logged in! You could be earning 1 points for this order."));
			System.out.println(order.loyalty_points.getText());

		} catch (Exception e) {
			e.getMessage();

		} finally {

			order.delete_product(driver);
		}

	}

	@Then("^Cart should state, You are not logged in! You could be earning Thirty Two points for this order$")
	public void cart_should_state_You_could_be_earning_twenty_nine_points_for_this_order() throws Throwable {
		try {
			assertThat(order.loyalty_points.getText(), CoreMatchers
					.containsString("You're not logged in! You could be earning 32 points for this order."));
			System.out.println(order.loyalty_points.getText());

		} catch (Exception e) {
			e.getMessage();

		} finally {

			order.delete_product(driver);
		}

	}

	@Then("^Cart should state, You could be earning Seventy one points for this order$")
	public void cart_should_state_You_could_be_earning_Eighty_points_for_this_order() throws Throwable {
		try {
			assertThat(order.loyalty_points.getText(),
					CoreMatchers.containsString("You could be earning 71 points for this order"));
			System.out.println(order.loyalty_points.getText());

		} catch (Exception e) {
			e.getMessage();

		} finally {

			order.delete_product(driver);
		}

	}

	@Then("^Cart should state You will earn Thirty Two points for this order$")
	public void cart_should_state_You_will_earn_Twenty_Nine_points_for_this_order() throws Throwable {
		try {

			assertThat(order.loyalty_points.getText(),
					CoreMatchers.containsString("You will earn 32 points from this order"));
			System.out.println(order.loyalty_points.getText());

		} catch (Exception e) {
			e.getMessage();

		} finally {

			order.delete_product(driver);
		}

	}

	@And("^user enters Coupon$")
	public void enter_coupon(DataTable table) throws Throwable {

		try {
			List<List<String>> data = table.raw();
			wait.until(ExpectedConditions.elementToBeClickable(order.coupon_box));
			// Actions action = new Actions(driver);

			action.moveToElement(order.coupon_box).build().perform();

			Thread.sleep(2000);
			order.coupon_box.click();

			order.coupon_box.sendKeys(data.get(0).get(1));
			Thread.sleep(5000);

			System.out.println("Entered coupon code is :" + order.coupon_box.getAttribute("value"));

		} catch (Exception e) {
			e.getMessage();

		}

	}

	/*
	 * @And("^user enters Coupon \"(.*)\"$") public void enter_coupon(String
	 * coupon_code) throws Throwable {
	 * 
	 * wait.until(ExpectedConditions.elementToBeClickable(order.coupon_box));
	 * order.coupon_box.sendKeys(coupon_code); }
	 */

	@And("^user clicks Apply button$")
	public void click_apply_button() throws Throwable {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.coupon_box));
			if (order.coupon_box.getText() != null) {
				wait.until(ExpectedConditions.elementToBeClickable(order.apply));
				order.apply.click();
				Thread.sleep(3000);
			} else {

			}

		} catch (Exception e) {

			e.getMessage();
			order.apply.click();

		}
	}

	@And("^user adds free cookies$")
	public void add_free_cookies() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.free_cookies));
		order.free_cookies.click();
		Thread.sleep(2000);
		order.free_cookies.click();
		Thread.sleep(2000);
//		order.free_cookies.click();
//		Thread.sleep(2000);
//		order.free_cookies.click();
//		Thread.sleep(2000);
//		order.free_cookies.click();
//		Thread.sleep(2000);
//		order.free_cookies.click();
//		Thread.sleep(2000);
	}

	@And("^Free cookies should be added to the cart$")
	public void verify_free_cookies() throws Throwable {

		try {
			Thread.sleep(3000);
			assertThat(order.added_product_coupon.getText(), CoreMatchers.containsString("2x Chocolate Chunk"));
			System.out.println(order.added_product_coupon.getText());
		} catch (Exception e) {
			e.printStackTrace();
			order.delete_product(driver);
		} finally {

			order.remove_coupon_button.click();
			System.out.println("Coupon removed");

//			order.delete_product(driver);
		}

	}

	@And("^user clicks Add free item button$")
	public void add_free_items() throws Throwable {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.add_free_items));
			order.add_free_items.click();
			// order.add_free_items.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Then("^Error popup stating You must be logged in to use this coupon should appear$")
	public void verify_popup() throws Throwable {

		try {
//			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOf(signup.popup));
			Assert.assertEquals("You must be logged in to use this coupon", signup.popup.getText());
			System.out.println(signup.popup.getText());

		} finally {

			order.delete_product(driver);

		}
	}

	@And("^user selects date from calender$")
	public void select_date() throws Throwable {

		try {
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(order.datepicker));
			order.datepicker.click();
		} catch (Exception e) {
			e.getMessage();
			wait.until(ExpectedConditions.elementToBeClickable(order.datepicker));
			order.datepicker.click();
		}

		/*
		 * SimpleDateFormat df = new SimpleDateFormat("MM/dd/YYYY"); Date dt = new
		 * Date(); Calendar cl = Calendar.getInstance(); cl.setTime(dt);
		 * cl.add(Calendar.DAY_OF_YEAR, -1); dt=cl.getTime(); String str =
		 * df.format(dt); System.out.println("the date today is " + str);
		 */

	}

	@Then("^user should not be able to select back date$")
	public boolean verify_back_date_selection() throws Throwable {
		try {
			List<WebElement> allDates = driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//td"));
			System.out.println(order.current_date.getText());
			String cur_date = order.current_date.getText();
			int res = Integer.parseInt(cur_date);
			System.out.println(res);

			for (int i = res; i < (res + 1); i++) {

				System.out.println(driver.findElement(By.xpath("//*[text()=" + (res - 1) + "]")).isSelected());

				if (!(driver.findElement(By.xpath("//*[text()=" + (res - 1) + "]")).isSelected())) {
					System.out.println("Unable to select back date");
					return true;
				}
			}

		} catch (Exception e)

		{
			e.getMessage();
			System.out.println("Previous date is not selectable");
			return true;
		}
		return false;
	}

	// @SuppressWarnings("unused")
	@And("^user selects date from calender beyond six months but is unable to do so$")
	public boolean select_date_beyond_six_month() throws Throwable {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.datepicker));
			order.datepicker.click();
			// List<WebElement> allDates =
			// driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//td"));
			System.out.println(order.current_date.getText());
			String str = order.current_date.getText();
			wait.until(ExpectedConditions.elementToBeClickable(order.forward_arrow));
			order.forward_arrow.click();
			Thread.sleep(2000);
			order.forward_arrow.click();
			Thread.sleep(2000);
			order.forward_arrow.click();
			Thread.sleep(2000);
			order.forward_arrow.click();
			Thread.sleep(2000);
			order.forward_arrow.click();
			Thread.sleep(2000);
			order.forward_arrow.click();
			Thread.sleep(2000);
			order.forward_arrow.click();
			Thread.sleep(2000);
			order.forward_arrow.click();

			List<WebElement> allDates = driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//td"));
			Thread.sleep(3000);
			int size = allDates.size();
			System.out.println(size);
			// String str = order.current_date.getText();
			int res = Integer.parseInt(str);
			System.out.println(res);
			for (int i = res; i < (res + 1); i++) {

				System.out.println(driver.findElement(By.xpath("//*[text()=" + (res + 1) + "]")).isSelected());

				if (!(driver.findElement(By.xpath("//*[text()=" + (res + 1) + "]")).isSelected())) {
					System.out.println("Unable to select date beyond 6 months");
					return true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		System.out.println("Date beyond 6 month is selectable");
		return false;
	}

	@Then("^user should not be able to select date beyond six months$")
	public boolean verify_date_selection() throws Throwable {

		// try{
		List<WebElement> allDates = driver.findElements(By.xpath("//table[@class='ui-datepicker-calendar']//td"));
		Thread.sleep(3000);
		int size = allDates.size();
		System.out.println(size);
		String str = order.current_date.getText();

		for (WebElement el : allDates) {
			System.out.println(el.getText());
			int res1 = Integer.parseInt(str);
			int res = Integer.parseInt(el.getText());
			if (res > res1)

			{
				if (el.isEnabled()) {

					System.out.println("date selection beyond six month is enabled");
					break;
				}
			} else {
				System.out.println("CLICKING Day at current day Date is selectable");
				continue;
			}
		}

		return false;

	}

	@And("^user deleted the product$")
	public void delete_product() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.delete_product_button));
		order.delete_product(driver);
	}

	@Then("^product should be present$")
	public boolean verify_product() throws Throwable {

		try {
			wait.until(ExpectedConditions.visibilityOf(order.product_ordered));
			System.out.println(order.product_ordered.getText());
			assertThat(order.product_ordered.getText(), CoreMatchers.containsString("1x Major Rager (18 Traditional)"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			order.delete_product(driver);

		}
		return false;
	}

	@And("^user goes to the cart again$")
	public void go_to_cart_again() throws Throwable {
		try {

			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button_Stage18));
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("return arguments[0].click()", order.alt_cart_button_Stage18);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Then("^product added after clicking add more items button should be present in the cart$")
	public void add_more_items_button() throws Throwable {

		try {

			wait.until(ExpectedConditions.visibilityOf(order.product_added_through_add_more_items_button));
			System.out.println(order.product_added_through_add_more_items_button.getText());
			assertThat(order.product_added_through_add_more_items_button.getText(),
					CoreMatchers.containsString("Major Rager"));
			Thread.sleep(10000);

		} finally {
			order.delete_product(driver);
			order.delete_product(driver);
		}
	}

	@Then("^upsell should be displayed$")
	public boolean verify_upsell() throws Throwable {

		try {
			wait.until(ExpectedConditions.visibilityOf(order.upsell));
			if (order.upsell.isDisplayed()) {
				System.out.println("Upsell is displayed");

				return true;

			}
		} catch (Exception e) {

			e.printStackTrace();
		} finally {

			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);

		}

		return false;
	}

	@And("^user clicks on  close button$")
	public void click_close() throws Throwable {

		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.product_close));
			order.product_close.click();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Then("^product menu should close$")
	public void close_menu() throws Throwable {

		wait.until(ExpectedConditions.invisibilityOf(order.product_close));

		Assert.assertFalse(order.product_close.isDisplayed());
		System.out.println("User is able to close product menu");

	}

	@Then("^User should get a pop up stating that Delivery instructions saved$")
	public void Delivery_instructions_saved_popup() throws Throwable {

		try {
			wait.until(ExpectedConditions.visibilityOf(signup.popup));
			Assert.assertEquals("Delivery instructions saved", signup.popup.getText());
			System.out.println(signup.popup.getText());

		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);

		}
	}

	@And("^user enters message for recepient$")
	public void message_for_recepient(DataTable table) throws Throwable {

		/*
		 * new WebDriverWait(driver, 30).until( webDriver -> ((JavascriptExecutor)
		 * webDriver).executeScript("return document.readyState").equals( "complete"));
		 */
		Thread.sleep(10000);
		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.elementToBeClickable(order.message));
		order.message.sendKeys(data.get(0).get(1));

	}

	@Then("^User should get a pop up stating that Message successfully added$")
	public void Message_added_verification() throws Throwable {

		try {
			Thread.sleep(5000);
			Assert.assertEquals("Message successfully added", signup.popup.getText());
			System.out.println(signup.popup.getText());

		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);

		}
	}

	@Then("^User should get a pop up stating Product Name added to cart$")
	public void product_name() throws Throwable {
		try {
			wait.until(ExpectedConditions.visibilityOf(signup.popup));
			Assert.assertEquals("Major Rager (18 Traditional) added to cart!", signup.popup.getText());
			System.out.println(signup.popup.getText());

		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);

		}
	}

	@When("^user clicks on Delivery time & date displayed on the cart top$")
	public void user_clicks_on_Delivery_time_date_displayed_on_the_cart_top() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.edit_delivery));
			order.edit_delivery.click();
		} catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			order.cart_button.click();
			order.delete_product(driver);
		}
	}

	@And("^user clicks on Pickup time & date displayed on the cart top$")
	public void user_clicks_on_Pickup_time_date_displayed_on_the_cart_top() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.edit_delivery));
			order.edit_delivery.click();
		} catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			order.cart_button.click();
			order.delete_product(driver);
		}
	}

	@When("^user changes date$")
	public void user_changes_date() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_delivery_datepicker));
			order.cart_delivery_datepicker.click();
			String str = order.current_date.getText();
			int res = Integer.parseInt(str);
			System.out.println("res=" + res);

			WebElement el = driver.findElement(By.xpath("//a[contains(text()," + (res + 1) + ")]"));
			System.out.println("el=" + el.getText());
			el.click();
			System.out.println(driver.findElement(By.xpath("//a[contains(text()," + (res + 1) + ")]")).isSelected());

		} catch (Exception e) {
			e.printStackTrace();
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			order.cart_button.click();
			order.delete_product(driver);
		}
	}

	@When("^user changes time$")
	public void user_changes_time() throws Throwable {
		try {

			// order.cart_delivery_timepicker.click();
			Thread.sleep(10000);
			// WebElement w = driver.findElement(By.xpath(
			// "/html[1]/body[1]/div[2]/section[1]/section[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/div[1]/select[1]"));
			// wait.until(ExpectedConditions.elementToBeSelected(w));
			// Thread.sleep(5000);
			Select time_dropdown = new Select(order.cart_delivery_timepicker);
			time_dropdown.selectByIndex(8);

			Thread.sleep(3000);

		} catch (Exception e) {
			e.printStackTrace();
			/*
			 * wait.until(ExpectedConditions.elementToBeClickable(order. cart_button));
			 * order.cart_button.click(); order.delete_product(driver);
			 */
		}
	}

	@When("^user clicks Update button$")
	public void user_clicks_Update_button() throws Throwable {
		Thread.sleep(1500);
		wait.until(ExpectedConditions.elementToBeClickable(order.cart_update_button));
		order.cart_update_button.click();
	}

	@Then("^updated date & time should be displayed on the cart top$")
	public void updated_date_time_should_be_displayed_on_the_cart_top() throws Throwable {

		try {
			String str = order.edit_delivery_text.getText();
			order.edit_delivery.click();

			System.out.println("################ String Captured is" + str);
			System.out.println(str.length());
			System.out.println(str.substring(22, 28));
			System.out.println(str.substring(11, 19));
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			String date = (String) jse.executeScript("return arguments[0].value", order.cart_delivery_datepicker);
			System.out.println(date);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String newDateStr = simpleDateFormat.format(simpleDateFormat.parse(str.substring(20, 28)));
			String str_new[] = newDateStr.split("/");
			String day = str_new[1];
			System.out.println(day);

			// assertEquals(str.substring(25, 27),date.substring(8,10));
			assertThat(day, CoreMatchers.containsString(date.substring(8, 10)));

			String time = (String) jse.executeScript("return arguments[0].value", order.cart_delivery_timepicker);
			System.out.println(time);

			assertThat(str.substring(12, 20), CoreMatchers.containsString(time));
		} finally {

			order.delete_product(driver);
		}
	}

	@Then("Delivery should be displayed on the cart top")
	public void delivery_should_not_be_displayed() throws Throwable {

		try {
			Thread.sleep(2000);
			Assert.assertTrue(order.edit_delivery_text.getText().contains("Delivery"));

			System.out.println(order.edit_delivery_text.getText());
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("Pickup should be displayed on the cart top")
	public void pickup_should_not_be_displayed() throws Throwable {

		try {
			Thread.sleep(2000);
			Assert.assertTrue(order.edit_delivery_text.getText().contains("Pickup"));

			System.out.println(order.edit_delivery_text.getText());
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^updated pickup date & time should be displayed on the cart top$")
	public void updated_pickup_date_time_should_be_displayed_on_the_cart_top() throws Throwable {

		try {
			String str = order.edit_delivery_text.getText();
			order.edit_delivery.click();

			System.out.println("################ String Captured is" + str + str.length());
			System.out.println(str.substring(22, 28));
			System.out.println(str.substring(10, 17));
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			String date = (String) jse.executeScript("return arguments[0].value", order.cart_delivery_datepicker);
			System.out.println(date);

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mm/yy");
			String newDateStr = simpleDateFormat.format(simpleDateFormat.parse(str.substring(20, 28)));
			String str_new[] = newDateStr.split("/");
			String day = str_new[1];
			System.out.println(day);

			// assertEquals(str.substring(25, 27),date.substring(8,10));
			assertThat(day, CoreMatchers.containsString(date.substring(8, 10)));

			// assertEquals(str.substring(23, 25),date.substring(8,10));
			String time = (String) jse.executeScript("return arguments[0].value", order.cart_delivery_timepicker);
			System.out.println(time);

			assertThat(str.substring(10, 18), CoreMatchers.containsString(time));
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^user should be able to see Add more items button on Checkout page$")
	public void user_should_be_able_to_see_Add_more_items_button_on_Checkout_page() throws Throwable {

		try {
			assertThat(order.add_more_button.getText(), CoreMatchers.containsString("Add more items"));
		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);
		}
	}

	@Then("^user should redirect to Menu page$")
	public void user_should_redirect_to_Menu_page() throws Throwable {
		try {
			wait.until(ExpectedConditions.visibilityOf(order.menu_page));
			assertThat(order.menu_page.getText(), CoreMatchers.containsString("DEALS"));
			System.out.println(order.menu_page.getText());
			System.out.println("user redirects to Menu page");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));

			order.alt_cart_button.click();
			System.out.println("cart button is clicked");
			order.delete_product(driver);
		}

	}

	@And("^user clicks on Add more items button on Checkout page$")
	public void user_clicks_on_Add_more_items_button_on_Checkout_page() throws Throwable {
		/*
		 * try{
		 * 
		 * Thread.sleep(10000);
		 * wait.until(ExpectedConditions.visibilityOf(order.add_more_button)); Actions
		 * action = new Actions(driver);
		 * 
		 * action.moveToElement(order.add_more_button).build().perform();
		 * 
		 * Thread.sleep(3000);
		 * 
		 * order.add_more_button.click(); order.add_more_button.click();
		 * 
		 * 
		 * }catch(Exception e) {
		 */
		try {
			Thread.sleep(10000);
			wait.until(ExpectedConditions.visibilityOf(order.alt_add_more_button));
			// Actions action = new Actions(driver);

			action.moveToElement(order.alt_add_more_button).build().perform();

			Thread.sleep(3000);

			order.alt_add_more_button.click();
			// order.alt_add_more_button.click();
			System.out.println("Add more items button is clicked");
			log.info("Add more items button is clicked");

		} catch (Exception j) {

			j.printStackTrace();
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			order.cart_button.click();
			order.delete_product(driver);
		}

		// }

	}

	@And("^user clicks radio button saying Register after Checkout$")
	public void click_radio_button() throws Throwable {
		try {
			// wait.until(ExpectedConditions.elementToBeClickable(order.register));
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("return arguments[0].click()", order.register);
		} catch (Exception e) {
			e.printStackTrace();
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			order.cart_button.click();
			order.delete_product(driver);
		}
	}

	@And("^user clicks radio button saying Already registered$")
	public void select_already_registered() throws Throwable {

		try {
			/*
			 * driver.navigate().refresh(); new WebDriverWait(driver, 30).until( webDriver
			 * -> ((JavascriptExecutor)
			 * webDriver).executeScript("return document.readyState").equals( "complete"));
			 */

			Thread.sleep(10000);

			wait.until(ExpectedConditions.elementToBeClickable(order.already_registered));
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			order.already_registered.click();
//			jse.executeScript("return arguments[0].click()", order.already_registered);
			System.out.println("Checkbox -Already registered is clicked");
			/*
			 * Thread.sleep(3000); jse.executeScript("return arguments[0].click()",
			 * order.register);
			 * System.out.println("Checkbox -register after checkout is clicked" );
			 * Thread.sleep(3000); jse.executeScript("return arguments[0].click()",
			 * order.already_registered);
			 * System.out.println("Checkbox -Already registered is clicked");
			 */
		} catch (Exception r) {
			try {
				// JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("return arguments[0].click()", order.alt_already_registered);
			} catch (Exception e) {
				e.printStackTrace();
				wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
				order.cart_button.click();
				order.delete_product(driver);
			}
		}

	}

	@And("^user enters emailID$")
	public void user_enters_emailID(DataTable table) throws Throwable {

		try {
			List<List<String>> data = table.raw();
			wait.until(ExpectedConditions.elementToBeClickable(order.checkout_login_email));
			order.checkout_login_email.sendKeys(data.get(0).get(1));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@When("^user enters new emailID$")
	public void user_enters_new_emailID() throws Throwable {

		String randomEmail = order.randomEmail();
		wait.until(ExpectedConditions.elementToBeClickable(signup.emailid));
		signup.emailid.sendKeys(randomEmail);
	}

	@And("^user enters password$")
	public void user_enters_password(DataTable table) throws Throwable {
		try {
			List<List<String>> data = table.raw();
			wait.until(ExpectedConditions.elementToBeClickable(order.checkout_login_password));
			order.checkout_login_password.sendKeys(data.get(0).get(1));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@And("^user clicks login In button$")
	public void user_clicks_login_In_button() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.checkout_login_btn));
			order.checkout_login_btn.click();
			order.checkout_login_btn.click();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@And("^user enters password & confirm password$")
	public void password_confirm_password(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();

		order.register_new_password.sendKeys(data.get(0).get(1));
		Thread.sleep(2000);
		order.register_new_password_confirm.sendKeys(data.get(1).get(1));

	}

	@Then("^User must get logged in successfully$")
	public void User_must_get_logged_in_successfully() throws Throwable {

		try {
			String str = new String();
			str = s.login_text.getText();
			System.out.println(str);
			if (str.contains("Hi")) {
				System.out.println("Test Case Passed");
			} else {
				System.out.println("Test Case Failed");
			}
		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(s.login_text));
			s.login_text.click();
			wait.until(ExpectedConditions.elementToBeClickable(s.logout));
			s.logout.click();

		}
	}

	@Then("^radio options - Continue As Guest,Register After Checkout,Already Registered should disappear$")
	public boolean radio_options_Continue_As_Guest_Register_After_Checkout_Already_Registered_should_disappear()
			throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.cash));

			Assert.assertTrue(!(order.register.isDisplayed()));

			/*
			 * Thread.sleep(5000);
			 * Assert.assertTrue(!(order.continue_as_guest.isDisplayed())); System.out.
			 * println("Radio button - Continue as guest is not displayed");
			 */

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Radio button - Register after checkout is not displayed");
			try {
				Assert.assertFalse(order.already_registered.isDisplayed());

			} catch (Exception j) {
				j.printStackTrace();
				System.out.println("Radio button - Already registered is not displayed");
				return true;
			}
		}
//		finally {
//			wait.until(ExpectedConditions.elementToBeClickable(s.login_text));
//			s.login_text.click();
//			wait.until(ExpectedConditions.elementToBeClickable(s.logout));
//			s.logout.click();
//
//		}
		return false;
	}

	@And("user navigates back")
	public void user_navigates_back() throws Throwable {

		driver.navigate().back();
	}

	@Then("^The button in the cart says Checkout$")
	public void cart_says_Checkout() throws Throwable {
		try {
			String str = order.checkout_button.getText();
			Assert.assertEquals("Checkout", str);
		} finally {
			order.delete_product(driver);
		}
	}

	@And("^user clicks on Location tab$")
	public void user_clicks_on_Location_tab() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.location_tab));
		order.location_tab.click();
		try {
			Thread.sleep(5000);
			if (order.order_button.isDisplayed()) {

				System.out.println("Cart is empty");
				log.info("Cart is empty");
			}
		} catch (Exception j) {
			j.printStackTrace();
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button_Stage18));
			// log.info("waiting for cart button to appear");
			log.debug("waiting for cart button to appear");

			order.alt_cart_button_Stage18.click();
			// log.info("cart button is clicked");
			log.debug("cart button is clicked");
			order.delete_product(driver);
			// log.info("delete product from the cart");
			log.debug("delete product from the cart");
		}
		Thread.sleep(2000);
	}

	@And("^user clicks on Locations tab$")
	public void user_clicks_on_Locations_tab() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.location_tab));
		order.location_tab.click();
//		try {
//			Thread.sleep(5000);
//			if (order.order_button.isDisplayed()) {
//
//				System.out.println("Cart is empty");
//				log.info("Cart is empty");
//			}
//		} catch (Exception j) {
//			j.printStackTrace();
//			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button_Stage18));
//			// log.info("waiting for cart button to appear");
//			log.debug("waiting for cart button to appear");
//
//			order.alt_cart_button_Stage18.click();
//			// log.info("cart button is clicked");
//			log.debug("cart button is clicked");
//			order.delete_product(driver);
//			// log.info("delete product from the cart");
//			log.debug("delete product from the cart");
//		}
		Thread.sleep(2000);
	}

	@Then("^Stores not yet launched should displayed message \"(.*?)\"$")
	public void stores_not_yet_launched_should_displayed_message(String arg1) throws Throwable {

		wait.until(ExpectedConditions.visibilityOf(order.coming_soon));
		System.out.println(order.coming_soon.getText());
		Assert.assertEquals(arg1, order.coming_soon.getText());
	}

	@When("^user clicks on Request Notification$")
	public void user_clicks_on_Request_Notification() throws Throwable {
		WebDriverWait localWait = new WebDriverWait(driver, 8);
		try {
			localWait.until(ExpectedConditions.elementToBeClickable(order.request_notification));
			// Thread.sleep(5000);
			order.request_notification.click();
		} catch (Exception e) {
			// JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("return arguments[0].click()", order.alt_request_notification);
		}
	}

	@When("^user clicks submit$")
	public void user_clicks_submit() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(order.submit_notification));
		order.submit_notification.click();
	}

	@And("^user enters emailID to receive notification$")
	public void enters_emailID_to_receive_notification(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.visibilityOf(order.emailID_for_notification));
		order.emailID_for_notification.sendKeys(data.get(0).get(1));

	}

	@Then("^popup should appear saying \"(.*?)\"$")
	public void popup_should_appear_saying(String arg1) throws Throwable {

		Thread.sleep(2000);
		Assert.assertEquals(arg1, signup.popup.getText());
	}

	@Then("^popup should appear saying Please enter a valid email in the text box$")
	public void enter_a_valid_email() throws Throwable {

		Thread.sleep(2000);
		Assert.assertEquals("Please enter a valid email in the text box.", signup.popup.getText());
	}

	@Then("^popup should appear saying Please enter a phone number in a standard format with the 3-digit area code such as xxx-xxx-xxxx$")
	public void phone_length() throws Throwable {
		Thread.sleep(2000);
		Assert.assertEquals(
				"Please enter a phone number in a standard format with the 3-digit area code such as xxx-xxx-xxxx",
				signup.popup.getText());

	}

	@When("^user selects radio button : phone to request notification$")
	public void user_selects_radio_button_phone_to_request_notification() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.phone_notification));
			order.phone_notification.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@And("^user enters phone number$")
	public void user_enters_phone_number(DataTable arg1) throws Throwable {

		try {
			List<List<String>> data = arg1.raw();
			wait.until(ExpectedConditions.visibilityOf(order.emailID_for_notification));
			order.emailID_for_notification.sendKeys(data.get(0).get(1));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Then("user should able to see search box option in cart for delivery")
	public void search_cart() throws Throwable {
		try {
			Assert.assertTrue(order.address_cart.isDisplayed());
			System.out.println("Address search box in the cart is displayed for Delivery order");
		} finally {
			order.delete_product(driver);
		}
	}

	@And("^user goes to address search box in the cart$")
	public void click_address_search_box() throws Throwable {

		order.address_cart.click();
	}

	@And("^user enters the address in the cart$")
	public void enter_address_in_the_cart(DataTable table) throws Throwable {
//		try {
			List<List<String>> data = table.raw();
			System.out.println(data.get(0).get(1));
			wait.until(ExpectedConditions.elementToBeClickable(order.address_cart));
			order.address_cart.sendKeys(data.get(0).get(1));

			// Thread.sleep(5000);
			// wait.until(ExpectedConditions.visibilityOf(order.address_suggestion_cart));

			wait.until(ExpectedConditions.elementToBeClickable(order.address_suggestion_cart));
			System.out.println("address suggestion is displayed");
			order.address_suggestion_cart.click();
			System.out.println("address suggestion is clicked");
//		} catch (Exception j) {
//			order.address_cart.sendKeys(Keys.ENTER);
//
//		}

	}

	@Then("^popup should appear saying - Address updated$")
	public void popup_verification() throws Throwable {
		try {
			Thread.sleep(3000);
			Assert.assertEquals("Address updated", signup.popup.getText());
			System.out.println("Popup message displayed : " + signup.popup.getText());
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^popup should appear stating - Order is scheduled for pickup$")
	public void popup_verification_from_pickup_button_under_cart() throws Throwable {
		try {
			Thread.sleep(3000);
			Assert.assertEquals("Order is scheduled for pickup", signup.popup.getText());
			System.out.println("Popup message displayed : " + signup.popup.getText());
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^popup should appear stating - Order is scheduled for delivery$")
	public void popup_verification_from_delivery_button_under_cart() throws Throwable {
		try {
			Thread.sleep(3000);
			Assert.assertEquals("Order is scheduled for delivery", signup.popup.getText());
			System.out.println("Popup message displayed : " + signup.popup.getText());
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^order button should change to Cart button$")
	public void order_button_should_change_to_Cart_button() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			Assert.assertTrue(order.cart_button.isDisplayed());
			System.out.println("Order button changes to cart button :" + order.cart_button.getText());
		} catch (Exception e) {

			e.printStackTrace();
		} finally {

			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			order.cart_button.click();
			order.delete_product(driver);
		}

	}

	@Then("^Delivery button should be highlighed in purple color$")
	public void check_delivery_button_color() throws Throwable {
		try {
			String button_color = order.cart_delivery_button.getCssValue("background-color");
			System.out.println(button_color);
//			Assert.assertEquals(button_color, "rgb[(59, 0, 13]1)");
			assertThat(order.cart_delivery_button.getCssValue("background-color"),
					CoreMatchers.containsString("rgba(59, 0, 131, 1)"));
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^Pickup button should be highlighed in purple color$")
	public void check_pickup_button_color() throws Throwable {
		try {
			String button_color = order.cart_pickup_button.getCssValue("background-color");
			System.out.println(button_color);
//			Assert.assertEquals(button_color, "rgb[(59, 0, 13]1)");
			assertThat(order.cart_pickup_button.getCssValue("background-color"),
					CoreMatchers.containsString("rgba(59, 0, 131, 1)"));
		} finally {
			order.delete_product(driver);
		}
	}

	@And("^user clicks on Delivery button in the cart$")
	public void click_delivery_button_in_the_cart() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.cart_delivery_button));
		order.cart_delivery_button.click();
		System.out.println("Delivery button inside cart is clicked");
	}

	@And("^user clicks on Pickup button in the cart$")
	public void click_pickup_button_in_the_cart() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.cart_pickup_button));
		order.cart_pickup_button.click();
		System.out.println("Pickup button inside cart is clicked");
	}

	@Then("^popup appears saying - This address isnt in this store's delivery zone. Please enter a new address$")
	public void verify_popup_received() throws Throwable {
		try {
			Thread.sleep(3000);
			Assert.assertEquals("This address isn't in this store's delivery zone. Please enter a new address.",
					signup.popup.getText());
			System.out.println("Popup message displayed : " + signup.popup.getText());
		} finally {
			order.delete_product(driver);
		}

	}

	@Then("^Address box for delivery should be removed$")
	public void Address_box_for_delivery_should_be_removed() throws Throwable {

		try {
			Thread.sleep(3000);
			Assert.assertTrue(!order.address_cart.isDisplayed());
			System.out.println("Address box for delivery is removed on clicking Pickup button inside cart");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			order.delete_product(driver);
		}
	}

	@And("^user deletes the items from the cart$")
	public void deletes_the_items_from_the_cart() throws Throwable {
		try {
			System.out.println("deleting cart item #########################");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^Cart button should change to Order button$")
	public void Cart_button_should_change_to_Order_button() throws Throwable {

		try {
			Thread.sleep(3000);
			Assert.assertTrue(order.order_button.isDisplayed());
			System.out.println("Cart button changes to Order button :" + order.order_button.getText());
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@Then("^checkout page should display details as per pickup order$")
	public void checkout_page_should_display_details_as_per_pickup_order() throws Throwable {

		try {
			Thread.sleep(3000);
			Assert.assertFalse(order.Recipient_Delivery_Information.isDisplayed());
			System.out.println("Recepient delivery info. is not displayed");
		} catch (NoSuchElementException | ElementNotInteractableException e) {
			System.out.println("Recepient delivery info. is not displayed");
		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);
		}
	}

	@Then("^checkout page should display details as per delivery order$")
	public void checkout_page_should_display_details_as_per_delivery_order() throws Throwable {

		try {
			Thread.sleep(3000);
			Assert.assertTrue(order.Recipient_Delivery_Information.isDisplayed());
			System.out.println("Recepient delivery info. is displayed");
		} catch (NoSuchElementException | ElementNotInteractableException e) {
			System.out.println("Recepient delivery info. is not displayed");
		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);
		}
	}

	@Then("^cart image should be displayed inside Cart button$")
	public void verify_cart_image() throws Throwable {

		try {
			Thread.sleep(3000);
			Assert.assertTrue(order.cart_image.isDisplayed());
			System.out.println("Cart image is displayed inside Cart button");
		} finally {

			order.delete_product(driver);
		}
	}

	@And("^user clicks on the added product in the cart$")
	public void clicks_on_the_added_product_in_the_cart() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.product_ordered));
		order.product_ordered.click();
	}

	@And("^user selects a quantity from the dropdown$")
	public void quantity_from_the_dropdown() throws Throwable {

		Thread.sleep(5000);
		Select quantity_dropdown = new Select(order.quantity_update_dropdown);
		quantity_dropdown.selectByIndex(1);

	}

	@And("^user clicks update product button$")
	public void product_update_button() throws Throwable {

		order.quantity_update_button.click();
	}

	@Then("^updated quantity should reflect in the cart$")
	public void verify_updated_quantity() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
//			order.cart_button.click();
			Thread.sleep(2000);

//			Assert.assertEquals("2x Insomniac (24 Traditional) ", order.product_ordered.getText());
			assertThat(order.product_ordered.getText(), CoreMatchers.containsString("2x"));
			System.out.println("User is able to edit product in the cart");
			log.info("User is able to edit product in the cart");
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^Delivery address should be displayed as mentioned below:$")
	public void verify_delivery_address_on_tracking_no(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		Thread.sleep(5000);
		assertThat(order.store_name.getText(), CoreMatchers.containsString(data.get(0).get(1)));
		System.out.println(order.store_name.getText());

	}

	@Then("^Pickup address should be displayed as mentioned below:$")
	public void verify_Pickup_address_on_tracking_no(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		Thread.sleep(3000);
		assertThat(order.store_name.getText(), CoreMatchers.containsString(data.get(0).get(1)));
		System.out.println(order.store_name.getText());

	}

	@Then("^Delivery address should be displayed on tracking page as mentioned below:$")
	public void verify_delivery_address_on_tracking_page(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		Thread.sleep(3000);
		assertThat(order.address_on_tracking_page.getText(), CoreMatchers.containsString(data.get(0).get(1)));
		System.out.println(order.address_on_tracking_page.getText());

	}

	@Then("^Pickup and Delivery buttons should be present in the cart$")
	public void Pickup_and_Delivery_buttons_should_be_present() throws Throwable {

		try {
			Assert.assertTrue(order.cart_delivery_button.isDisplayed());
			System.out.println("Delivery button in the cart is displayed");
			Assert.assertTrue(order.cart_pickup_button.isDisplayed());
			System.out.println("Pickup button in the cart is displayed");
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^Pickup and Delivery buttons should not be displayed in the cart$")
	public void Pickup_and_Delivery_buttons_should_not_be_present() throws Throwable {

		try {
			Assert.assertTrue(!order.cart_delivery_button.isDisplayed());
			System.out.println("Delivery button in the cart is not displayed");
			Assert.assertTrue(!order.cart_pickup_button.isDisplayed());
			System.out.println("Pickup button in the cart is not displayed");
		} finally {
			order.delete_product(driver);
		}

	}

	@Then("^Only Pickup buttons should be present in the cart$")
	public void Only_Pickup_buttons_should_be_present_in_the_cart() throws Throwable {
		try {
			Assert.assertTrue(!order.cart_delivery_button.isDisplayed());
			System.out.println("Delivery button in the cart is not displayed");
			Assert.assertTrue(order.only_cart_pickup_button.isDisplayed());
			System.out.println("Only Pickup button in the cart is  displayed");
		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^popup should appear stating - Coupon successfully applied$")
	public void amount_coupon_popup() throws Throwable {
		try {
			Thread.sleep(2000);
			Assert.assertEquals("Coupon successfully applied.", signup.popup.getText());
			System.out.println(signup.popup.getText());

		} catch (Exception e) {
			e.getMessage();

		}
	}

	@And("^Amount coupon should be added successfully to the cart$")
	public void Amount_coupon_should_be_added_successfully_to_the_cart() throws Throwable {
		try {
			Thread.sleep(3000);
			Assert.assertTrue(order.amount_coupon.isDisplayed());
			System.out.println("Amount coupon is applied successfully by logged in user");
			Thread.sleep(500);

		} finally {
			wait.until(ExpectedConditions.visibilityOf(order.remove_coupon_button));
			order.remove_coupon_button.click();
			System.out.println("Coupon removed");
			order.delete_product(driver);
		}
	}

	@And("^Percentage coupon should be added successfully to the cart$")
	public void Percentage_coupon_should_be_added_successfully_to_the_cart() throws Throwable {
//		try {
			Thread.sleep(3000);

			String product_cost = order.product_cost.getText().substring(1);
			double cost = Double.parseDouble(product_cost);

			System.out.println("product cost : " + cost);

			String discount_cost = order.percentage_coupon.getText().substring(2);
			double discount = Double.parseDouble(discount_cost);
			System.out.println("Discount coupon value : " + discount);

			Assert.assertEquals((0.1 * cost), discount, discount);
			System.out.println("Percentage coupon is applied successfully by logged in user");

//		} finally {
//			wait.until(ExpectedConditions.visibilityOf(order.remove_coupon_button));
//			order.remove_coupon_button.click();
//			System.out.println("Coupon removed");
//			order.delete_product(driver);
//		}
	}

	@And("^FreeDelivery coupon should be added successfully to the cart$")
	public void FreeDelivery_coupon_should_be_added_successfully_to_the_cart() throws Throwable {
		try {
			Thread.sleep(3000);
			Assert.assertTrue(order.delivery_coupon.isDisplayed());
			System.out.println("Delivery coupon is applied successfully by logged in user");

		} finally {
			wait.until(ExpectedConditions.visibilityOf(order.remove_coupon_button));
			order.remove_coupon_button.click();
			System.out.println("Coupon removed");
			order.delete_product(driver);
		}
	}

	@Then("same as above checkbox should be checked by default")
	public void verify_same_As_above_checkbox() throws Throwable {

		try {
			Thread.sleep(1500);
			wait.until(ExpectedConditions.visibilityOf(order.same_As_above));
			System.out.println(order.same_As_above.getAttribute("checked"));
			Assert.assertTrue(order.same_As_above.getAttribute("checked"), true);
			System.out.println("same as above checkox is clicked by default");

		} /*
			 * catch(Exception e) { e.printStackTrace(); System.out.
			 * println("same as above checkbox is not clicked by default"); }
			 */finally {
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);
		}
	}

	@Then("count of items should be displayed in the cart")
	public void count_items() throws Throwable {
		try {
			wait.until(ExpectedConditions.visibilityOf(order.cart_button_item_count));
			Assert.assertTrue(order.cart_button_item_count.isDisplayed());

		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);
		}

	}

	@And("^user clicks Remove button in the cart$")
	public void click_remove_button() throws Throwable {

		wait.until(ExpectedConditions.visibilityOf(order.remove_coupon_button));
		order.remove_coupon_button.click();
		System.out.println("Remove Coupon button is clicked");
	}

	@Then("^coupon should be removed successfully$")
	public void verify_coupon_removed() throws Throwable {

		try {
			wait.until(ExpectedConditions.visibilityOf(order.apply));
			Assert.assertTrue("", order.coupon_box.getText().isEmpty());
			System.out.println("Coupon removed successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Then("^popup should appear stating - Coupon Test not found$")
	public void verify_popup_for_wrong_coupon(DataTable args) throws Throwable {

		List<List<String>> data = args.raw();

		try {
			// Thread.sleep(2000);

			Assert.assertEquals(data.get(0).get(1), signup.popup.getText());
			System.out.println(signup.popup.getText());

		} catch (Exception e) {
			e.getMessage();

		} finally {
			order.delete_product(driver);
		}
	}

	@Then("^popup should appear stating - This coupon has expired$")
	public void verify_popup_for_expired_coupon(DataTable args) throws Throwable {

		List<List<String>> data = args.raw();

		try {
			// Thread.sleep(2000);

			Assert.assertEquals(data.get(0).get(1), signup.popup.getText());
			System.out.println(signup.popup.getText());

		} catch (Exception e) {
			e.getMessage();

		} finally {
			order.delete_product(driver);
		}
	}

	@And("^user clicks right arrow$")
	public void user_clicks_right_arrow() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(order.forward_arrow));
		order.forward_arrow.click();
		System.out.println("forward arrow is clicked");
	}

	@Then("^right and left arrow should be displayed in the calender to change months$")
	public void right_and_left_arrow_should_be_displayed_in_the_calender_to_change_months() throws Throwable {

		Assert.assertTrue(order.right_date_arrow.isDisplayed());
		System.out.println("right date arrow is displayed");
		Assert.assertTrue(order.left_date_arrow.isDisplayed());
		System.out.println("left date arrow is displayed");

	}

	@Then("^popup should appear stating - This coupon requires a minimum purchase of ten dollar\\. You currently have Eight point eighty dollar in your cart$")
	public void popup_should_appear_stating_This_coupon_requires_a_minimum_purchase_of_ten_dollar_You_currently_have_six_dollar_in_your_cart()
			throws Throwable {

		try {
			// Thread.sleep(3000);
			Assert.assertEquals(
					"This coupon requires a minimum purchase of $10.00. You currently have $1.65 in your cart",
					signup.popup.getText());
			System.out.println(signup.popup.getText());

		} catch (Exception e) {
			e.getMessage();

		} finally {

			order.delete_product(driver);

		}

	}

	@Then("^popup should appear stating$")
	public void popup_should_appear_stating$(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();
		try {
			wait.until(ExpectedConditions.visibilityOf(signup.popup));
			Assert.assertEquals(data.get(0).get(0), signup.popup.getText());
			System.out.println(signup.popup.getText());

		} finally {
//			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
//			order.alt_cart_button.click();
			order.delete_product(driver);

		}

	}

	@Then("^popup should appear stating - You can not select asap if the current time is outside of the selected stores hours$")
	public void You_can_not_select_asap_if_the_current_time_is_outside_of_the_selected_stores_hours() throws Throwable {

		// Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOf(signup.popup));
		Assert.assertEquals("You can not select asap if the current time is outside of the selected stores hours.",
				signup.popup.getText());
		System.out.println(signup.popup.getText());

	}

	@And("^On Menu Page click on hundred cookies$")
	public void On_Menu_Page_click_on_hundred_cookies() throws Throwable {

		try {
			Thread.sleep(10000);
			// driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			// JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollTo(0, 8000)");

			wait.until(ExpectedConditions.elementToBeClickable(order.hundred_cookies));
			// Actions action = new Actions(driver);

			action.moveToElement(order.hundred_cookies).build().perform();

			Thread.sleep(2000);
			order.hundred_cookies.click();

		} catch (Exception e) {
			e.getMessage();
			wait.until(ExpectedConditions.elementToBeClickable(order.hundred_cookies_pic));
			// Actions action = new Actions(driver);

			action.moveToElement(order.hundred_cookies_pic).build().perform();
			Thread.sleep(2000);
			order.hundred_cookies_pic.click();
		}
	}

	@Then("^payment option cash should not be displayed on checkout page$")
	public boolean payment_option_cash_should_not_be_displayed_on_checkout_page() throws Throwable {

		try {
			Thread.sleep(10000);
			Assert.assertEquals(0, order.cash.getSize());
			System.out.println(order.cash.getSize());

		} catch (Exception e) {
			e.printStackTrace();
			return true;
		} finally {
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);
		}
		return false;
	}

	@And("^user clicks on Address icon$")
	public void user_clicks_on_Address_icon() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(order.address_book_icon));
		order.address_book_icon.click();
	}

	@Then("^Address book  page should open$")
	public void address_book_page_should_open() throws Throwable {

		try {
			wait.until(ExpectedConditions.visibilityOf(order.address_book_header));
			Assert.assertTrue(order.address_book_header.isDisplayed());

			System.out.println(order.address_book_header.getText());
		} catch (Exception e) {

			wait.until(ExpectedConditions.visibilityOf(order.alt_address_book_header));
			Assert.assertTrue(order.alt_address_book_header.isDisplayed());

			System.out.println(order.alt_address_book_header.getText());
		}

	}

	@And("^user clicks on left arrow to see address$")
	public void click_left_arrow() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.left_arrow));
		order.left_arrow.click();
	}

	@And("^user clicks on right arrow to see address$")
	public void click_right_arrow() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(order.right_arrow));
		order.right_arrow.click();
	}

	@And("^user is not able to locate store displaying message displayed$")
	public void locate_store() throws Throwable {
		wait.until(ExpectedConditions.visibilityOf(order.no_store_present));
		if (order.no_store_present.isDisplayed()) {
			Thread.sleep(2000);
			System.out.println(order.no_store_present.getText());
		}

	}

	@Then("^store should be displayed$")
	public void verify_store_displayed() throws Throwable {

		wait.until(ExpectedConditions.visibilityOf(order.store_displayed));
		Assert.assertTrue(order.store_displayed.isDisplayed());
		System.out.println(order.store_displayed.getText());
	}

	@Then("^User should able to move left arrow to see near by stores$")
	public void verify_new_store_address() throws Throwable {

		wait.until(ExpectedConditions.visibilityOf(order.new_store_address_left));
		Assert.assertTrue(order.new_store_address_left.isDisplayed());

		System.out.println(order.new_store_address_left.getText());

	}

	@Then("^User should able to move right arrow to see near by stores$")
	public void verify_right_arrow() throws Throwable {

		wait.until(ExpectedConditions.visibilityOf(order.new_store_address_right));
		Assert.assertTrue(order.new_store_address_right.isDisplayed());
		order.new_store_address_right.isEnabled();

		System.out.println(order.new_store_address_right.getText());
	}

	@Then("^Other stores should be disabled$")
	public void verify_other_stores() throws Throwable {
		try {
			wait.until(ExpectedConditions.visibilityOf(order.disabled_pickup_one));
			// Assert.assertFalse(order.disabled_pickup_one.isEnabled());
			System.out.println("Pickup button is disabled");

			wait.until(ExpectedConditions.visibilityOf(order.disabled_pickup_two));
			Assert.assertFalse(order.disabled_pickup_two.isEnabled());
			System.out.println("Pickup button is disabled");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Then("^catering availability with green tick in stores should be visible$")
	public void verify_green_tick() throws Throwable {

		wait.until(ExpectedConditions.visibilityOf(order.catering_check));
		Assert.assertTrue(order.catering_check.isDisplayed());
		System.out.println("catering availability with green tick in stores is visible");
	}

	@Then("^catering unavailability is shown with green tick not visible$")
	public boolean verify_cross_mark() throws Throwable {

		try {
			wait.until(ExpectedConditions.visibilityOf(order.catering_check));
			Assert.assertTrue(!order.catering_check.isDisplayed());
			log.info("catering unavailability is shown with green tick not visible");

		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}

		return false;
		/*
		 * wait.until(ExpectedConditions.visibilityOf(order.catering_cross));
		 * Assert.assertTrue(order.catering_cross.isDisplayed()); System.out.
		 * println("catering unavailability with red crossmark in stores is visible" );
		 */
	}

	@And("^user removes entered address$")
	public void user_removes_entered_address() throws Throwable {

		order.address_box.clear();
		System.out.println("Entered address in the Address box is cleared");
	}

	@And("^user clicks on Gifts tab$")
	public void click_Gifts_tab() throws Throwable {
		Thread.sleep(3000);
		// wait.until(ExpectedConditions.elementToBeClickable(order.gifts_tab));

		// order.gifts_tab.click();

		// Actions action = new Actions(driver);
		action.doubleClick(order.gifts_tab).build().perform();

		Thread.sleep(3000);
		order.gifts_tab.click();

	}

	@And("^user clicks Gift Card option$")
	public void click_gift_card() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(order.gift_card_tab));
		order.gift_card_tab.click();
	}

	@Then("^popup should appear$")
	public void popup_should_appear() throws Throwable {
		try {
			wait.until(ExpectedConditions.visibilityOf(order.confirm_changing_store));
			Assert.assertEquals("Confirm changing store", order.confirm_changing_store.getText());
			System.out.println(order.confirm_changing_store.getText());

		} catch (Exception e) {
			e.getMessage();

		}

	}

	@And("^on clicking Okay button$")
	public void clicking_okay_button() throws Throwable {
		wait.until(ExpectedConditions.visibilityOf(order.change_store_alert_button));
		order.change_store_alert_button.click();
	}

	@Then("^cart should get cleared$")
	public void cart_should_get_cleared() throws Throwable {
		try {
			wait.until(ExpectedConditions.visibilityOf(order.order_button));
			Assert.assertTrue(order.order_button.isDisplayed());
		} catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(order.order_button));
			Assert.assertTrue(order.order_button.isDisplayed());

		}
	}

	@Then("^only pickup button should be displayed$")

	public boolean only_pickup_button() throws Throwable {
		try {
			Assert.assertEquals(0, order.delivery_button.getSize());
			log.info("Delivery button should not be displayed :Size = " + order.delivery_button.getSize());

			Assert.assertTrue(order.pickup_only.isDisplayed());
			log.info("Pickup button should not be displayed :Size = " + order.pickup_only.getSize());

		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
		return false;
	}
	

	@And("^verify Map button is displayed$")
	public void verify_Map_BtnisDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(order.mapButton));
		System.out.println("Map button is displayed");		
	}
    
    @When("^Click on Map button$")
	public void click_on_MapBtn() throws InterruptedException {
    	order.mapButton.click();
    	Thread.sleep(4000);
	}
    
    @Then("^Verify Map gets open$")
	public void verify_Map_gets_Open() {
    	wait.until(ExpectedConditions.visibilityOf(order.hideMapButton));
		System.out.println("Map gets displayed success");	
	}
    
    @And("^click on Hide Map button$")
	public void click_on_hideMapBtn() {
    	order.hideMapButton.click();	
	}
    @Then("^verify Map is closed$")
	public void verify_Map_gets_Closed() {
    	wait.until(ExpectedConditions.visibilityOf(order.mapButton));
		System.out.println("Map gets closed success");	
	}
    
    
    
}
