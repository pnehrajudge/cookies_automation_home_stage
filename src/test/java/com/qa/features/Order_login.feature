@OrderLogin

Feature: Order_Login_Part1

  Background: user access to website
    Given User opens the Website without clearing cache

  # | url | https://5.stage.insomniacookies.com/ |

  Scenario: Precondition - user logs in as a current customer
    Given Click on Log In link present in top right corner of the homepage
    When User enters valid Email and Password
      | username | icprateeknehra@gmail.com |
      | password | Judge@123         |
    And Click on the Log In button

  #Then User must get logged in and UserName should be displayed in the heade

  Scenario: . TC036 - Logged in user : Verify User is able to place an Delivery order with Credit card
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


 
  #Scenario: TC037 - Logged in user : Verify User is able to place a Delivery order with Cash
    #Given user clicks on Order button
    #When user enters the address
      #| address | Northwestern University, Clark Street, Evanston, IL |
    #And user clicks on Delivery button
    #And user select Date and Time from Calendar
    #And user clicks on Continue
    #And On Menu Page click on The Insomniac
    #And user clicks on add product
    #And user goes to the cart
    #And user clicks on the checkout button
    #And user selects Payment Method as cash
    #And user clicks on Place Order
    #Then Order confirmation page should appear displaying Order summary
  
  Scenario: TC038 - Logged in user : Verify User is able to place a Delivery order with School Cash
    Given user clicks on Order button
    When user enters the address
      | address | 421 E Beaver Ave, State College  pa |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as School cash
    And user enters school cash number
      | School cash | 6020000000000000 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

  Scenario: TC039 - Logged in user : Verify User is able to place a Delivery order with Gift Card
    Given user clicks on Order button
    When user enters the address
      | address | 319 College Avenue Ithaca, NY. |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

  Scenario: TC040 - Logged in user : Verify User is able to place a Pickup order with Credit card
    Given user clicks on Order button
    When user enters the address
      | address | 402 14th Avenue SE Minneapolis, MN |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

  #Scenario: TC041 - Logged in user : Verify User is able to place a Pickup order with Cash
    #Given user clicks on Order button
    #When user enters the address
      #| address | 1600 Warren Street Mankato, MN |
    #And user clicks on Pickup button
    #And user select Date and Time from Calendar
    #And user clicks on Continue
    #And On Menu Page click on The Insomniac
    #And user clicks on add product
    #And user goes to the cart
    #And user clicks on the checkout button
    #And user selects Payment Method as cash
    #And user clicks on Place Order
    #Then Order confirmation page should appear displaying Order summary

 
  Scenario: TC042 - Logged in user : Verify User is able to place a Pickup order with School Cash
    Given user clicks on Order button
    When user enters the address
      | address | 421 E Beaver Ave, State College  pa |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as School cash
    And user enters school cash number
      | School cash | 6020000000000000 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


  Scenario: TC043 Logged in user - Verify User is able to place a Pickup order with Gift Card
    Given user clicks on Order button
    When user enters the address
      | address |  758 Asp Ave, Norman OK |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

  Scenario: TC045  Logged in user - Verify User is able to track an Delivery Order just after Ordering
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user clicks on Place Order
    And user clicks on Track Order button
    Then Tracking ID should be displayed along with status

  Scenario: TC046  Logged in user - Verify User is able to track a Pickup Order just after Ordering
    Given user clicks on Order button
    When user enters the address
      | address | 733 West Cross St. Ypsilanti, MI |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Gift card
    And user enters Gift card number
      | Gift Card | 9999998888877651 |
    And user clicks on Place Order
    And user clicks on Track Order button
    Then Tracking ID should be displayed along with status

  Scenario: TC049 - Logged in user - Verify user is able to set tip for an Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1227 Washington Avenue Miami Beach, FL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 03 20            |
    And user selects tip
      | tip selected | 1.00 |
    And validation popup appears saying tip updated
    And user clicks on Place Order
    Then Order confirmation page should appear displaying tip in Order summary
      | tip displayed | $1.00 |

 
  Scenario: TC051 - Logged in user - Verify loyalty point earned for Delivery total under 20$ calculates to 1$ = 1point
    Given user clicks on Order button
    When user enters the address
      | address | 1084 lancaster avenue, bryn mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Choco Chunk
    And user goes to the cart
    Then Cart should state, You will earn One points for this order

  
  Scenario: TC052 - Logged in user - Verify loyalty point earned for Delivery total above 20$ calculates to 1$ = 1.25 point
    Given user clicks on Order button
    When user enters the address
      | address | 1229 South University Avenue Ann Arbor, MI |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then Cart should state You will earn Thirty Two points for this order

  
  Scenario: TC053 - Logged in user - Verify loyalty point earned for Pickup total under 20$ calculates to 1$ = 1point
    Given user clicks on Order button
    When user enters the address
      | address | 421 8th Avenue, New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Choco Chunk
    And user goes to the cart
    Then Cart should state, You will earn One points for this order

  Scenario: TC054 - Logged in user - Verify loyalty point earned for Pickup total above 20$ calculates to 1$ = 1.25 point
    Given user clicks on Order button
    When user enters the address
      | address | 2868 West Berry Street Fort Worth, TX |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then Cart should state You will earn Thirty Two points for this order


  Scenario: TC055 - Logged in user - Verify user is able to apply Product coupon 2 Free Cookies for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr bryn |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Product code | 2Free |
    And user clicks Apply button
    And user adds free cookies
    And user clicks Add free item button
    Then popup should appear stating - Coupon successfully applied
    And Free cookies should be added to the cart
    

  Scenario: TC056 - Logged in user - Verify user is able to apply Amount coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then popup should appear stating - Coupon successfully applied
    And Amount coupon should be added successfully to the cart


  Scenario: TC057 - Logged in user - Verify user is able to apply Percentage coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Percentage code | Warmgifts |
    And user clicks Apply button
    Then popup should appear stating - Coupon successfully applied
    And Percentage coupon should be added successfully to the cart

  Scenario: TC058 - Logged in user - Verify user is able to apply Free Delivery coupon for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Free Delivery code | TestFreeDelivery |
    And user clicks Apply button
    Then popup should appear stating - Coupon successfully applied
    And FreeDelivery coupon should be added successfully to the cart


  Scenario: TC059 - Logged in user - Verify user is able to apply Product coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Product code | 2Free |
    And user clicks Apply button
    And user adds free cookies
    And user clicks Add free item button
    Then popup should appear stating - Coupon successfully applied
    And Free cookies should be added to the cart

  Scenario: TC060 - Logged in user - Verify user is able to apply Amount coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then popup should appear stating - Coupon successfully applied
    And Amount coupon should be added successfully to the cart


  Scenario: TC061 - Logged in user - Verify user is able to apply Percentage coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Percentage code | Warmgifts |
    And user clicks Apply button
    Then popup should appear stating - Coupon successfully applied
    And Percentage coupon should be added successfully to the cart

  Scenario: Logged in user - Verify user is not able to apply Free Delivery coupon for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Free Delivery code | DELIVERY10 |
    And user clicks Apply button
    Then popup should appear stating - Coupon successfully applied
    And FreeDelivery coupon should be added successfully to the cart

  Scenario: TC065 - Logged in user - Verify user is not able to place an Delivery order for past date
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Delivery button
    And user selects date from calender
    Then user should not be able to select back date


  Scenario: TC063 - Logged in user - Verify user is not able to place an Pickup order for past date
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Pickup button
    And user selects date from calender
    Then user should not be able to select back date

  Scenario: TC062 - Logged in user - Verify user is not able to place an Delivery order beyond 6 months
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Delivery button
    And user selects date from calender beyond six months but is unable to do so

  Scenario: TC064 - Logged in user - Verify user is not able to place an Pickup order beyond 6 months
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Pickup button
    And user selects date from calender beyond six months but is unable to do so


  Scenario: TC066 - Logged in user - Verify user is able to delete items from cart and add them again into it for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user deleted the product
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then product should be present

  Scenario: TC067 - Logged in user - Verify user is able to delete items from cart and add them again into it for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 304 West 14th Street New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user deleted the product
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    Then product should be present


  Scenario: TC068 - Logged in user - Verify Upsell is displayed for the Delivery orders
    Given user clicks on Order button
    When user enters the address
      | address | ABC Kitchen, East 18th Street, New York, NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then upsell should be displayed

  Scenario: TC069 - Logged in user - Verify Upsell is displayed for the Pickup orders
    Given user clicks on Order button
    When user enters the address
      | address | 482 Third Avenue New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then upsell should be displayed


  Scenario: TC072 - Logged in user - Verify same as above is checked by default once user reaches checkout page
    Given user clicks on Order button
    When user enters the address
      | address | 412 Broadway North, Fargo, ND 58102 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then same as above checkbox should be checked by default

  Scenario: TC073 Logged in user - Verify cart image shows the count of items added to the cart
    Given user clicks on Order button
    When user enters the address
      | address | 128 West Chimes St Baton Rouge, LA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    Then count of items should be displayed in the cart

  Scenario: TC074 - Logged in user - Verify that user is getting a pop up  stating that Delivery instructions saved when entering data in delivery instruction textbox
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    Then User should get a pop up stating that Delivery instructions saved


  Scenario: TC075 - Logged in user - Verify that user is getting a pop up  stating that Message successfully added when entering data in Message for recipient textbox
    Given user clicks on Order button
    When user enters the address
      | address | 3205 Hardy St. 39401 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters message for recepient
      | message | This is for you |
    Then User should get a pop up stating that Message successfully added


  Scenario: TC076 : Logged in user - Verify user is able to remove cookie dough after applying it for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on cookie dough button
    And user clicks on remove cookie dough button
    Then user should be able to remove cookie dough after applying it for Delivery Order

   Scenario: TC077 : Logged in user - Verify User should be able to remove all applied coupons
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    And user clicks Remove button in the cart
    Then coupon should be removed successfully


   Scenario: TC078 : Logged in user - Verify that user get a popup "Coupon not found" when entering wrong coupon code
    Given user clicks on Order button
    When user enters the address
      | address | 2171 University Avenue, Morgantown, WV |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | coupon code | Test |
    And user clicks Apply button
    Then popup should appear stating - Coupon Test not found
      | popup | Coupon "Test"" not found |

@OrderLoginDebug
  Scenario: TC079 : Logged in user - Verify that when coupon gets expired user should get a pop "This Coupon has Expired"
    Given user clicks on Order button
    When user enters the address
      | address | 2171 University Avenue, Morgantown, WV |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | coupon code | TEST_EXPIRED |
    And user clicks Apply button
    Then popup should appear stating - This coupon has expired
      | popup | This coupon has expired |
    
  Scenario: TC083 - Logged in user - Verify that user is getting pop up after adding product in cart
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    Then User should get a pop up stating Product Name added to cart


  Scenario: TC84 -  Logged in user - Verify user is able to update delivery time from the cart for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St Philadelphia, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on Delivery time & date displayed on the cart top
    And user changes date
    And user changes time
    And user clicks Update button
    Then updated date & time should be displayed on the cart top

  Scenario: TC087 - Logged in user - Verify right & left arrows are displayed in the calander to change months for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Pickup button
    And user selects date from calender
    And user clicks right arrow
    Then right and left arrow should be displayed in the calender to change months

  Scenario: TC088 - Logged in user - Verify right & left arrows are displayed in the calander to change months for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 115 MacDougal Street, New York NY |
    And user clicks on Delivery button
    And user selects date from calender
    And user clicks right arrow
    Then right and left arrow should be displayed in the calender to change months

  Scenario: TC089 - Logged in user - Verify FeedDelivery coupon does pop-up if order value is less than 10$ for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave S, Birmingham, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on a Choco_Chunk
    #And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Delivery code | DELIVERY10 |
    And user clicks Apply button
    Then popup should appear stating - This coupon requires a minimum purchase of ten dollar. You currently have Eight point eighty dollar in your cart


  Scenario: TC90 - Logged in user - Verify Amount coupon display pop-up if order value is less than 10$ for Delivery Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr, PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on a Choco_Chunk
    #And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then popup should appear stating - This coupon requires a minimum purchase of ten dollar. You currently have Eight point eighty dollar in your cart

 
  Scenario: TC91 - Logged in user - Verify FeedDelivery coupon does pop-up if order value is less than 10$ for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on a Choco_Chunk
    #And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Delivery code | DELIVERY10 |
    And user clicks Apply button
    Then popup should appear stating - This coupon requires a minimum purchase of ten dollar. You currently have Eight point eighty dollar in your cart


  Scenario: TC92 - Logged in user - Verify Amount coupon display pop-up if order value is less than 10$ for Pickup Order
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr, PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on a Choco_Chunk
    #And user clicks on add product
    And user goes to the cart
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    Then popup should appear stating - This coupon requires a minimum purchase of ten dollar. You currently have Eight point eighty dollar in your cart


  Scenario: TC93 - Logged in user - Verify FeedDelivery coupon gets removed in case user removes a product from cart and order value goes below 10$
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave S, Birmingham, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart again
    And user enters Coupon
      | Delivery code | DELIVERY10 |
    And user clicks Apply button
    And user deletes the items from the cart
    Then coupon should be removed successfully


  Scenario: TC094 - Logged in user - Verify Amount coupon gets removed in case user removes a product from cart and order value goes below 10$
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave S, Birmingham, AL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on a Six Pack
    And user clicks on add product
    And On Menu Page click on a Six Pack
    And user clicks on add product
    And user goes to the cart again
    And user enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    And user deletes the items from the cart
    Then coupon should be removed successfully


  Scenario: TC095 Logged in user - Verify pop-up appears if Delivery order subtotal < 6$ but if Pickup order < 6$, user proceeds for checkout
    Given user clicks on Order button
    When user enters the address
      | address | 1919 11th Ave S, Birmingham, AL |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Choco Chunk
    And user goes to the cart
    And user clicks on the checkout button
    #And user selects Payment Method as cash
    #And user clicks on Place Order
    Then popup should appear stating
      | In order to use this shipping method, you must order a minimum of $6.00. You order is currently $1.65 |


  Scenario: TC96 - Logged in user - Verify user is not able to proceed futher by selecting current date and time ASAP when selected store is closed
    Given user clicks on Order button
    When user enters the address
      | address | 1913 South 4th Street, Louisville, KY 40208 |
    And user clicks on Delivery button
    And user clicks on Continue
    Then popup should appear stating - You can not select asap if the current time is outside of the selected stores hours

  Scenario: TC97 - Logged in user - Verify user is able to close menu page for every product
    Given user clicks on Order button
    When user enters the address
      | address | 482 Third Avenue New York, NY |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on  close button
    Then product menu should close

  Scenario: TC098 - Logged in user - Verify there is no Cash option on checkout page on order over $100
    Given user clicks on Order button
    When user enters the address
      | address | 1084 East Lancaster Avenue, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on hundred cookies
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    Then payment option cash should not be displayed on checkout page


  
