package com.qa.stepDefinition;

import static org.junit.Assert.assertThat;
//import static org.testng.AssertJUnit.assertEquals;

import java.util.List;

import org.apache.log4j.Logger;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.AdminStores;
import com.qa.pages.Order;
import com.qa.pages.ShipCookies;
import com.qa.pages.SignIn;
import com.qa.pages.SignUp;
import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;


public class Step_ShipCookies extends TestBase{

	WebDriver driver = getDriver();

	AdminStores adminStores = new AdminStores(driver);
	Order order = new Order(driver);
	SignUp signup = new SignUp(driver);
	SignIn s = new SignIn(driver);
	ShipCookies sc = new ShipCookies(driver);

	// Logger log= Logger.getLogger(step_Order.class);

	Logger log = Logger.getLogger("shauryaLogger");

	WebDriverWait wait = new WebDriverWait(driver, 30);
	
	
	@And("^verify captcha is disabled$")
	public void captcha_code() throws InterruptedException{
		adminStores.disableReCaptcha();
	}

	@And("^user clicks Ship cookies option$")
	public void user_clicks_Ship_cookies_option() throws Throwable {
		try {

			wait.until(ExpectedConditions.elementToBeClickable(sc.ship_cookies_tab));
			sc.ship_cookies_tab.click();

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			Thread.sleep(5000);
			if (order.order_button.isDisplayed()) {

				System.out.println("Cart is empty");
				log.info("Cart is empty");
			}
		} catch (Exception j) {
			j.printStackTrace();
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			// log.info("waiting for cart button to appear");
			log.debug("waiting for cart button to appear");

			order.alt_cart_button.click();
			// log.info("cart button is clicked");
			log.debug("cart button is clicked");
			order.delete_product(driver);
			// log.info("delete product from the cart");
			log.debug("delete product from the cart");
		}

	}

	@And("^user selects Twelve cookies Gift box$")
	public void user_selects_Twelve_cookies_Gift_box() throws Throwable {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(sc.twelve_cookies));
			log.info("waiting for Twelve cookie Gift box to be clickable");
			log.error("Twelve cookie Gift box is not clickable");

			Actions action = new Actions(driver);
			action.moveToElement(sc.twelve_cookies).build().perform();

			Thread.sleep(2000);
			sc.twelve_cookies.click();
			log.info("Twelve cookie Gift box is clicked");

		} catch (Exception e) {

			Actions action = new Actions(driver);
			action.moveToElement(sc.alt_twelve_cookies).build().perform();

			Thread.sleep(2000);
			sc.alt_twelve_cookies.click();
			log.info("Twelve cookie Gift box is clicked");
		}

	}

	@Then("saved credit card should not be visibile to the user")
	public void Saved_credit_should_not_be_visibile_to_the_user() throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait waitLocal = new WebDriverWait(driver, 30);
			waitLocal.until(ExpectedConditions.visibilityOf(sc.saved_credit_card));
			Thread.sleep(4000);
//			Assert.assertTrue(!(sc.saved_credit_card.isDisplayed()));
			System.out.println("Saved credit card is displayed::##### Test Fail");
			flag = true;
		} 
		catch (Exception e){
			flag = false;
			System.out.println("Saved credit card is not displayed::##### Test Pass");
		}
		finally {
			Assert.assertTrue("Saved credit card is displayed::##### Test Fail", !flag);
			wait.until(ExpectedConditions.elementToBeClickable(order.alt_cart_button));
			order.alt_cart_button.click();
			order.delete_product(driver);

		}

		/*log.info("Saved credit card is displayed");
		return false;*/

	}
	
	@And("user selects saved credit card")
	public void user_clicks_saved_credit_card() throws Throwable{
		
		
		wait.until(ExpectedConditions.visibilityOf(sc.saved_credit_card));
		Select saved_credit_card = new Select(sc.saved_credit_card);
		Thread.sleep(3000);
		saved_credit_card.selectByIndex(1);
//		sc.saved_credit_card.click();
		log.info("Saved Credit card is selected");
		
	}
	
	

	@Then("^ship cookies page should be displayed successfully$")
	public boolean verify_ship_cookies_page() throws Throwable {

		try {
			wait.until(ExpectedConditions.visibilityOf(sc.ship_cookies_page));

			Assert.assertTrue(sc.ship_cookies_page.isDisplayed());
			System.out.println(sc.ship_cookies_page.getText());
			log.info("ship cookies page is displayed successfully");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("ship cookies page is not displayed successfully");
		return false;
	}

	@And("^user enters Valid Details under recipient Delivery Info & your Info$")
	public void user_enters_Valid_Details_under_recipient_Delivery_Info_your_Info(DataTable table) throws Throwable {

		Thread.sleep(10000);

		List<List<String>> data = table.raw();

		System.out.println(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.recipient_name));
		order.recipient_name.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.recipient_phone));
		order.recipient_phone.sendKeys(data.get(1).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_name));
		order.customer_name.sendKeys(data.get(2).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_phone));
		order.customer_phone.sendKeys(data.get(3).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(order.customer_email));
		order.customer_email.sendKeys(data.get(4).get(1));

	}

	@And("^user enters shipping address$")
	public void user_enters_shipping_address(DataTable table) throws Throwable {
		
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Thread.sleep(10000);
		try{
		List<List<String>> data = table.raw();
		wait.until(ExpectedConditions.elementToBeClickable(sc.recipient_address_box));
		sc.recipient_address_box.sendKeys(data.get(0).get(1));
		wait.until(ExpectedConditions.elementToBeClickable(sc.address_suggestion));
		sc.address_suggestion.click();		
		Thread.sleep(5000);
		}catch (Exception e){
			e.printStackTrace();
			driver.navigate().refresh();
			List<List<String>> data = table.raw();
			wait.until(ExpectedConditions.elementToBeClickable(sc.recipient_address_box));
			sc.recipient_address_box.sendKeys(data.get(0).get(1));
			wait.until(ExpectedConditions.elementToBeClickable(sc.address_suggestion));
			sc.address_suggestion.click();		
			Thread.sleep(5000);
			
		}

	}

	@And("^user selects triple layer cookie cake$")
	public void user_selects_triple_layer_cookie_cake() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(sc.triple_layer_cake));
		log.info("waiting for Twelve cookie Gift box to be clickable");
		log.error("Twelve cookie Gift box is not clickable");

		Actions action = new Actions(driver);
		action.moveToElement(sc.triple_layer_cake).build().perform();

		Thread.sleep(2000);
		sc.triple_layer_cake.click();
		log.info("Twelve cookie Gift box is clicked");
	}

	@And("^user adds cookies by clicking \\+$")
	public void user_adds_cookies_by_clicking() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(sc.triple_layer_choco_chunk));
		sc.triple_layer_choco_chunk.click();
		wait.until(ExpectedConditions.elementToBeClickable(sc.triple_layer_classic));
		sc.triple_layer_classic.click();
		wait.until(ExpectedConditions.elementToBeClickable(sc.triple_layer_double_choco_chunk));
		sc.triple_layer_double_choco_chunk.click();
	}

	@And("^user clicks Add Product button$")
	public void user_clicks_Add_Product_button() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(sc.add_product_button));
		sc.add_product_button.click();
	}

	@And("^user selects Eighteen cookies Gift box$")
	public void user_selects_Eighteen_cookies_Gift_box() throws Throwable {

		wait.until(ExpectedConditions.elementToBeClickable(sc.eighteen_cookies_gift_box));
		log.info("waiting for Eighteen cookie Gift box to be clickable");
		log.error("Eighteen cookie Gift box is not clickable");

		Actions action = new Actions(driver);
		action.moveToElement(sc.eighteen_cookies_gift_box).build().perform();

		Thread.sleep(2000);
		sc.eighteen_cookies_gift_box.click();
		log.info("Eighteen cookie Gift box is clicked");
	}

	@And("^user selects Twenty Four cookies Gift box$")
	public void user_selects_Twenty_Four_cookies_Gift_box() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(sc.twenty_four_cookies_gift_box));
		log.info("waiting for Eighteen cookie Gift box to be clickable");
		log.error("Eighteen cookie Gift box is not clickable");

		Actions action = new Actions(driver);
		action.moveToElement(sc.twenty_four_cookies_gift_box).build().perform();

		Thread.sleep(2000);
		sc.twenty_four_cookies_gift_box.click();
		log.info("Eighteen cookie Gift box is clicked");

	}

	@And("^user selects cookies manually$")
	public void select_twelve_cookies_manually() throws Throwable {

		wait.until(ExpectedConditions.visibilityOfAllElements(sc.manual_cookies_selection));
		for (int i = 0; i < 3; i++) {
			for (WebElement e : sc.manual_cookies_selection) {
				e.click();

			}
		}

	}

	@And("^user deselects cookies manually$")
	public void deselect_twelve_cookies_manually() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfAllElements(sc.manual_cookies_deselection));
		for (int i = 0; i < 3; i++) {
			for (WebElement e : sc.manual_cookies_deselection) {
				e.click();

			}
		}

	}
	
	@Then("^Make required choices button should appear indicating zero selections$") 
	public boolean verify_required_choices_button() throws Throwable {
		
		try{
			Thread.sleep(3000);
		assertThat(sc.make_choice_button.getText(), CoreMatchers.containsString("0"));
		log.debug("User is able to deselect cookies successfully");
		return true;
		}catch(Exception e){
			
			e.printStackTrace();
		}
		log.debug("User is not able to deselect cookies successfully");
		return false;
	}
	
	@Then("^menu options should be displayed$")
	public void verify_menu_options(DataTable table) throws Throwable{
		
		List<List<String>> data= table.raw();
		wait.until(ExpectedConditions.visibilityOfAllElements(sc.menu_options));
		int i=0;
		for(WebElement e : sc.menu_options)
		{
			
			Assert.assertEquals(data.get(i).get(0), e.getText());
			System.out.println(data.get(i).get(0) +":"+ e.getText() );
			log.info(data.get(i).get(0) +":"+ e.getText());
			i=i+1;
		}

	}
	
	@And("^user clicks \\+ of Vegan/ Gluten Free Twelve Cookie Gift Box$")
	public void user_clicks_of_Vegan_Gluten_Free_twelve_Cookie_Gift_Box() throws Throwable {
		new WebDriverWait(driver, 30).until(
		          webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		wait.until(ExpectedConditions.elementToBeClickable(sc.add_vegan_12_cookies));
		Actions action = new Actions(driver);
		 
        action.moveToElement(sc.add_vegan_12_cookies).build().perform();
        
        Thread.sleep(5000);
		sc.add_vegan_12_cookies.click();
//		wait.until(ExpectedConditions.elementToBeClickable(sc.add_product_button));
//		sc.add_product_button.click();
	}

	@And("^user clicks \\+ of Vegan/ Gluten Free Twenty Four Cookie Gift Box$")
	public void user_clicks_of_Vegan_Gluten_Free_twentyfour_Cookie_Gift_Box() throws Throwable {
		new WebDriverWait(driver, 30).until(
		          webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		wait.until(ExpectedConditions.elementToBeClickable(sc.add_vegan_24_cookies));
		Actions action = new Actions(driver);
		 
        action.moveToElement(sc.add_vegan_24_cookies).build().perform();
        
        Thread.sleep(5000);
		sc.add_vegan_24_cookies.click();
	}
	
	@And("^user clicks quantity$")
	public void user_clicks_quantity_dropdown() throws Throwable{
		
		wait.until(ExpectedConditions.elementToBeClickable(sc.quantity_button));
		log.info("waiting for Quantity button to be clickable");
		
		sc.quantity_button.click();
		log.info("Quantity button is clicked");
		sc.quantity_index.click();
		log.info("Quantity index is clicked");
	}
	
	@Then("^updated quantity of the product should reflect in the cart$")
	public void updated_quantity_of_the_product_should_reflect_in_the_cart() throws Throwable{
		
		try{
			
			Thread.sleep(2000);
			
			Assert.assertThat(order.product_ordered.getText(),CoreMatchers.containsString("2x "));
			System.out.println("User is able to edit product in the cart");
			log.info("User is able to edit product in the cart");
			}finally{
				order.delete_product(driver);
			}
	}
	
	@And("^user selects VEGAN/GLUTEN FREE TWELVE COOKIE GIFT BOX$")
	public void user_selects_VEGAN_GLUTEN_FREE_TWELVE_COOKIE_GIFT_BOX() throws Throwable{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		wait.until(ExpectedConditions.elementToBeClickable(sc.vegan_12_cookies));
		Actions action = new Actions(driver);
		 
        action.moveToElement(sc.vegan_12_cookies).build().perform();
        
        Thread.sleep(5000);
		sc.vegan_12_cookies.click();
		
	}
	
	@And("^user selects VEGAN/GLUTEN FREE TWENTY FOUR COOKIE GIFT BOX$")
	public void user_selects_VEGAN_GLUTEN_FREE_TWENTY_FOUR_COOKIE_GIFT_BOX() throws Throwable{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		wait.until(ExpectedConditions.elementToBeClickable(sc.vegan_24_cookies));
		Actions action = new Actions(driver);
		 
        action.moveToElement(sc.vegan_24_cookies).build().perform();
        
        Thread.sleep(5000);
		sc.vegan_24_cookies.click();
		
	}
	
	@Then("^user should get the suggestion to place Delivery order$")
	public void verify_suggestion_is_displayed() throws Throwable{
		try{
		Thread.sleep(3000);
		Assert.assertTrue(sc.suggestion.isDisplayed());
		System.out.println("Suggestion displayed is: " +sc.suggestion.getText());
		log.info("Suggestion displayed is: " +sc.suggestion.getText());
		log.error("Suggestion is not displayed");
		}finally{
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			order.cart_button.click();
			order.delete_product(driver);
		}
	}
	
	@And("^user clicks on here link displayed in suggession under address box$")
	public void click_here_link() throws Throwable {
		
		wait.until(ExpectedConditions.visibilityOf(sc.suggestion));
		sc.here.click();
	}
	
	@Then("^shipping methods should be displayed$")
	public void shipping_methods_should_be_displayed() throws Throwable{
		
		try{
		wait.until(ExpectedConditions.visibilityOf(sc.shipping_method_1));
		Assert.assertTrue(sc.shipping_method_1.isDisplayed());
		log.debug("Shipping method :" +sc.shipping_method_1.getText()+ "is displayed.");
		
		Assert.assertTrue(sc.shipping_method_2.isDisplayed());
		log.debug("Shipping method :" +sc.shipping_method_2.getText()+ "is displayed.");
		
		Assert.assertTrue(sc.shipping_method_3.isDisplayed());
		log.debug("Shipping method :" +sc.shipping_method_3.getText()+ "is displayed.");
		
		}finally{
			wait.until(ExpectedConditions.elementToBeClickable(order.cart_button));
			order.cart_button.click();
			order.delete_product(driver);
		}
	}
	
}
