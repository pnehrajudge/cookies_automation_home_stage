package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.utils.TestBase;

public class Menu extends TestBase{
	
	@FindBy(xpath = "//a[contains(text(),'Menu')]")
	public WebElement menu_tab;
	
	@FindBy(xpath = "//h3[contains(text(),'Deals')]")
	public WebElement menu_page;

	@FindBys(@FindBy(xpath = "//img[contains(@class,'card-img-top ae-img')]"))
    public WebElement menu_images;
	
	WebDriver driver = getDriver();
	WebDriverWait wait = new WebDriverWait(driver, 30);
	
	public Menu(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	

}
