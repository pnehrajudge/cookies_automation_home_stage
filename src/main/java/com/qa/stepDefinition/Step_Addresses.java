package com.qa.stepDefinition;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;
import com.qa.hooks.ServiceHooks;
import com.qa.pages.AdminCallCenter;
import com.qa.pages.Catering;
import com.qa.pages.Order;
import com.qa.pages.SignIn;
import com.qa.pages.SignUp;
import com.qa.utils.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Step_Addresses extends TestBase{
	
	
	WebDriver driver = getDriver();
	AdminCallCenter adminCallCenter = new AdminCallCenter(driver);
	Catering catering = new Catering(driver);
	Order order = new Order(driver);
	SignUp signup = new SignUp(driver);
	SignIn s = new SignIn(driver);
	Actions action = new Actions(driver);
	JavascriptExecutor jse = (JavascriptExecutor) driver;
	ServiceHooks serviceHooks = new ServiceHooks();
	
	String url= getUrl();

	
	@When("^user enters the Address as \"([^\"]*)\"$")
	public void user_enters_the_address(String arg) throws Throwable {
		WebDriverWait waitlocal = new WebDriverWait(driver, 10);
		waitlocal.until(ExpectedConditions.visibilityOf(order.address_box));
		// log.info("waiting for address textbox to appear");
		/* \"([^\"]*)\"
		 * new WebDriverWait(driver, 20).until( webDriver ->
		 * ((JavascriptExecutor)
		 * webDriver).executeScript("return document.readyState").equals(
		 * "complete"));
		 */
		String selectAll = Keys.chord(Keys.CONTROL, "a");
		order.address_box.sendKeys(selectAll,Keys.DELETE);
		boolean verify_text = order.address_box.getAttribute("value").isEmpty();
		System.out.println("Value present in Address testbox :" + verify_text);
		// log.info("Value present in Address testbox :"+verify_text);

			
			// Thread.sleep(5000);
			order.address_box.sendKeys(arg);
			System.out.println("Entered address is: " + order.address_box.getAttribute("value"));
		Thread.sleep(500);
		order.address_box.sendKeys(Keys.DOWN);
		order.address_box.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		try{
		if(order.okayToClearCart.isDisplayed()){
			order.okayToClearCart.click();
		}
		}
		catch (Exception e){
			System.out.println("cart is already empty");
			
		}
//		new WebDriverWait(driver, 30).until(webDriver -> ((JavascriptExecutor) webDriver)
//				.executeScript("return document.readyState").equals("complete"));
		
	}
	
	
	@Given("^User opens the Website1$")
	 public void user_opens_the_Website(/*DataTable table*/) throws Throwable {
		    	driver.get(url); 
	       
		  }
	
	@Then("^Verify if Delivery button is present and get store name if delivery button is present$")
	public void verify_Delivery_button_is_Present() throws IOException {
		
		WebDriverWait waitlocal = new WebDriverWait(driver, 10);
		waitlocal.until(ExpectedConditions.visibilityOf(order.addressVerificationDeliveryBtn));
		if(order.addressVerificationDeliveryBtn.isDisplayed()) {
			String storeName = order.storeName.getText();
			System.out.println("Delivery Button Present @ Store Name...#######"+storeName);
			String srcPath = ServiceHooks.CaptureScreen(driver);
			Reporter.addScreenCaptureFromPath(srcPath);
			
		}
		
	}
	
	

}
